import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { CommunicationService } from "../services/communication.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { DialogService } from "../services/dialog.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__public_jobs",
  templateUrl: "./public_jobs.template.html",
  styleUrls: ["./public_jobs.component.scss"],
})
export class PublicJobsComponent implements OnInit {
  @HostBinding("class") hostclasses = "nonstandardiconcontainer container px-0 viewfor__DETAILS viewhas__filters";
  constructor(
    private apiService: ApiService,
    private commonutilsService: CommonutilsService,
    private communicationService: CommunicationService,
    private templateutilsService: TemplateutilsService,
    public dialogService: DialogService,
    private toastService: ToastService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      if (params.job_location_city) {
        this.filter_data["job_location_city"] = params.job_location_city;
      }
      if (params.job_category_id) {
        this.filter_data["job_category_id"] = parseInt(params.job_category_id);
      }
    });

    this.initializeFormGroup();

    this.route.queryParams.subscribe((queryParams) => {
      if (queryParams.page_number) {
        this.page_number = parseInt(queryParams.page_number);
      } else {
        this.page_number = 1;
      }
      this.load_data();
    });
  }

  filter_data = {};
  filtermodalid: number;
  openFilterModal() {
    if (this.filtermodalid === undefined) {
      this.filtermodalid = Math.floor(Math.random() * 10000 + 1);
    }
    this.communicationService.communication = { context: { parentid: this.filtermodalid }, obj: { filter_data: this.filter_data } };
    this.dialogService.openComponent("CandidateSearchJobsFilterComponent", "CandidateSearchJobsFilterModule", this.dialogService.DIALOGTYPES.LG, undefined, "fullscreendialog");
    let parentSubscription = this.communicationService.detectNewCommunication().subscribe(() => {
      if (this.communicationService.communication.context.parentid != this.filtermodalid) {
        return;
      }
      this.filter_data = this.communicationService.communication.obj.filter_data;
      this.page_number = 1;
      var newQueryParams = Object.assign({}, this.route.snapshot.queryParams);
      for (var paramKey in this.filter_data) {
        if (newQueryParams[paramKey] !== undefined) {
          delete newQueryParams[paramKey];
        }
      }
      if (newQueryParams["page_number"] !== undefined) {
        delete newQueryParams["page_number"];
      }
      this.router.navigate([], { queryParams: newQueryParams });
      this.load_data();
      parentSubscription.unsubscribe();
    });
  }

  sort_data = { sort_by: "id", sort_order: "DESC" };
  sortmodalid: number;
  openSortModal() {
    if (this.sortmodalid === undefined) {
      this.sortmodalid = Math.floor(Math.random() * 10000 + 1);
    }
    this.communicationService.communication = { context: { parentid: this.sortmodalid }, obj: { sort_data: this.sort_data } };
    this.dialogService.openComponent("CandidateSearchJobsSortComponent", "CandidateSearchJobsSortModule", this.dialogService.DIALOGTYPES.LG, undefined, "fullscreendialog");
    let parentSubscription = this.communicationService.detectNewCommunication().subscribe(() => {
      if (this.communicationService.communication.context.parentid != this.sortmodalid) {
        return;
      }
      this.sort_data = this.communicationService.communication.obj.sort_data;
      this.load_data();
      parentSubscription.unsubscribe();
    });
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("job_location_city", new FormControl(""));
    this.load_city_id_options().subscribe((res) => {
      this.response_city_id_options = res;
    });
    this.localform.addControl("job_category_id", new FormControl(""));
    this.load_category_id_options().subscribe((res) => {
      this.response_category_id_options = res;
    });
    this.localform.addControl("searchbutton", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("filter_button", new FormControl(""));
    this.localform.addControl("jobs", new FormArray([]));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  searchPublicJobs() {
    var params = {};
    if (this.localform.value["job_location_city"] !== undefined && this.localform.value["job_location_city"] !== "") {
      params["job_location_city"] = this.localform.value["job_location_city"];
    }
    if (this.localform.value["job_category_id"] !== undefined && this.localform.value["job_category_id"] !== "") {
      params["job_category_id"] = this.localform.value["job_category_id"];
    }
    if (JSON.stringify(params) === JSON.stringify(this.filter_data)) {
      this.toastService.showError("No changes done", "Same filters data already being displayed.");
      return;
    }
    this.templateutilsService.navigateToRoute("jobs", undefined, params);
    setTimeout(() => {
      location.reload();
    }, 200);
  }
  addNode_jobs(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("company_photo_id", new FormControl(""));
    nodeControl.addControl("job_title", new FormControl(""));
    nodeControl.addControl("job_description", new FormControl(""));
    nodeControl.addControl("job_description_clean", new FormControl(""));
    nodeControl.addControl("video_id_media_frame_url", new FormControl(""));
    nodeControl.addControl("company_name", new FormControl(""));
    nodeControl.addControl("applybutton", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("sharebutton", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("job_location_city", new FormControl(""));
    nodeControl.addControl("min_job_salary", new FormControl("", Validators.min(0)));
    nodeControl.addControl("num_of_openings", new FormControl("", Validators.min(0)));
    nodeControl.addControl("experience", new FormControl(""));
    nodeControl.addControl("applied_on", new FormControl(""));
    nodeControl.addControl("favorite_id", new FormControl(""));
    nodeControl.addControl("application_id", new FormControl(""));
    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("num_of_applications", new FormControl("", this.skipsubmitValidator));

    if (nodeData !== undefined) {
      nodeControl.controls["company_photo_id"].setValue(nodeData["company_photo_id"]);
      nodeControl.controls["job_title"].setValue(nodeData["job_title"]);
      nodeControl.controls["job_description"].setValue(nodeData["job_description"]);
      nodeControl.controls["job_description_clean"].setValue(nodeData["job_description_clean"]);
      nodeControl.controls["video_id_media_frame_url"].setValue(nodeData["video_id_media_frame_url"]);
      nodeControl.controls["company_name"].setValue(nodeData["company_name"]);
      nodeControl.controls["applybutton"].setValue(nodeData["applybutton"]);
      nodeControl.controls["sharebutton"].setValue(nodeData["sharebutton"]);
      nodeControl.controls["job_location_city"].setValue(nodeData["job_location_city"]);
      nodeControl.controls["min_job_salary"].setValue(nodeData["min_job_salary"]);
      nodeControl.controls["num_of_openings"].setValue(nodeData["num_of_openings"]);
      nodeControl.controls["experience"].setValue(nodeData["experience"]);
      nodeControl.controls["applied_on"].setValue(nodeData["applied_on"]);
      nodeControl.controls["favorite_id"].setValue(nodeData["favorite_id"]);
      nodeControl.controls["application_id"].setValue(nodeData["application_id"]);
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["num_of_applications"].setValue(nodeData["num_of_applications"]);
    }
    (<FormArray>this.localform.controls["jobs"]).push(nodeControl);
  }

  removeNode_jobs(index) {
    (<FormArray>this.localform.controls["jobs"]).removeAt(index);
  }

  records_per_page = 6;
  page_number = 1;
  total_pages;
  load_data_page(page_number) {
    var newQueryParams = Object.assign({}, this.route.snapshot.queryParams);
    newQueryParams["page_number"] = page_number;
    this.router.navigate(["."], { relativeTo: this.route, queryParams: newQueryParams });
  }
  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getactivejobsforpublic", { filters: this.filter_data, order: this.sort_data, records_per_page: this.records_per_page, page_number: this.page_number }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res.response;
      if (!isNaN(res.total)) {
        this.total_pages = Math.ceil(res.total / this.records_per_page);
      } else {
        this.total_pages = Math.ceil(res.total[Object.keys(res.total)[0]] / this.records_per_page);
      }

      this.localform.controls["job_location_city"].setValue(this.response_data["job_location_city"]);
      this.localform.controls["job_category_id"].setValue(this.response_data["job_category_id"]);
      this.localform.controls["searchbutton"].setValue(this.response_data["searchbutton"]);
      this.localform.controls["filter_button"].setValue(this.response_data["filter_button"]);
      this.localform.controls["jobs"] = new FormArray([]);
      if (this.response_data["jobs"] !== undefined) {
        this.response_data["jobs"].forEach((node) => {
          this.addNode_jobs(node);
        });
      }
    });
  }

  response_category_id_options: any;
  load_category_id_options() {
    return this.apiService.post_api("jobportal", "getjobcategoriesoptions");
  }

  response_city_id_options: any;
  load_city_id_options() {
    return this.apiService.post_api("jobportal", "getjobcitiesoptions");
  }
}
