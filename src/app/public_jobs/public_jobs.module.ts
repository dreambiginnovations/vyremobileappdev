import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { CustomDropDownModule } from "../common/dropdown/dropdown.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { PublicJobsComponent } from "./public_jobs.component";

export const routes: Routes = [
  {
    path: "",
    component: PublicJobsComponent,
  },
];

@NgModule({
  declarations: [PublicJobsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, CustomDropDownModule],
})
export class PublicJobsModule {
  static components = {
    PublicJobsComponent: PublicJobsComponent,
  };
}
