import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__candidate_job_view_details",
  templateUrl: "./candidate_job_view_details.template.html",
  styleUrls: ["./candidate_job_view_details.component.scss"],
})
export class CandidateJobViewDetailsComponent implements OnInit {
  @HostBinding("class") hostclasses = "nonstandardiconcontainer container px-0 viewfor__DETAILS";
  id: any;
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, public templateutilsService: TemplateutilsService, private toastService: ToastService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];

    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("video_id", new FormControl(""));
    this.localform.addControl("company_photo_id", new FormControl(""));
    this.localform.addControl("job_title", new FormControl(""));
    this.localform.addControl("employer_mobile", new FormControl(""));
    this.localform.addControl("mobiletitle", new FormControl(""));
    this.localform.addControl("job_location_city", new FormControl(""));
    this.localform.addControl("min_job_salary", new FormControl("", Validators.min(0)));
    this.localform.addControl("num_of_openings", new FormControl("", Validators.min(0)));
    this.localform.addControl("experience", new FormControl(""));
    this.localform.addControl("social_share", new FormControl(""));
    this.localform.addControl("applied_on", new FormControl(""));
    this.localform.addControl("favorite_id", new FormControl(""));
    this.localform.addControl("apply", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("apply", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("not_favorite", new FormControl(""));
    this.localform.addControl("favorite", new FormControl(""));
    this.localform.addControl("job_category_id", new FormControl(""));
    this.load_category_id_options().subscribe((res) => {
      this.response_category_id_options = res;
    });
    this.localform.addControl("job_role_id", new FormControl(""));
    this.load_role_id_options().subscribe((res) => {
      this.response_role_id_options = res;
    });
    this.localform.addControl("min_experience", new FormControl("", Validators.min(0)));
    this.localform.addControl("max_experience", new FormControl("", Validators.min(0)));
    this.localform.addControl("is_remote_job", new FormControl(""));
    this.localform.addControl("job_location", new FormControl(""));
    this.localform.addControl("job_latitude_location", new FormControl(""));
    this.localform.addControl("job_longitude_location", new FormControl(""));
    this.localform.addControl("job_location_city", new FormControl(""));
    this.localform.addControl("job_location_state", new FormControl(""));
    this.localform.addControl("job_location_country", new FormControl(""));
    this.localform.addControl("min_job_salary", new FormControl("", Validators.min(0)));
    this.localform.addControl("max_job_salary", new FormControl("", Validators.min(0)));
    this.localform.addControl("education_requirement", new FormControl(""));
    this.localform.addControl("num_of_openings", new FormControl("", Validators.min(0)));
    this.localform.addControl("id", new FormControl(""));
    this.localform.addControl("num_of_applications", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("job_description", new FormControl(""));
    this.localform.addControl("job_description_clean", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("attachments", new FormArray([], Validators.min(1)));
  }

  markJobAsFavorite(formgroup) {
    this.apiService.post_api("jobportal", "markJobAsFavorite", { job_id: formgroup.value["id"] }).subscribe((res) => {
      formgroup.controls["favorite_id"].setValue(res.inserted_entity_id);
    });
  }
  unmarkJobAsFavorite(formgroup) {
    this.apiService.post_api("jobportal", "unmarkJobAsFavorite", { id: formgroup.value["favorite_id"] }).subscribe((res) => {
      formgroup.controls["favorite_id"].setValue(null);
    });
  }
  addNode_attachments(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("attachment_media_id", new FormControl(""));

    if (nodeData !== undefined) {
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["attachment_media_id"].setValue(nodeData["attachment_media_id"]);
    }
    (<FormArray>this.localform.controls["attachments"]).push(nodeControl);
  }

  removeNode_attachments(index) {
    (<FormArray>this.localform.controls["attachments"]).removeAt(index);
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getjobforcandidate", { id: this.id }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["video_id"].setValue(this.response_data["video_id"]);
      this.localform.controls["company_photo_id"].setValue(this.response_data["company_photo_id"]);
      this.localform.controls["job_title"].setValue(this.response_data["job_title"]);
      this.localform.controls["employer_mobile"].setValue(this.response_data["employer_mobile"]);
      this.localform.controls["mobiletitle"].setValue(this.response_data["mobiletitle"]);
      this.localform.controls["job_location_city"].setValue(this.response_data["job_location_city"]);
      this.localform.controls["min_job_salary"].setValue(this.response_data["min_job_salary"]);
      this.localform.controls["num_of_openings"].setValue(this.response_data["num_of_openings"]);
      this.localform.controls["experience"].setValue(this.response_data["experience"]);
      this.localform.controls["social_share"].setValue(this.response_data["social_share"]);
      this.localform.controls["applied_on"].setValue(this.response_data["applied_on"]);
      this.localform.controls["favorite_id"].setValue(this.response_data["favorite_id"]);
      this.localform.controls["apply"].setValue(this.response_data["apply"]);
      this.localform.controls["apply"].setValue(this.response_data["apply"]);
      this.localform.controls["not_favorite"].setValue(this.response_data["not_favorite"]);
      this.localform.controls["favorite"].setValue(this.response_data["favorite"]);
      this.localform.controls["job_category_id"].setValue(this.response_data["job_category_id"]);
      this.localform.controls["job_role_id"].setValue(this.response_data["job_role_id"]);
      this.localform.controls["min_experience"].setValue(this.response_data["min_experience"]);
      this.localform.controls["max_experience"].setValue(this.response_data["max_experience"]);
      this.localform.controls["is_remote_job"].setValue(this.response_data["is_remote_job"]);
      this.localform.controls["job_location"].setValue(this.response_data["job_location"]);
      this.localform.controls["job_latitude_location"].setValue(this.response_data["job_latitude_location"]);
      this.localform.controls["job_longitude_location"].setValue(this.response_data["job_longitude_location"]);
      this.localform.controls["job_location_city"].setValue(this.response_data["job_location_city"]);
      this.localform.controls["job_location_state"].setValue(this.response_data["job_location_state"]);
      this.localform.controls["job_location_country"].setValue(this.response_data["job_location_country"]);
      this.localform.controls["min_job_salary"].setValue(this.response_data["min_job_salary"]);
      this.localform.controls["max_job_salary"].setValue(this.response_data["max_job_salary"]);
      this.localform.controls["education_requirement"].setValue(this.response_data["education_requirement"]);
      this.localform.controls["num_of_openings"].setValue(this.response_data["num_of_openings"]);
      this.localform.controls["id"].setValue(this.response_data["id"]);
      this.localform.controls["num_of_applications"].setValue(this.response_data["num_of_applications"]);
      this.localform.controls["job_description"].setValue(this.response_data["job_description"]);
      this.localform.controls["job_description_clean"].setValue(this.response_data["job_description_clean"]);
      this.localform.controls["attachments"] = new FormArray([]);
      if (this.response_data["attachments"] !== undefined) {
        this.response_data["attachments"].forEach((node) => {
          this.addNode_attachments(node);
        });
      }
    });
  }

  response_category_id_options: any;
  load_category_id_options() {
    return this.apiService.post_api("jobportal", "getjobcategoriesoptions");
  }

  response_role_id_options: any;
  load_role_id_options() {
    return this.apiService.post_api("jobportal", "getjobrolesoptions");
  }
}
