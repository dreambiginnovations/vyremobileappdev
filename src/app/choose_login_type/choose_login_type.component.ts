import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { EnvService } from "../services/env.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__choose_login_type",
  templateUrl: "./choose_login_type.template.html",
  styleUrls: ["./choose_login_type.component.scss"],
})
export class ChooseLoginTypeComponent implements OnInit {
  @HostBinding("class") hostclasses = "viewascardondesktop bg-white align-self-center container px-0 viewfor__DETAILS";
  showchoice = false;
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, public envService: EnvService, private templateutilsService: TemplateutilsService, private toastService: ToastService) {}

  ngOnInit() {
    this.getLatestUserState();

    this.initializeFormGroup();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  getLatestUserState() {
    this.apiService.post_api("jobportal", "getuserfromsession").subscribe((res) => {
      if (res.candidate_id !== null) {
        this.chooseUserType("candidate");
      } else if (res.employer_id !== null) {
        this.chooseUserType("employer");
      } else {
        this.showchoice = true;
      }
    });
  }
  chooseUserType(type) {
    this.apiService.post_api("jobportal", "choose", { type: type }).subscribe((res) => {
      if (res.status == "FALSE") {
        this.toastService.showError("Error", res.message);
      }
      this.commonutilsService.processResponse(res);
    });
  }
}
