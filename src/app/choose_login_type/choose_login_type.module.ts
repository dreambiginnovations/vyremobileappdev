import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { ChooseLoginTypeComponent } from "./choose_login_type.component";

export const routes: Routes = [
  {
    path: "",
    component: ChooseLoginTypeComponent,
  },
];

@NgModule({
  declarations: [ChooseLoginTypeComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class ChooseLoginTypeModule {
  static components = {
    ChooseLoginTypeComponent: ChooseLoginTypeComponent,
  };
}
