import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { EmployerRequestVideoResumeComponent } from "./employer_request_video_resume.component";

import { PipesModule } from "../common/pipes/pipes.module";

export const routes: Routes = [
  {
    path: "",
    component: EmployerRequestVideoResumeComponent,
  },
];

@NgModule({
  declarations: [EmployerRequestVideoResumeComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})

export class EmployerRequestVideoResumeModule {
  static components = {
    EmployerRequestVideoResumeComponent: EmployerRequestVideoResumeComponent,
  };
}
