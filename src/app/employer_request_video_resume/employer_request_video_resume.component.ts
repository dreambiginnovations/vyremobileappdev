import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NavigationService } from "../services/navigation.service";
import { EnvService } from "../services/env.service";
import { ApiService } from "../services/api.service";
import { AppstateService } from "../services/appstate.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_request_video_resume",
  templateUrl: "./employer_request_video_resume.component.html",
  styleUrls: ["./employer_request_video_resume.component.scss"],
})
export class EmployerRequestVideoResumeComponent implements OnInit {
  @HostBinding("class") hostclasses = "viewascardondesktop bg-white px-0 flex-column nonstandardiconcontainer text-center container viewfor__DETAILS";
  constructor(public router: Router,private navigationService: NavigationService, public envService: EnvService, private apiService: ApiService, private commonutilsService: CommonutilsService, private toastService: ToastService, private appstateService: AppstateService) {
    this.username = this.appstateService.appState.user.name;
  }

  step = "1.1";
  gotostep(value) {
    this.step = value;
  }
  gotonextstate: any;
  
  ngOnInit() {
    console.log("this.appstateService.tempdata", this.appstateService.tempdata);
    this.gotonextstate = this.appstateService.tempdata?.gotonextstate;
    if(this.gotonextstate===undefined) {this.gotonextstate = "candidate/home";}
    this.initializeFormGroup();
    this.getvideorequestsofcandidate();
  }

  localform: FormGroup;
  initializeFormGroup() {
    this.localform = new FormGroup({});
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }
  videorequest:any = {};
  username: string = "";
  getvideorequestsofcandidate() {
    this.apiService.post_api("jobportal", "getvideorequestsofcandidate").subscribe((res) => {
      this.videorequest = res.result[0];
    });
  }

}
