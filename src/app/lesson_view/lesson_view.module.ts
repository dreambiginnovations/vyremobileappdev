import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { YouTubePlayerModule } from "@angular/youtube-player";
import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { LessonViewComponent } from "./lesson_view.component";

export const routes: Routes = [
  {
    path: "",
    component: LessonViewComponent,
  },
];

@NgModule({
  declarations: [LessonViewComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, YouTubePlayerModule, ImgCropperModule, VideoRecordModule],
})
export class LessonViewModule {
  static components = {
    LessonViewComponent: LessonViewComponent,
  };
}
