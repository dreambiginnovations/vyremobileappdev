import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__lesson_view",
  templateUrl: "./lesson_view.template.html",
  styleUrls: ["./lesson_view.component.scss"],
})
export class LessonViewComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  id: any;
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, private toastService: ToastService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];

    this.initializeFormGroup();
    this.load_data();

    this.apiService.post_api("jobportal", "registerview", { content_article_id: this.id, content_type: "LESSON" }).subscribe((viewres) => {});
  }

  localform: FormGroup;
  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("id", new FormControl(""));
    this.localform.addControl("content_article_name", new FormControl(""));
    this.localform.addControl("attachments", new FormArray([]));
    this.localform.addControl("content_article_content", new FormControl(""));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  addNode_attachments(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("attachment_type", new FormControl(""));
    nodeControl.addControl("attachment_name", new FormControl(""));
    nodeControl.addControl("course_lesson_media_dummy_id", new FormControl(""));
    nodeControl.addControl("course_lesson_media_dummy_id2", new FormControl(""));
    nodeControl.addControl("dummy_label_for_fileicon", new FormControl(""));
    nodeControl.addControl("media_file_internal_name", new FormControl(""));

    if (nodeData !== undefined) {
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["attachment_type"].setValue(nodeData["attachment_type"]);
      nodeControl.controls["attachment_name"].setValue(nodeData["attachment_name"]);
      nodeControl.controls["course_lesson_media_dummy_id"].setValue(nodeData["course_lesson_media_dummy_id"]);
      nodeControl.controls["course_lesson_media_dummy_id2"].setValue(nodeData["course_lesson_media_dummy_id2"]);
      nodeControl.controls["dummy_label_for_fileicon"].setValue(nodeData["dummy_label_for_fileicon"]);
      nodeControl.controls["media_file_internal_name"].setValue(nodeData["media_file_internal_name"]);
    }
    (<FormArray>this.localform.controls["attachments"]).push(nodeControl);
  }

  removeNode_attachments(index) {
    (<FormArray>this.localform.controls["attachments"]).removeAt(index);
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getlesson", { id: this.id }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["id"].setValue(this.response_data["id"]);
      this.localform.controls["content_article_name"].setValue(this.response_data["content_article_name"]);
      this.localform.controls["attachments"] = new FormArray([]);
      if (this.response_data["attachments"] !== undefined) {
        this.response_data["attachments"].forEach((node) => {
          this.addNode_attachments(node);
        });
      }

      this.localform.controls["content_article_content"].setValue(this.response_data["content_article_content"]);
    });
  }
}
