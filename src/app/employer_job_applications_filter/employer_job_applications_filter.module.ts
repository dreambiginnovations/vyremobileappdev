import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { CustomDropDownModule } from "../common/dropdown/dropdown.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerJobApplicationsFilterComponent } from "./employer_job_applications_filter.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerJobApplicationsFilterComponent,
  },
];

@NgModule({
  declarations: [EmployerJobApplicationsFilterComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, CustomDropDownModule],
})
export class EmployerJobApplicationsFilterModule {
  static components = {
    EmployerJobApplicationsFilterComponent: EmployerJobApplicationsFilterComponent,
  };
}
