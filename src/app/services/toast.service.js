"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var ToastService = /** @class */ (function () {
    function ToastService(dialogService) {
        this.dialogService = dialogService;
    }
    ToastService.prototype.showSuccess = function (title, message, timeOut) {
        if (timeOut === undefined) {
            timeOut = 3000;
        }
        /*this.dialogService.success(message, title, {
          positionClass: "toast-top-center",
          timeOut,
        });*/
        this.dialogService.showMessageDialog(message, title);
    };
    ToastService.prototype.showError = function (title, message, timeOut) {
        if (timeOut === undefined) {
            timeOut = 3000;
        }
        /*this.dialogService.error(message, title, {
          positionClass: "toast-top-center",
          timeOut,
        });*/
        this.dialogService.showMessageDialog(message, title);
    };
    ToastService.prototype.showWarning = function (title, message, timeOut) {
        if (timeOut === undefined) {
            timeOut = 3000;
        }
        /*this.dialogService.warning(message, title, {
          positionClass: "toast-top-center",
          timeOut,
        });*/
        this.dialogService.showMessageDialog(message, title);
    };
    ToastService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], ToastService);
    return ToastService;
}());
exports.ToastService = ToastService;
