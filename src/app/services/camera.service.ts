import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CameraService {
  constructor() { }

  // Method to fetch the list of available cameras
  async getCameraList(): Promise<MediaDeviceInfo[]> {
    try {
      var stream = await navigator.mediaDevices.getUserMedia({video: true});
      const devices = await navigator.mediaDevices.enumerateDevices();
      console.log("navigator.mediaDevices.enumerateDevices()", devices);
      this.stopStream(stream);
      var videoinputdevices = devices.filter(device => device.kind === 'videoinput');
      if(videoinputdevices.length<=1) {
        return videoinputdevices;
      } else {
        return this.getCameraDevicesInCaseOfMultiple();
      }
    } catch (error) {
      console.error('Error enumerating devices:', error);
      return [];
    }
  }
  
  stopStream(stream) {
    var tracks = stream.getTracks();
    for(var i = 0; i < tracks.length; i++){
       console.log("stopping track", tracks[i].stop())
    }
  }
  async getCameraDevicesInCaseOfMultiple() {
    var cameradevices = [];
    var stream;
    try {
      console.log("Processing front camera");
      stream = await navigator.mediaDevices.getUserMedia({video: {facingMode: {exact: "user"}}});
      cameradevices.push({deviceId: this.getCameraDeviceIdFromStream(stream), label: "Front Camera"});
      this.stopStream(stream);
    } catch (frontfacingerror) {
      console.error('Got error while accesing front camera:', frontfacingerror);
    }
    
    try {
      console.log("Processing rear camera");
      stream = await navigator.mediaDevices.getUserMedia({video: {facingMode: {exact: "environment"}}});
      cameradevices.push({deviceId: this.getCameraDeviceIdFromStream(stream), label: "Rear Camera"});
      this.stopStream(stream);
    } catch (rearfacingerror) {
      console.error('Got error while accesing rear camera:', rearfacingerror);
    }
    return cameradevices;
  }
  getCameraDeviceIdFromStream(stream) {
    var tracks = stream.getTracks();
    for(var i = 0; i < tracks.length; i++){
       console.log("deviceid of track", tracks[i].getSettings().deviceId)
    }
    return tracks[0].getSettings().deviceId;
  }
}
