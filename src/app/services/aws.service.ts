import { Injectable } from "@angular/core";
import { HttpEvent, HttpEventType } from "@angular/common/http";
import { Router } from "@angular/router";

import { AppstateService } from "./appstate.service";
import { ApiService } from "./api.service";
import { EnvService } from "./env.service";
import { DialogService } from "./dialog.service";
import { ToastService } from "./toast.service";
import { CommunicationService } from "./communication.service";

@Injectable({
  providedIn: "root",
})
export class AWSService {
  constructor(private apiService: ApiService, private dialogService: DialogService, private communicationService: CommunicationService, private appstateService: AppstateService, private envService: EnvService, private router: Router, private toastService: ToastService) {}

  progress;
  public uploadInProgress = false;

  prepareToUploadFileToS3(fileName: string, fileCategory: string, isNewFile: boolean) {
    const postParams = { fileName, fileCategory, isNewFile };
    return this.apiService.post_api("common", "aws_put_createpresignedurl", postParams);
  }

  uploadBase64ImageToS3(params) {
    console.log("params", params);
    const url = params.url;
    const fileContent = params.fileContent;
    const headers = params.headers;
    console.log(params);
    const data = fileContent.replace(/^data:image\/\w+;base64,/, "");
    const buff = new Buffer(data, "base64");
    this.uploadWithProgress(url, buff.buffer, headers, "Uploading");
  }

  uploadFileToS3(params) {
    const url = params.url;
    const fileContent = params.fileContent;
    const headers = params.headers;
    console.log(params);
    this.uploadWithProgress(url, fileContent, headers, "Uploading");
  }
  uploadWithProgress(url, content, headers, uploadProgressTitle) {
    this.uploadInProgress = true;
    const dialogRef = this.dialogService.openComponent("ProgressBarComponent", "ProgressBarModule");
    this.apiService.post_api_alt(url, content, headers).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          console.log("Request has been made!");
          this.communicationService.broadcastCommunication({
            context: { parentid: "progressbarparent" },
            obj: { title: uploadProgressTitle, progress: 0 },
          });
          break;
        case HttpEventType.ResponseHeader:
          console.log("Response header has been received!", event);
          break;
        case HttpEventType.UploadProgress:
          this.progress = Math.round((event.loaded / event.total) * 100);
          console.log(`Uploaded! ${this.progress}%`);
          this.communicationService.broadcastCommunication({
            context: { parentid: "progressbarparent" },
            obj: { title: uploadProgressTitle, progress: this.progress },
          });
          break;
        case HttpEventType.Response:
          console.log("Upload complete");
          this.uploadInProgress = false;
          dialogRef.close();
          setTimeout(() => {
            this.progress = 0;
          }, 500);
      }
    });
  }

  deleteExistingFile(fileName, fileCategory) {
    this.apiService.post_api("common", "deleteobject", { fileName, fileCategory }).subscribe((res) => {});
  }
  currentUserMediaID: string | number;
  newUserMediaID: string | number;
  handleCandidateGuideProfileVideoUpload(questionNumber: number, communication_obj?: any) {
    console.log("Got into uploading Guided Profile", questionNumber);
    if(this.currentUserMediaID==undefined) {
      this.currentUserMediaID = this.appstateService.appState.user.video_id;
    }
    this.appstateService.central_upload_obj.uploadInProgress = true;
    if(questionNumber==0) {
      this.apiService.post_api("jobportal", "insertEmptyMedia", { media_type: "video_playlist", to_be_deleted: 0 }).subscribe((mediaRes) => {
        this.newUserMediaID = mediaRes.inserted_entity_id;
        this.handleCandidateGuideProfileVideoUpload_playlist(questionNumber, this.newUserMediaID);
        /*this.apiService.post_api("jobportal", "updatecandidatecandidate", {video_id: newUserMediaID, mobile: currentUserMobile}).subscribe((res) => {
          this.handleCandidateGuideProfileVideoUpload_playlist(newUserMediaID);
        });*/
      });  
    } else {
      if(communication_obj!==undefined && communication_obj.playlist_id) {
        this.newUserMediaID = communication_obj.playlist_id;
      }
      this.handleCandidateGuideProfileVideoUpload_playlist(questionNumber, this.newUserMediaID);
    }
  }
  handleCandidateGuideProfileVideoUpload_playlist(questionNumber, newUserMediaID:any) {
    this.handleCandidateGuideProfileVideoUpload_playlist_item(questionNumber, newUserMediaID);
  }
  handleCandidateGuideProfileVideoUpload_playlist_item(count, userPlayListId) {
    this.appstateService.central_upload_obj.uploadingTitle = "Preparing to upload for: " + this.appstateService.guided_recording["recording_" + count]["question"];
    this.apiService.post_api("jobportal", "insertEmptyMedia", { media_type: "video", playlist_id: userPlayListId, to_be_deleted: 0 }).subscribe((mediaRes) => {
      var insertedVideoId = mediaRes.inserted_entity_id;
      const d = new Date();
      let time = d.getTime();
      const videoFileName = "media_" + this.appstateService.appState.user.id + "_" + time + ".webm";
      var fileCategory = "userprofile__video";
      const mediaVideoFileURL = this.envService.values.ugcmediaurl + videoFileName;
      this.apiService.post_api("jobportal", "updateMedia", {id: insertedVideoId, media_file_internal_name: videoFileName, media_file_url: mediaVideoFileURL, media_file_external_name: this.appstateService.guided_recording["recording_" + count]["question"]}).subscribe((dummyRes) => {
        this.appstateService.central_upload_obj.uploadingProgress = 0;
        this.prepareToUploadFileToS3(videoFileName, fileCategory, true).subscribe((videoFileUploadRes) => {
          const params = {
            url: videoFileUploadRes.url,
            fileContent: this.appstateService.guided_recording["recording_" + count]["video"],
            headers: videoFileUploadRes.headers,
          };
          this.appstateService.central_upload_obj.uploadingTitle = "Uploading video for: " + this.appstateService.guided_recording["recording_" + count]["question"];
          this.apiService.post_api_alt(params.url, params.fileContent, params.headers).subscribe((videouploadevent: HttpEvent<any>) => {
            switch (videouploadevent.type) {
              case HttpEventType.Sent:
                console.log("Video Request has been made!", count);
                this.appstateService.central_upload_obj.uploadingProgress = 0;
                break;
              case HttpEventType.ResponseHeader:
                console.log("Video Response header has been received!", videouploadevent, count);
                break;
              case HttpEventType.UploadProgress:
                var progress = Math.round((videouploadevent.loaded / videouploadevent.total) * 100);
                console.log("Video Uploaded! " + progress + "%", count);
                this.appstateService.central_upload_obj.uploadingProgress = progress;
                break;
              case HttpEventType.Response:
                console.log("Video Upload complete", count);
                //this.appstateService.central_upload_obj.uploadingProgress = 0;
                count++;
                if(count===1) {
                  this.deleteExistingCandidateProfileVideoData(this.currentUserMediaID);
                  this.notifyEmployersOfVideoUpload();
                  this.apiService.post_api("jobportal", "updatecandidatecandidate", {video_id: this.newUserMediaID, mobile: this.appstateService.appState.user.mobile}).subscribe((res) => {});
                  this.apiService.post_api("jobportal", "updateMedia", {id: this.newUserMediaID, to_be_deleted: null}).subscribe((dummyRes) => {});
                } else if(count===4) {
                  this.toastService.showSuccess("Video Updated Successfully!", "Your video is updated successfully. It will be processed and ready in a few minutes.");
                  this.appstateService.guided_recording = undefined;
                  setTimeout(()=>{
                    this.router.navigate(['/candidate/searchjobs']);
                  }, 1000);
                }
                this.appstateService.central_upload_obj.uploadInProgress = false;
                this.apiService.post_api("jobportal", "updateMedia", {id: insertedVideoId, to_be_deleted: null}).subscribe((dummyRes) => {});
            }
          });
        });
      });
    });
  }
  handleCandidateOnewayInterviewVideoUpload() {
    this.appstateService.central_upload_obj.uploadInProgress = true;
    this.apiService.post_api("jobportal", "insertEmptyMedia", { media_type: "video_playlist" }).subscribe((mediaRes) => {
      var newUserMediaID = mediaRes.inserted_entity_id;
      this.handleCandidateOnewayInterviewVideoUpload_item(0);
    });
  }
  handleCandidateOnewayInterviewVideoUpload_item(count) {
    this.appstateService.central_upload_obj.uploadingTitle = "Preparing to upload for: " + this.appstateService.guided_recording["recording_" + count]["question"];
    this.apiService.post_api("jobportal", "insertEmptyMedia", { media_type: "video" }).subscribe((mediaRes) => {
      var insertedVideoId = mediaRes.inserted_entity_id;
      const d = new Date();
      let time = d.getTime();
      const videoFileName = "media_" + this.appstateService.appState.user.id + "_" + time + ".webm";
      var fileCategory = "userprofile__video";
      this.prepareToUploadFileToS3(videoFileName, fileCategory, true).subscribe((videoFileUploadRes) => {
        const params = {
          url: videoFileUploadRes.url,
          fileContent: this.appstateService.guided_recording["recording_" + count]["video"],
          headers: videoFileUploadRes.headers,
        };
        const mediaVideoFileURL = this.envService.values.ugcmediaurl + videoFileName;
        this.appstateService.central_upload_obj.uploadingTitle = "Uploading video for: " + this.appstateService.guided_recording["recording_" + count]["question"];
        this.apiService.post_api_alt(params.url, params.fileContent, params.headers).subscribe((videouploadevent: HttpEvent<any>) => {
          switch (videouploadevent.type) {
            case HttpEventType.Sent:
              console.log("Video Request has been made!", count);
              this.appstateService.central_upload_obj.uploadingProgress = 0;
              break;
            case HttpEventType.ResponseHeader:
              console.log("Video Response header has been received!", videouploadevent, count);
              break;
            case HttpEventType.UploadProgress:
              var progress = Math.round((videouploadevent.loaded / videouploadevent.total) * 100);
              console.log("Video Uploaded! " + progress + "%", count);
              this.appstateService.central_upload_obj.uploadingProgress = progress;
              break;
            case HttpEventType.Response:
              console.log("Video Upload complete", count);
              this.apiService.post_api("jobportal", "updateMedia", {id: insertedVideoId, media_file_internal_name: videoFileName, media_file_url: mediaVideoFileURL, media_file_external_name: this.appstateService.guided_recording["recording_" + count]["question"]}).subscribe((dummyRes) => {
                  this.appstateService.guided_recording["recording_" + count]["answer_id"] = insertedVideoId;
                  count++;
                  if(this.appstateService.guided_recording["recording_" + count]===undefined) {
                    this.appstateService.tempdata.screening_questions = [];
                    var tempCount = 0;
                    while(this.appstateService.guided_recording["recording_" + tempCount]!==undefined) {
                      console.log("this.appstateService.guided_recording", this.appstateService.guided_recording);
                      this.appstateService.tempdata.screening_questions.push({question_id: this.appstateService.guided_recording["recording_" + tempCount].question_id, answer_id: this.appstateService.guided_recording["recording_" + tempCount].answer_id});
                      tempCount++;
                    }
                    this.apiService.post_api("jobportal", "addjobapplication", this.appstateService.tempdata).subscribe((res) => {
                      this.appstateService.central_upload_obj.uploadInProgress = false;
                      this.appstateService.guided_recording = undefined;
                      this.toastService.showSuccess("Success", "Job Application submitted Successfully!");
                      this.markInterviewRequests();
                      this.router.navigate(['/candidate/myjobs']);
                    });
                  } else {
                    this.handleCandidateOnewayInterviewVideoUpload_item(count);  
                  }
                });
          }
        });
      });
    });
  }
  
  markInterviewRequests() {
    const timestamp = Date.now();
    const date = new Date(timestamp);
    const formattedTimestamp = date.toLocaleString('en-US', {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
    });
    this.apiService.post_api("jobportal", "markcandidateinterviewrequestasdone", { user_id: this.appstateService.appState.user.id, job_id: this.appstateService.tempdata.id, request_status: "COMPLETED", completed_on: formattedTimestamp  }).subscribe((res) => {});
  }
  notifyEmployersOfVideoUpload() {
    this.apiService.post_api("jobportal", "notifyemployersofvideoupload", { id: this.appstateService.appState.user.id }).subscribe((res) => {});
  }
  deleteExistingCandidateProfileVideoData(mediaID:any) {
    this.apiService.post_api("jobportal", "markallmediafordelete", { media_id: mediaID }).subscribe((mediaRes) => {
      
    });
  }
  
}


