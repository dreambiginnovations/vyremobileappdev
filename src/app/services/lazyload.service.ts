import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class LazyloadService {
  importProgressBarModule() {
    return import("../common/progressbar/progressbar.module").then((mod) => mod.ProgressBarModule);
  }

  importNavbarexpandedcandidateModule() {
    return import("../navbarexpandedcandidate/navbarexpandedcandidate.module").then((mod) => mod.NavbarexpandedcandidateModule);
  }

  importNavbarexpandedemployerModule() {
    return import("../navbarexpandedemployer/navbarexpandedemployer.module").then((mod) => mod.NavbarexpandedemployerModule);
  }

  importVjsPlayerModule() {
    return import("../common/videoplay/vjs-player.module").then((mod) => mod.VjsPlayerModule);
  }

  importCandidateSearchJobsFilterModule() {
    return import("../candidate_search_jobs_filter/candidate_search_jobs_filter.module").then((mod) => mod.CandidateSearchJobsFilterModule);
  }
  importCandidateSearchJobsSortModule() {
    return import("../candidate_search_jobs_sort/candidate_search_jobs_sort.module").then((mod) => mod.CandidateSearchJobsSortModule);
  }

  importEmployerJobApplicationsFilterModule() {
    return import("../employer_job_applications_filter/employer_job_applications_filter.module").then((mod) => mod.EmployerJobApplicationsFilterModule);
  }
  importEmployerJobSeekersFilterModule() {
    return import("../employer_job_seekers_filter/employer_job_seekers_filter.module").then((mod) => mod.EmployerJobSeekersFilterModule);
  }
  importCandidateNologinFormModule() {
    return import("../candidate_nologin_form/candidate_nologin_form.module").then((mod) => mod.CandidateNologinFormModule);
  }
  importSharepopupModule() {
    return import("../common/sharepopup/sharepopup.module").then((mod) => mod.SharepopupModule);
  }
}
