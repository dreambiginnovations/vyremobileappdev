import { Injectable } from "@angular/core";
import { HttpClient, HttpEventType, HttpEvent, HttpBackend } from "@angular/common/http";
import { Observable, Subject, BehaviorSubject } from "rxjs";

import { EnvService } from "./env.service";
import { AppstateService } from "./appstate.service";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  constructor(private http: HttpClient, private envService: EnvService, private handler: HttpBackend, private appStateService: AppstateService) {}

  private state = new Subject<any>();
  latestApiEvent: HttpEvent<any>;
  latestApiEventType: string;

  post_api(apiController: string, apiMethod: string, apiParams?: any, apiURLObj?: string) {
    const backendURL = this.envService.values.api;
    let apiURL;
    if (apiURLObj !== undefined) {
      if (typeof apiURLObj === "string") {
        apiURL = apiURLObj;
      }
    } else {
      apiURL = backendURL + "/" + apiController + "/" + apiMethod;
    }
    if (apiParams === undefined) {
      apiParams = {};
    }
    for (var key in this.appStateService.importantqueryparamsmaster) {
      var value = this.appStateService.importantqueryparamsmaster[key];
      if (this.appStateService.importantqueryparams[value] !== undefined) {
        apiParams[value] = this.appStateService.importantqueryparams[value];
      }
    }
    return this.http.post<any>(apiURL, apiParams);
  }

  post_api_alt(apiURL: string, postData: any, headers?) {
    if (headers === undefined) {
      headers = {};
    }
    const httpClient = new HttpClient(this.handler);
    return httpClient.put(apiURL, postData, {
      headers,
      reportProgress: true,
      observe: "events",
    });
  }
  get_api_alt(url) {
    const httpClient = new HttpClient(this.handler);
    return httpClient.get(url);
  }
  announceApiStateChange(): void {
    this.state.next("");
  }
  detectApiStateChange(): Observable<any> {
    return this.state.asObservable();
  }
  updateApiStateChange(event: HttpEvent<any>): HttpEvent<any> {
    this.latestApiEvent = event;
    if (this.latestApiEvent.type === HttpEventType.Sent) {
      this.latestApiEventType = "SENT";
    } else if (this.latestApiEvent.type === HttpEventType.UploadProgress) {
      this.latestApiEventType = "UPLOADINPROGRESS";
    } else if (this.latestApiEvent.type === HttpEventType.Response) {
      this.latestApiEventType = "RESPONSE";
    }
    this.announceApiStateChange();
    if (event["body"]) {
      this.appStateService.setResponseObjAsAppState(event["body"].appstate);
      if (!event["body"].total) {
        event["body"] = event["body"].response;
      }
    }
    try {
      if (this.appStateService.usertoken === undefined && this.appStateService.appState && this.appStateService.appState.is_mobile_app && this.appStateService.appState.user && this.appStateService.appState.user.id) {
        this.appStateService.updateTokenInApp();
        this.appStateService.usertoken = "";
        setTimeout(() => {
          this.appStateService.getTokenFromApp();
          console.log("this.appStateService.usertoken", this.appStateService.usertoken);
          this.post_api(this.envService.values.updateusertoke_controller, this.envService.values.updateusertoke_method, { user_id: this.appStateService.appState.user.id, usertoken: this.appStateService.usertoken }).subscribe((res) => {});
        }, 10000);
      }
    } catch (error) {}
    return event;
  }
  public isLoading = new BehaviorSubject(false);
}
