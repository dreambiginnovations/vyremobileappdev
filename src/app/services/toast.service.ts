import { Injectable } from "@angular/core";
import { DialogService } from "./dialog.service";

@Injectable({
  providedIn: "root",
})
export class ToastService {
  constructor(private dialogService: DialogService) {}

  showSuccess(title, message, timeOut?) {
    if (timeOut === undefined) {
      timeOut = 3000;
    }
    /*this.dialogService.success(message, title, {
      positionClass: "toast-top-center",
      timeOut,
    });*/
    this.dialogService.showMessageDialog(message, title);
  }

  showError(title, message, timeOut?) {
    if (timeOut === undefined) {
      timeOut = 3000;
    }
    /*this.dialogService.error(message, title, {
      positionClass: "toast-top-center",
      timeOut,
    });*/
    this.dialogService.showMessageDialog(message, title);
  }

  showWarning(title, message, timeOut?) {
    if (timeOut === undefined) {
      timeOut = 3000;
    }
    /*this.dialogService.warning(message, title, {
      positionClass: "toast-top-center",
      timeOut,
    });*/
    this.dialogService.showMessageDialog(message, title);
  }
}
