import { Injectable, Inject } from "@angular/core";
import { DOCUMENT } from "@angular/common";

@Injectable({
  providedIn: "root",
})
export class JsonLDService {
  static scriptType = "application/ld+json";
  constructor(@Inject(DOCUMENT) private _document: Document) {}

  removeStructuredData(): void {
    const els = [];
    ["structured-data", "structured-data-org"].forEach((c) => {
      els.push(...Array.from(this._document.head.getElementsByClassName(c)));
    });
    els.forEach((el) => this._document.head.removeChild(el));
  }

  insertSchema(schema: Record<string, any>, className = "structured-data"): void {
    let script;
    let shouldAppend = false;
    if (this._document.head.getElementsByClassName(className).length) {
      script = this._document.head.getElementsByClassName(className)[0];
    } else {
      script = this._document.createElement("script");
      shouldAppend = true;
    }
    script.setAttribute("class", className);
    script.type = JsonLDService.scriptType;
    script.text = JSON.stringify(schema);
    if (shouldAppend) {
      this._document.head.prepend(script);
    }
  }

  formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  }

  createJobSchemaFromJob(jobObj) {
    var date = new Date(jobObj["created_at"]);
    var newDate = new Date(date.setMonth(date.getMonth() + 6));
    jobObj["valid_through"] = this.formatDate(newDate) + " 00:00:00";

    var jobSchema = {
      "@context": "https://schema.org/",
      "@type": "JobPosting",
      title: jobObj["job_title"],
      description: jobObj["job_description"],
      identifier: {
        "@type": "PropertyValue",
        name: "SWAAYAM",
        value: jobObj["id"],
      },
      datePosted: jobObj["created_at"],
      validThrough: jobObj["valid_through"],
      employmentType: "FULL_TIME",
      hiringOrganization: {
        "@type": "Organization",
        name: jobObj["company_name"],
        logo: jobObj["company_logo_url"],
      },
      applicantLocationRequirements: {
        "@type": "Country",
        name: "INDIA",
      },
      baseSalary: {
        "@type": "MonetaryAmount",
        currency: "INR",
        value: {
          "@type": "QuantitativeValue",
          value: jobObj["min_job_salary"],
          unitText: "MONTH",
        },
      },
    };

    if (jobObj["is_remote_job"] === "Yes") {
      jobSchema["applicantLocationRequirements"] = {
        "@type": "Country",
        name: "INDIA",
      };
      jobSchema["jobLocationType"] = "TELECOMMUTE";
    } else {
      jobSchema["jobLocation"] = {
        "@type": "Place",
        address: {
          "@type": "PostalAddress",
          streetAddress: jobObj["job_location"],
          addressLocality: jobObj["job_location_city"],
          addressRegion: jobObj["job_location_state"],
          addressCountry: jobObj["job_location_country"],
          postalCode: "560001",
        },
      };
    }
    return jobSchema;
  }
}
