"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var DomService = /** @class */ (function () {
    function DomService() {
        this.DEVICETYPES = {
            MOBILE: 1,
            OTHERS: 2
        };
        this.STDSIZES = {
            XS: { width: "360px" },
            SM: { width: "576px" },
            MD: { width: "768px" },
            LG: { width: "992px" },
            XL: { width: "1200px" }
        };
    }
    DomService.prototype.appInitialize = function () {
        var smWidth = this.STDSIZES.SM.width.replace("px", "");
        if (screen.width < parseInt(smWidth)) {
            this.device = this.DEVICETYPES.MOBILE;
        }
        else {
        }
        console.log("Domservice Intialized " + screen.width);
    };
    DomService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], DomService);
    return DomService;
}());
exports.DomService = DomService;
