"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
var ApiService = /** @class */ (function () {
    function ApiService(http, envService, handler, appStateService) {
        this.http = http;
        this.envService = envService;
        this.handler = handler;
        this.appStateService = appStateService;
        this.state = new rxjs_1.Subject();
        this.isLoading = new rxjs_1.BehaviorSubject(false);
    }
    ApiService.prototype.post_api = function (apiController, apiMethod, apiParams, apiURLObj) {
        var backendURL = this.envService.values.api;
        var apiURL;
        if (apiURLObj !== undefined) {
            if (typeof apiURLObj === "string") {
                apiURL = apiURLObj;
            }
        }
        else {
            apiURL = backendURL + "/" + apiController + "/" + apiMethod;
        }
        if (apiParams === undefined) {
            apiParams = {};
        }
        for (var key in this.appStateService.importantqueryparamsmaster) {
            var value = this.appStateService.importantqueryparamsmaster[key];
            if (this.appStateService.importantqueryparams[value] !== undefined) {
                apiParams[value] = this.appStateService.importantqueryparams[value];
            }
        }
        return this.http.post(apiURL, apiParams);
    };
    ApiService.prototype.post_api_alt = function (apiURL, postData, headers) {
        if (headers === undefined) {
            headers = {};
        }
        var httpClient = new http_1.HttpClient(this.handler);
        return httpClient.put(apiURL, postData, {
            headers: headers,
            reportProgress: true,
            observe: "events"
        });
    };
    ApiService.prototype.get_api_alt = function (url) {
        var httpClient = new http_1.HttpClient(this.handler);
        return httpClient.get(url);
    };
    ApiService.prototype.announceApiStateChange = function () {
        this.state.next("");
    };
    ApiService.prototype.detectApiStateChange = function () {
        return this.state.asObservable();
    };
    ApiService.prototype.updateApiStateChange = function (event) {
        var _this = this;
        this.latestApiEvent = event;
        if (this.latestApiEvent.type === http_1.HttpEventType.Sent) {
            this.latestApiEventType = "SENT";
        }
        else if (this.latestApiEvent.type === http_1.HttpEventType.UploadProgress) {
            this.latestApiEventType = "UPLOADINPROGRESS";
        }
        else if (this.latestApiEvent.type === http_1.HttpEventType.Response) {
            this.latestApiEventType = "RESPONSE";
        }
        this.announceApiStateChange();
        if (event["body"]) {
            this.appStateService.setResponseObjAsAppState(event["body"].appstate);
            if (!event["body"].total) {
                event["body"] = event["body"].response;
            }
        }
        try {
            if (this.appStateService.usertoken === undefined && this.appStateService.appState && this.appStateService.appState.is_mobile_app && this.appStateService.appState.user && this.appStateService.appState.user.id) {
                this.appStateService.updateTokenInApp();
                this.appStateService.usertoken = "";
                setTimeout(function () {
                    _this.appStateService.getTokenFromApp();
                    console.log("this.appStateService.usertoken", _this.appStateService.usertoken);
                    _this.post_api(_this.envService.values.updateusertoke_controller, _this.envService.values.updateusertoke_method, { user_id: _this.appStateService.appState.user.id, usertoken: _this.appStateService.usertoken }).subscribe(function (res) { });
                }, 10000);
            }
        }
        catch (error) { }
        return event;
    };
    ApiService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], ApiService);
    return ApiService;
}());
exports.ApiService = ApiService;
