import { Injectable } from "@angular/core";

import { EnvService } from "./env.service"; // just caling here gets it initialized
import { DomService } from "./dom.service";
import { ApiService } from "./api.service";
import { AppstateService } from "./appstate.service";

@Injectable()
export class AppinitializeService {
  constructor(private envService: EnvService, private domService: DomService, private apiService: ApiService, private appStateService: AppstateService) {}

  initializeApp(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.domService.appInitialize();
      this.apiService.post_api("backend", "getAppState").subscribe((res) => {
        // appstate will get automatically set due to httpinterceptor
        resolve();
      });
    });
  }
}
