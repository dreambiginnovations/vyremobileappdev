"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var LazyloadService = /** @class */ (function () {
    function LazyloadService() {
    }
    LazyloadService.prototype.importProgressBarModule = function () {
        return Promise.resolve().then(function () { return require("../common/progressbar/progressbar.module"); }).then(function (mod) { return mod.ProgressBarModule; });
    };
    LazyloadService.prototype.importNavbarexpandedcandidateModule = function () {
        return Promise.resolve().then(function () { return require("../navbarexpandedcandidate/navbarexpandedcandidate.module"); }).then(function (mod) { return mod.NavbarexpandedcandidateModule; });
    };
    LazyloadService.prototype.importNavbarexpandedemployerModule = function () {
        return Promise.resolve().then(function () { return require("../navbarexpandedemployer/navbarexpandedemployer.module"); }).then(function (mod) { return mod.NavbarexpandedemployerModule; });
    };
    LazyloadService.prototype.importVjsPlayerModule = function () {
        return Promise.resolve().then(function () { return require("../common/videoplay/vjs-player.module"); }).then(function (mod) { return mod.VjsPlayerModule; });
    };
    LazyloadService.prototype.importCandidateSearchJobsFilterModule = function () {
        return Promise.resolve().then(function () { return require("../candidate_search_jobs_filter/candidate_search_jobs_filter.module"); }).then(function (mod) { return mod.CandidateSearchJobsFilterModule; });
    };
    LazyloadService.prototype.importCandidateSearchJobsSortModule = function () {
        return Promise.resolve().then(function () { return require("../candidate_search_jobs_sort/candidate_search_jobs_sort.module"); }).then(function (mod) { return mod.CandidateSearchJobsSortModule; });
    };
    LazyloadService.prototype.importEmployerJobApplicationsFilterModule = function () {
        return Promise.resolve().then(function () { return require("../employer_job_applications_filter/employer_job_applications_filter.module"); }).then(function (mod) { return mod.EmployerJobApplicationsFilterModule; });
    };
    LazyloadService.prototype.importEmployerJobSeekersFilterModule = function () {
        return Promise.resolve().then(function () { return require("../employer_job_seekers_filter/employer_job_seekers_filter.module"); }).then(function (mod) { return mod.EmployerJobSeekersFilterModule; });
    };
    LazyloadService.prototype.importCandidateNologinFormModule = function () {
        return Promise.resolve().then(function () { return require("../candidate_nologin_form/candidate_nologin_form.module"); }).then(function (mod) { return mod.CandidateNologinFormModule; });
    };
    LazyloadService.prototype.importSharepopupModule = function () {
        return Promise.resolve().then(function () { return require("../common/sharepopup/sharepopup.module"); }).then(function (mod) { return mod.SharepopupModule; });
    };
    LazyloadService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], LazyloadService);
    return LazyloadService;
}());
exports.LazyloadService = LazyloadService;
