"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var CommunicationService = /** @class */ (function () {
    function CommunicationService() {
        this.state = new rxjs_1.Subject();
        this.communication = { context: undefined, obj: undefined };
    }
    CommunicationService.prototype.announceNewCommunication = function () {
        this.state.next("");
    };
    CommunicationService.prototype.detectNewCommunication = function () {
        return this.state.asObservable();
    };
    CommunicationService.prototype.broadcastCommunication = function (newCommunication) {
        this.communication = newCommunication;
        this.announceNewCommunication();
    };
    CommunicationService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], CommunicationService);
    return CommunicationService;
}());
exports.CommunicationService = CommunicationService;
