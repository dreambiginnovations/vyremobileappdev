import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

declare function native_updateAppToken(): any;
declare function native_getAppToken(): any;

@Injectable({
  providedIn: "root",
})
export class AppstateService {
  public hasunreadnotifications = true;
  public appState:any = {};
  public guided_recording: any;
  public central_upload_obj: any = {uploadInProgress:false};
  public postloginstate;
  public usertoken;
  public tempdata: any;
  public importantqueryparams: any = {};
  public importantqueryparamsmaster = ["SUPER__campaign", "SUPER__medium", "SUPER__source"];
  constructor() {}

  private state = new Subject<any>();
  announceAppStateChange(): void {
    this.state.next("");
  }
  detectAppStateChange(): Observable<any> {
    return this.state.asObservable();
  }
  setResponseObjAsAppState(newAppState: any): void {
    if (this.appState === undefined || JSON.stringify(this.appState) !== JSON.stringify(newAppState)) {
      this.appState = Object.assign({}, newAppState);
      this.announceAppStateChange();
    }
  }
  getTokenFromApp() {
    this.usertoken = native_getAppToken();
  }
  updateTokenInApp() {
    native_updateAppToken();
  }
}
