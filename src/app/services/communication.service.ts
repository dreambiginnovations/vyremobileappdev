import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class CommunicationService {
  private state = new Subject<any>();

  communication = { context: undefined, obj: undefined };
  private announceNewCommunication() {
    this.state.next("");
  }
  detectNewCommunication(): Observable<any> {
    return this.state.asObservable();
  }
  broadcastCommunication(newCommunication: COMMUNICATION): void {
    this.communication = newCommunication;
    this.announceNewCommunication();
  }
}

export interface COMMUNICATION {
  context: { parentid?: string | number; communicationid?: number };
  obj: any;
}
