import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
// import { NgDialogAnimationService } from 'ng-dialog-animation';

import { DialogComponent } from "../common/dialog/dialog.component";
import { DomService } from "./dom.service";

import { ImageCropperDialogComponent } from "../common/imagecropper/imagecropperdialog.component";
import { VideoRecordDialogComponent } from "../common/videorecord/videorecord.component";
import { VjsPlayerComponent } from "../common/videoplay/vjs-player.component";
import { AlertComponent } from "../common/dialog/alert.component";

@Injectable({
  providedIn: "root",
})
export class DialogService {
  DIALOGTYPES = {
    STANDARD: 1,
    XS: 2,
    LG: 3,
  };

  constructor(private dialog: MatDialog, private domService: DomService) {}

  dismissOpenDialogs() {
    this.dialog.closeAll();
  }

  showMessageDialog(message: string, title?: string) {
    const __hostclasses = "alertdialog";
    const dialogRef = this.dialog.open(AlertComponent, {
      panelClass: __hostclasses,
      data: { message: message, title: title },
    });
  }

  openComponent(componentName: string, moduleName: string, dialogType?: number, componentContext?: any, panelClass?: string) {
    const dialogContext = this.createDialogContext(dialogType, panelClass);
    const dialogRef = this.dialog.open(DialogComponent, {
      width: dialogContext.width,
      position: dialogContext.position,
      maxWidth: dialogContext.maxWidth,
      maxHeight: dialogContext.maxHeight,
      data: { componentContext, componentName, moduleName },
      panelClass: dialogContext.panelClass,
    });
    return dialogRef;
  }

  openImageCropper(parentId, imageChangedEvent, isRoundCropper) {
    const __hostclasses = "imagecropper";
    const dialogRef = this.dialog.open(ImageCropperDialogComponent, {
      maxHeight: 420,
      data: { parentId, imageChangedEvent, roundCropper: isRoundCropper },
      panelClass: __hostclasses,
    });
  }

  openVideoUploader(parentId) {
    const __hostclasses = "videoupload";
    this.dialog.open(VideoRecordDialogComponent, {
      height: "100%",
      data: { parentId },
      panelClass: __hostclasses,
    });
  }

  openVideoDisplay(videoURL: string, videoFrame: string) {
    const __hostclasses = "videoplayer";
    this.openComponent(
      "VjsPlayerComponent",
      "VjsPlayerModule",
      this.DIALOGTYPES.LG,
      {
        autoplay: true,
        fluid: true,
        live: false,
        poster: videoFrame,
        sources: [{ src: videoURL, type: "video/webm" }],
      },
      __hostclasses
    );
  }

  createDialogContext(dialogType, panelClass): DialogContext {
    if (dialogType === undefined) {
      dialogType = this.DIALOGTYPES.STANDARD;
    }
    let width;
    let position;
    let maxHeight;
    const maxWidth = "100%";
    if (dialogType === this.DIALOGTYPES.STANDARD) {
      if (this.domService.device === this.domService.DEVICETYPES.MOBILE) {
        width = "100%";
        position = { top: "auto", bottom: "0px", left: "0px", right: "0px" };
        maxHeight = "75%";
      } else {
        width = "80%";
        position = undefined;
        maxHeight = "auto";
      }
    } else if (dialogType === this.DIALOGTYPES.XS) {
      if (this.domService.device === this.domService.DEVICETYPES.MOBILE) {
        width = "100%";
        position = { top: "auto", bottom: "0px", left: "0px", right: "0px" };
        maxHeight = "75%";
      } else {
        width = this.domService.STDSIZES.XS.width;
        position = { top: "auto", bottom: "auto", left: "auto", right: "auto" };
        maxHeight = "auto";
      }
    } else if (dialogType === this.DIALOGTYPES.LG) {
      if (this.domService.device === this.domService.DEVICETYPES.MOBILE) {
        width = "100%";
        position = { top: "auto", bottom: "0px", left: "0px", right: "0px" };
        maxHeight = "100%";
      } else {
        width = this.domService.STDSIZES.XS.width;
        position = undefined;
        maxHeight = "auto";
      }
    }
    const dialogTypeObj: DialogContext = {
      panelClass: panelClass === undefined ? "" : panelClass,
      position,
      width,
      maxWidth,
      maxHeight,
    };
    return dialogTypeObj;
  }
}

export interface DialogContext {
  width: string;
  position: {
    top?: string;
    bottom?: string;
    left?: string;
    right?: string;
  };
  panelClass: string;
  height?: string;
  maxHeight?: number | string;
  maxWidth?: number | string;
}
