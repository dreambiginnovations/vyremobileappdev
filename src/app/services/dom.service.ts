import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class DomService {
  public device: number;
  public DEVICETYPES = {
    MOBILE: 1,
    OTHERS: 2,
  };
  public STDSIZES = {
    XS: { width: "360px" },
    SM: { width: "576px" },
    MD: { width: "768px" },
    LG: { width: "992px" },
    XL: { width: "1200px" },
  };

  appInitialize() {
    let smWidth = this.STDSIZES.SM.width.replace("px", "");
    if (screen.width < parseInt(smWidth)) {
      this.device = this.DEVICETYPES.MOBILE;
    } else {
    }
    console.log("Domservice Intialized " + screen.width);
  }
}
