"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var rxjs_1 = require("rxjs");
var AppstateService = /** @class */ (function () {
    function AppstateService() {
        this.hasunreadnotifications = true;
        this.appState = {};
        this.central_upload_obj = { uploadInProgress: false };
        this.importantqueryparams = {};
        this.importantqueryparamsmaster = ["SUPER__campaign", "SUPER__medium", "SUPER__source"];
        this.state = new rxjs_1.Subject();
    }
    AppstateService.prototype.announceAppStateChange = function () {
        this.state.next("");
    };
    AppstateService.prototype.detectAppStateChange = function () {
        return this.state.asObservable();
    };
    AppstateService.prototype.setResponseObjAsAppState = function (newAppState) {
        if (this.appState === undefined || JSON.stringify(this.appState) !== JSON.stringify(newAppState)) {
            this.appState = Object.assign({}, newAppState);
            this.announceAppStateChange();
        }
    };
    AppstateService.prototype.getTokenFromApp = function () {
        this.usertoken = native_getAppToken();
    };
    AppstateService.prototype.updateTokenInApp = function () {
        native_updateAppToken();
    };
    AppstateService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], AppstateService);
    return AppstateService;
}());
exports.AppstateService = AppstateService;
