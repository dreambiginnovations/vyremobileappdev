"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var AWSService = /** @class */ (function () {
    function AWSService(apiService, dialogService, communicationService, appstateService, envService, router, toastService) {
        this.apiService = apiService;
        this.dialogService = dialogService;
        this.communicationService = communicationService;
        this.appstateService = appstateService;
        this.envService = envService;
        this.router = router;
        this.toastService = toastService;
        this.uploadInProgress = false;
    }
    AWSService.prototype.prepareToUploadFileToS3 = function (fileName, fileCategory, isNewFile) {
        var postParams = { fileName: fileName, fileCategory: fileCategory, isNewFile: isNewFile };
        return this.apiService.post_api("common", "aws_put_createpresignedurl", postParams);
    };
    AWSService.prototype.uploadBase64ImageToS3 = function (params) {
        console.log("params", params);
        var url = params.url;
        var fileContent = params.fileContent;
        var headers = params.headers;
        console.log(params);
        var data = fileContent.replace(/^data:image\/\w+;base64,/, "");
        var buff = new Buffer(data, "base64");
        this.uploadWithProgress(url, buff.buffer, headers, "Uploading");
    };
    AWSService.prototype.uploadFileToS3 = function (params) {
        var url = params.url;
        var fileContent = params.fileContent;
        var headers = params.headers;
        console.log(params);
        this.uploadWithProgress(url, fileContent, headers, "Uploading");
    };
    AWSService.prototype.uploadWithProgress = function (url, content, headers, uploadProgressTitle) {
        var _this = this;
        this.uploadInProgress = true;
        var dialogRef = this.dialogService.openComponent("ProgressBarComponent", "ProgressBarModule");
        this.apiService.post_api_alt(url, content, headers).subscribe(function (event) {
            switch (event.type) {
                case http_1.HttpEventType.Sent:
                    console.log("Request has been made!");
                    _this.communicationService.broadcastCommunication({
                        context: { parentid: "progressbarparent" },
                        obj: { title: uploadProgressTitle, progress: 0 }
                    });
                    break;
                case http_1.HttpEventType.ResponseHeader:
                    console.log("Response header has been received!", event);
                    break;
                case http_1.HttpEventType.UploadProgress:
                    _this.progress = Math.round((event.loaded / event.total) * 100);
                    console.log("Uploaded! " + _this.progress + "%");
                    _this.communicationService.broadcastCommunication({
                        context: { parentid: "progressbarparent" },
                        obj: { title: uploadProgressTitle, progress: _this.progress }
                    });
                    break;
                case http_1.HttpEventType.Response:
                    console.log("Upload complete");
                    _this.uploadInProgress = false;
                    dialogRef.close();
                    setTimeout(function () {
                        _this.progress = 0;
                    }, 500);
            }
        });
    };
    AWSService.prototype.deleteExistingFile = function (fileName, fileCategory) {
        this.apiService.post_api("common", "deleteobject", { fileName: fileName, fileCategory: fileCategory }).subscribe(function (res) { });
    };
    AWSService.prototype.handleCandidateGuideProfileVideoUpload = function () {
        var _this = this;
        this.currentUserMediaID = this.appstateService.appState.user.video_id;
        var currentUserMobile = this.appstateService.appState.user.mobile;
        this.appstateService.central_upload_obj.uploadInProgress = true;
        this.apiService.post_api("jobportal", "insertEmptyMedia", { media_type: "video_playlist" }).subscribe(function (mediaRes) {
            var newUserMediaID = mediaRes.inserted_entity_id;
            _this.apiService.post_api("jobportal", "updatecandidatecandidate", { video_id: newUserMediaID, mobile: currentUserMobile }).subscribe(function (res) {
                _this.handleCandidateGuideProfileVideoUpload_playlist(newUserMediaID);
            });
        });
    };
    AWSService.prototype.handleCandidateGuideProfileVideoUpload_playlist = function (newUserMediaID) {
        this.handleCandidateGuideProfileVideoUpload_playlist_item(0, newUserMediaID);
    };
    AWSService.prototype.handleCandidateGuideProfileVideoUpload_playlist_item = function (count, userPlayListId) {
        var _this = this;
        this.appstateService.central_upload_obj.uploadingTitle = "Preparing to upload for: " + this.appstateService.guided_recording["recording_" + count]["question"];
        this.apiService.post_api("jobportal", "insertEmptyMedia", { media_type: "video", playlist_id: userPlayListId }).subscribe(function (mediaRes) {
            var insertedVideoId = mediaRes.inserted_entity_id;
            var d = new Date();
            var time = d.getTime();
            var frameFileName = "frame_media_" + _this.appstateService.appState.user.id + "_" + time + ".png";
            var videoFileName = "media_" + _this.appstateService.appState.user.id + "_" + time + ".webm";
            var fileCategory = "userprofile__video";
            _this.prepareToUploadFileToS3(frameFileName, fileCategory, true).subscribe(function (frameUploadRes) {
                var mediaFrameFileURL = _this.envService.values.ugcmediaurl + frameFileName;
                var mediaVideoFileURL = _this.envService.values.ugcmediaurl + videoFileName;
                _this.apiService.post_api("jobportal", "updateMedia", { id: insertedVideoId, media_frame_url: mediaFrameFileURL, media_file_internal_name: videoFileName, media_file_url: mediaVideoFileURL, media_file_external_name: _this.appstateService.guided_recording["recording_" + count]["question"] }).subscribe(function (dummyRes) {
                    var params = {
                        url: frameUploadRes.url,
                        fileContent: _this.appstateService.guided_recording["recording_" + count]["frame"],
                        headers: frameUploadRes.headers
                    };
                    var url = params.url;
                    var fileContent = params.fileContent;
                    var headers = params.headers;
                    var data = fileContent.replace(/^data:image\/\w+;base64,/, "");
                    var buff = new Buffer(data, "base64");
                    _this.apiService.post_api_alt(url, buff.buffer, headers).subscribe(function (frameuploadevent) {
                        switch (frameuploadevent.type) {
                            case http_1.HttpEventType.Sent:
                                console.log("Frame Request has been made!", count);
                                _this.appstateService.central_upload_obj.uploadingProgress = 0;
                                break;
                            case http_1.HttpEventType.ResponseHeader:
                                console.log("Frame Response header has been received!", frameuploadevent, count);
                                break;
                            case http_1.HttpEventType.UploadProgress:
                                var progress = Math.round((frameuploadevent.loaded / frameuploadevent.total) * 100);
                                console.log("Frame Uploaded! " + progress + "%", count);
                                _this.appstateService.central_upload_obj.uploadingProgress = progress;
                                break;
                            case http_1.HttpEventType.Response:
                                console.log("Frame Upload complete", count);
                                _this.appstateService.central_upload_obj.uploadingProgress = 0;
                                _this.prepareToUploadFileToS3(videoFileName, fileCategory, true).subscribe(function (videoFileUploadRes) {
                                    var params = {
                                        url: videoFileUploadRes.url,
                                        fileContent: _this.appstateService.guided_recording["recording_" + count]["video"],
                                        headers: videoFileUploadRes.headers
                                    };
                                    _this.appstateService.central_upload_obj.uploadingTitle = "Uploading video for: " + _this.appstateService.guided_recording["recording_" + count]["question"];
                                    _this.apiService.post_api_alt(params.url, params.fileContent, params.headers).subscribe(function (videouploadevent) {
                                        switch (videouploadevent.type) {
                                            case http_1.HttpEventType.Sent:
                                                console.log("Video Request has been made!", count);
                                                _this.appstateService.central_upload_obj.uploadingProgress = 0;
                                                break;
                                            case http_1.HttpEventType.ResponseHeader:
                                                console.log("Video Response header has been received!", videouploadevent, count);
                                                break;
                                            case http_1.HttpEventType.UploadProgress:
                                                var progress = Math.round((videouploadevent.loaded / videouploadevent.total) * 100);
                                                console.log("Video Uploaded! " + progress + "%", count);
                                                _this.appstateService.central_upload_obj.uploadingProgress = progress;
                                                break;
                                            case http_1.HttpEventType.Response:
                                                console.log("Video Upload complete", count);
                                                //this.appstateService.central_upload_obj.uploadingProgress = 0;
                                                count++;
                                                if (_this.appstateService.guided_recording["recording_" + count] === undefined) {
                                                    _this.appstateService.central_upload_obj.uploadingTitle = "Deleting previous videos.";
                                                    _this.deleteExistingCandidateProfileVideoData(_this.currentUserMediaID);
                                                    _this.notifyEmployersOfVideoUpload();
                                                }
                                                else {
                                                    setTimeout(function () {
                                                        _this.handleCandidateGuideProfileVideoUpload_playlist_item(count, userPlayListId);
                                                    }, 500);
                                                }
                                        }
                                    });
                                });
                        }
                    });
                });
            });
        });
    };
    AWSService.prototype.notifyEmployersOfVideoUpload = function () {
        this.apiService.post_api("jobportal", "notifyemployersofvideoupload", { id: this.appstateService.appState.user.id }).subscribe(function (res) { });
    };
    AWSService.prototype.deleteExistingCandidateProfileVideoData = function (mediaID) {
        var _this = this;
        this.apiService.post_api("jobportal", "markallmediafordelete", { media_id: mediaID }).subscribe(function (mediaRes) {
            _this.toastService.showSuccess("Video Updated Successfully!", "Your video is updated successfully. It will be processed and ready in a few minutes.");
            setTimeout(function () {
                _this.appstateService.central_upload_obj.uploadInProgress = false;
                _this.router.navigate(['/candidate/searchjobs']);
            }, 1000);
        });
    };
    AWSService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], AWSService);
    return AWSService;
}());
exports.AWSService = AWSService;
