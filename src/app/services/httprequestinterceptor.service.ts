import { Injectable } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { tap } from "rxjs/operators";

import { ApiService } from "./api.service";
import { AppstateService } from "./appstate.service";
import { EnvService } from "./env.service";
import { DummyAPIService } from "./dummyapi.service";

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  private requests: HttpRequest<any>[] = [];
  isDummyAPIEnabled = false;
  constructor(private apiService: ApiService, private appstateService: AppstateService, private envService: EnvService, private dummyAPIService: DummyAPIService) {
    this.isDummyAPIEnabled = this.envService.values["dummyapi"];
  }

  removeRequest(req: HttpRequest<any>) {
    const i = this.requests.indexOf(req);
    if (i >= 0) {
      this.requests.splice(i, 1);
    }
    this.apiService.isLoading.next(this.requests.length > 0);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(this.isDummyAPIEnabled) {
      var dummyResponse = this.dummyAPIService.processRequestForDummyResponse(req);
      if(dummyResponse!=undefined) {
        var response:any = {};
        if(dummyResponse.appstate) {
          response = {body: dummyResponse};
          this.apiService.updateApiStateChange(response);
          return of(new HttpResponse(response));
        } 
        console.log("here2");
        response.body = {};
        response.body.appstate = this.appstateService.appState;
        response.body.response = dummyResponse;
        console.log("Returning dummy response", response);
        this.apiService.updateApiStateChange(response);
        return of(new HttpResponse(response));
      }
    }
    req = req.clone({
      withCredentials: true,
      reportProgress: true,
    });
    this.requests.push(req);
    this.apiService.isLoading.next(true);
    return next.handle(req).pipe(
      tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            this.removeRequest(req);
          }
          event = this.apiService.updateApiStateChange(event);
        },
        (err: any) => {
          this.removeRequest(req);
          console.log("Got an error in HTTP Request", err);
        }
      )
    );
  }
}
