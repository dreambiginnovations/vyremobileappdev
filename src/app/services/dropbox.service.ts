import { Injectable } from "@angular/core";
import { HttpEvent, HttpEventType } from "@angular/common/http";

import { EnvService } from "./env.service";
import { ApiService } from "./api.service";
import { DialogService } from "./dialog.service";
import { CommunicationService } from "./communication.service";

@Injectable({
  providedIn: "root",
})
export class DropboxService {
  constructor(private apiService: ApiService, private dialogService: DialogService, private communicationService: CommunicationService) {}

  progress;
  public uploadInProgress = false;

  prepareToUploadFile(fileName: string, fileCategory: string, isNewFile: boolean) {
    const postParams = { fileName, fileCategory, isNewFile };
    return this.apiService.post_api("common", "dropbox_getuploadtoken", postParams);
  }

  uploadBase64Image(params) {
    console.log("params", params);
    const url = params.url;
    const fileContent = params.fileContent;
    const headers = params.headers;
    console.log(params);
    const data = fileContent.replace(/^data:image\/\w+;base64,/, "");
    const buff = new Buffer(data, "base64");
    this.uploadWithProgress(url, buff.buffer, headers, "Uploading");
  }

  uploadFile(params) {
    const url = params.url;
    const fileContent = params.fileContent;
    const headers = params.headers;
    console.log(params);
    this.uploadWithProgress(url, fileContent, headers, "Uploading");
  }
  uploadWithProgress(url, content, headers, uploadProgressTitle) {
    this.uploadInProgress = true;
    const dialogRef = this.dialogService.openComponent("ProgressBarComponent", "ProgressBarModule");
    this.apiService.post_api_alt(url, content, headers).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          console.log("Request has been made!");
          this.communicationService.broadcastCommunication({
            context: { parentid: "progressbarparent" },
            obj: { title: uploadProgressTitle, progress: 0 },
          });
          break;
        case HttpEventType.ResponseHeader:
          console.log("Response header has been received!");
          break;
        case HttpEventType.UploadProgress:
          this.progress = Math.round((event.loaded / event.total) * 100);
          console.log(`Uploaded! ${this.progress}%`);
          this.communicationService.broadcastCommunication({
            context: { parentid: "progressbarparent" },
            obj: { title: uploadProgressTitle, progress: this.progress },
          });
          break;
        case HttpEventType.Response:
          this.uploadInProgress = false;
          dialogRef.close();
          setTimeout(() => {
            this.progress = 0;
          }, 500);
      }
    });
  }

  deleteExistingFile(fileName, fileCategory) {
    this.apiService.post_api("common", "deleteobject", { fileName, fileCategory }).subscribe((res) => {});
  }
}
