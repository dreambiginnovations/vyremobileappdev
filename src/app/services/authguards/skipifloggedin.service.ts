import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AppstateService } from "../appstate.service";

@Injectable({ providedIn: "root" })
export class SkipIfAlreadyLoggedIn implements CanActivate {
  constructor(private _router: Router, private appstateService: AppstateService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.appstateService.appState.user && this.appstateService.appState.user.id) {
      if (this.appstateService.appState.user.candidate_id !== null) {
        if (this.appstateService.appState.user.is_basic_complete) {
          this._router.navigate(["candidate/home"]);
        } else {
          this._router.navigate(["candidate/insuff_profile"]);
        }
      } else if (this.appstateService.appState.user.employer_id !== null) {
        if (this.appstateService.appState.user.is_basic_complete) {
          this._router.navigate(["employer/home"]);
        } else {
          this._router.navigate(["employer/insuff_profile"]);
        }
      } else {
        this._router.navigate(["choose_login_type"]);
      }
      return false;
    }
    return true;
  }
}
