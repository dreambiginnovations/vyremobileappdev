import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AppstateService } from "../appstate.service";

@Injectable({ providedIn: "root" })
export class AllowForEmployerLogin implements CanActivate {
  constructor(private _router: Router, private appstateService: AppstateService) {}
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.appstateService.appState && this.appstateService.appState.user && this.appstateService.appState.user.id && this.appstateService.appState.user.employer_id !== null) {
      return true;
    } else {
      var navigateURL = "login_signup";
      if (this.appstateService.appState && this.appstateService.appState.user && this.appstateService.appState.user.id) {
        if (this.appstateService.appState.user.candidate_id !== null) {
          if (this.appstateService.appState.user.is_basic_complete) {
            navigateURL = "candidate/home";
          } else {
            navigateURL = "candidate/insuff_profile";
          }
        } else {
          navigateURL = "choose_login_type";
        }
        this.appstateService.postloginstate = undefined;
      } else {
        console.log("details", next, state);
        this.appstateService.postloginstate = undefined;
      }
      this._router.navigate([navigateURL], { queryParams: { SUPER__loadpagewithmessage: "ERROR! You need to be logged in as a Job Seeker to access this page." } });
      return false;
    }
  }
}
