import { Injectable } from "@angular/core";
import { Location } from "@angular/common";
import { TemplateutilsService } from "./templateutils.service";

@Injectable({
  providedIn: "root",
})
export class CommonutilsService {
  constructor(private templateutilsService: TemplateutilsService, private _location: Location) {}

  processSuccessState(stateObj) {
    return [stateObj.route, undefined];
  }

  goback() {
    this._location.back();
  }

  processResponse(res) {
    if (res.gotostate) {
      this.templateutilsService.navigateToRoute(res.gotostate, res.gotostaterouteparams, res.gotostatequeryparams);
    }
  }
}
