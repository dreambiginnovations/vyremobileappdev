import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { DialogService } from "./dialog.service";

@Injectable({
  providedIn: "root",
})
export class TemplateutilsService {
  constructor(private router: Router, private dialogService: DialogService) {}

  navigateToRoute(routeURL, routeParams?, queryParams?) {
    let nav = routeURL;
    if (routeParams === undefined) {
      this.router.navigate([nav], { queryParams });
    } else {
      this.router.navigate([nav, routeParams], { queryParams });
    }
  }

  navigateToURL(url) {
    window.location.href = url;
  }

  dismissAndNavigate(routeURL, routeParams?, queryParams?) {
    this.dialogService.dismissOpenDialogs();
    this.navigateToRoute(routeURL, routeParams, queryParams);
  }

  dismissDialogs() {
    this.dialogService.dismissOpenDialogs();
  }
}
