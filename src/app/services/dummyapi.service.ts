import { Injectable } from "@angular/core";
import { HttpRequest } from "@angular/common/http";

@Injectable()
export class DummyAPIService {

    getDummyResponseFromURL(pathname:string) {
        var response;
        if(pathname=="/backend/getAppState") {
            //no login case
            //response = this.dummy_api_responses.login.nologin;
            
            //login case
            response = this.dummy_api_responses.login.adminlogin;                
        }
        return response;
    }

    constructor() {}
    processRequestForDummyResponse(req: HttpRequest<any>) {
        var pathname = this.getAppStatePath(req.url);
        var response = this.getDummyResponseFromURL(pathname);
        return response;
    }
    getAppStatePath(url: string): string {
        const index = url.indexOf('index.php');
        if (index !== -1) {
            return url.substring(index + 9);
        }
        return url;
    }




    dummy_api_responses = {
        //login apis
        login: {
            nologin: {"appstate":{"__ci_last_regenerate":1718519139, "is_mobile_app":false}},
            adminlogin: {"appstate":{"__ci_last_regenerate":1718519139,"otp":9999,"userexists":true,"username":"ravi","current_location":"Jalandhar, Punjab, India","user":{"id":497062,"name":"ravi","mobile":9663089241,"candidate_id":490987,"candidate_status_id":null,"employer_id":null,"employer_status_id":null,"created_at":"2024-04-10 11:33:07","email":"","password":"","designation_id":0,"otp":null,"photo_id":null,"created_by_user_id":null,"created_by_usertype_id":null,"pan_card":"dfdfd","campaign":null,"source":"JOBS_PAGE","medium":null,"first_login_at":"2024-04-12 03:49:42","chosen_at":"2024-04-12 03:54:26","profile_completed_at":"2024-04-25 17:43:36","user_on_whatsapp":null,"current_temp_url":"497062605872","current_temp_url_created_at":"2024-06-16 11:58:22","manychat_subscriber_id":null,"current_location":"Jalandhar, Punjab, India","current_location_city":"Jalandhar","current_location_state":"Punjab","current_location_country":"India","current_latitude_location":"31.3260152","current_longitude_location":"75.5761829","temp_notificationsynced":0,"is_user_employee":0,"job_profile":null,"preferred_job_location":"Ernakulam, Kerala, India","preferred_job_location_city":"Ernakulam","preferred_job_location_state":"Kerala","preferred_job_location_country":"India","preferred_job_latitude_location":"9.9816358","preferred_job_longitude_location":"76.2998842","current_location_backup":null,"current_location_city_backup":null,"current_location_state_backup":null,"current_location_country_backup":null,"current_latitude_location_backup":null,"current_longitude_location_backup":null,"about_yourself":"","website_url":null,"linkedin_profile_link":null,"skype_id":null,"facebook":null,"twitter":null,"instagram":null,"youtube":null,"current_salary":"10000","job_category_id":12,"job_role_id":15,"skills":null,"dob":"1986-06-12","resume_id":null,"video_id":38804,"temp_deleted_video_id":null,"languages_known":null,"is_basic_complete":1,"profile_completion_percentage":100,"status_id":1,"resume_file_external_name":null,"resume_file_internal_name":null,"resume_file_url":null,"photo_id_media_file_internal_name":null,"video_id_media_file_internal_name":null,"video_id_media_file_url":"649dd9b3161de9\/0c9130517836a736","video_id_media_frame_url":null,"video_id_sprout_embedded_url":"","candidate_user_id":497062},"usertype":"candidate","is_mobile_app":false}},
        }
    }

}