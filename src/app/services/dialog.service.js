"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
// import { NgDialogAnimationService } from 'ng-dialog-animation';
var dialog_component_1 = require("../common/dialog/dialog.component");
var imagecropperdialog_component_1 = require("../common/imagecropper/imagecropperdialog.component");
var videorecord_component_1 = require("../common/videorecord/videorecord.component");
var alert_component_1 = require("../common/dialog/alert.component");
var DialogService = /** @class */ (function () {
    function DialogService(dialog, domService) {
        this.dialog = dialog;
        this.domService = domService;
        this.DIALOGTYPES = {
            STANDARD: 1,
            XS: 2,
            LG: 3
        };
    }
    DialogService.prototype.dismissOpenDialogs = function () {
        this.dialog.closeAll();
    };
    DialogService.prototype.showMessageDialog = function (message, title) {
        var __hostclasses = "alertdialog";
        var dialogRef = this.dialog.open(alert_component_1.AlertComponent, {
            panelClass: __hostclasses,
            data: { message: message, title: title }
        });
    };
    DialogService.prototype.openComponent = function (componentName, moduleName, dialogType, componentContext, panelClass) {
        var dialogContext = this.createDialogContext(dialogType, panelClass);
        var dialogRef = this.dialog.open(dialog_component_1.DialogComponent, {
            width: dialogContext.width,
            position: dialogContext.position,
            maxWidth: dialogContext.maxWidth,
            maxHeight: dialogContext.maxHeight,
            data: { componentContext: componentContext, componentName: componentName, moduleName: moduleName },
            panelClass: dialogContext.panelClass
        });
        return dialogRef;
    };
    DialogService.prototype.openImageCropper = function (parentId, imageChangedEvent, isRoundCropper) {
        var __hostclasses = "imagecropper";
        var dialogRef = this.dialog.open(imagecropperdialog_component_1.ImageCropperDialogComponent, {
            maxHeight: 420,
            data: { parentId: parentId, imageChangedEvent: imageChangedEvent, roundCropper: isRoundCropper },
            panelClass: __hostclasses
        });
    };
    DialogService.prototype.openVideoUploader = function (parentId) {
        var __hostclasses = "videoupload";
        this.dialog.open(videorecord_component_1.VideoRecordDialogComponent, {
            height: "100%",
            data: { parentId: parentId },
            panelClass: __hostclasses
        });
    };
    DialogService.prototype.openVideoDisplay = function (videoURL, videoFrame) {
        var __hostclasses = "videoplayer";
        this.openComponent("VjsPlayerComponent", "VjsPlayerModule", this.DIALOGTYPES.LG, {
            autoplay: true,
            fluid: true,
            live: false,
            poster: videoFrame,
            sources: [{ src: videoURL, type: "video/webm" }]
        }, __hostclasses);
    };
    DialogService.prototype.createDialogContext = function (dialogType, panelClass) {
        if (dialogType === undefined) {
            dialogType = this.DIALOGTYPES.STANDARD;
        }
        var width;
        var position;
        var maxHeight;
        var maxWidth = "100%";
        if (dialogType === this.DIALOGTYPES.STANDARD) {
            if (this.domService.device === this.domService.DEVICETYPES.MOBILE) {
                width = "100%";
                position = { top: "auto", bottom: "0px", left: "0px", right: "0px" };
                maxHeight = "75%";
            }
            else {
                width = "80%";
                position = undefined;
                maxHeight = "auto";
            }
        }
        else if (dialogType === this.DIALOGTYPES.XS) {
            if (this.domService.device === this.domService.DEVICETYPES.MOBILE) {
                width = "100%";
                position = { top: "auto", bottom: "0px", left: "0px", right: "0px" };
                maxHeight = "75%";
            }
            else {
                width = this.domService.STDSIZES.XS.width;
                position = { top: "auto", bottom: "auto", left: "auto", right: "auto" };
                maxHeight = "auto";
            }
        }
        else if (dialogType === this.DIALOGTYPES.LG) {
            if (this.domService.device === this.domService.DEVICETYPES.MOBILE) {
                width = "100%";
                position = { top: "auto", bottom: "0px", left: "0px", right: "0px" };
                maxHeight = "100%";
            }
            else {
                width = this.domService.STDSIZES.XS.width;
                position = undefined;
                maxHeight = "auto";
            }
        }
        var dialogTypeObj = {
            panelClass: panelClass === undefined ? "" : panelClass,
            position: position,
            width: width,
            maxWidth: maxWidth,
            maxHeight: maxHeight
        };
        return dialogTypeObj;
    };
    DialogService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], DialogService);
    return DialogService;
}());
exports.DialogService = DialogService;
