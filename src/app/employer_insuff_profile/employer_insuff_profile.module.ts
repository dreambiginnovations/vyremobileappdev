import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerInsuffProfileComponent } from "./employer_insuff_profile.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerInsuffProfileComponent,
  },
];

@NgModule({
  declarations: [EmployerInsuffProfileComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, VideoRecordModule],
})
export class EmployerInsuffProfileModule {
  static components = {
    EmployerInsuffProfileComponent: EmployerInsuffProfileComponent,
  };
}
