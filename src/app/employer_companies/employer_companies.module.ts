import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerCompaniesComponent } from "./employer_companies.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerCompaniesComponent,
  },
];

@NgModule({
  declarations: [EmployerCompaniesComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, VideoRecordModule],
})
export class EmployerCompaniesModule {
  static components = {
    EmployerCompaniesComponent: EmployerCompaniesComponent,
  };
}
