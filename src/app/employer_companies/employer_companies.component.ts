import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_companies",
  templateUrl: "./employer_companies.template.html",
  styleUrls: ["./employer_companies.component.scss"],
})
export class EmployerCompaniesComponent implements OnInit {
  @HostBinding("class") hostclasses = "nonstandardiconcontainer container px-0 viewfor__DETAILS";
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, public templateutilsService: TemplateutilsService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("post_job", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("companies", new FormArray([]));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  addNode_companies(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("video_id", new FormControl(""));
    nodeControl.addControl("company_name", new FormControl(""));
    nodeControl.addControl("company_location_city", new FormControl(""));
    nodeControl.addControl("no_of_employees", new FormControl(""));
    nodeControl.addControl("view_details", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("view_details", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("view_details", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("company_id", new FormControl(""));
    nodeControl.addControl("num_of_jobs", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("company_logo_id", new FormControl(""));

    if (nodeData !== undefined) {
      nodeControl.controls["video_id"].setValue(nodeData["video_id"]);
      nodeControl.controls["company_name"].setValue(nodeData["company_name"]);
      nodeControl.controls["company_location_city"].setValue(nodeData["company_location_city"]);
      nodeControl.controls["no_of_employees"].setValue(nodeData["no_of_employees"]);
      nodeControl.controls["view_details"].setValue(nodeData["view_details"]);
      nodeControl.controls["view_details"].setValue(nodeData["view_details"]);
      nodeControl.controls["view_details"].setValue(nodeData["view_details"]);
      nodeControl.controls["company_id"].setValue(nodeData["company_id"]);
      nodeControl.controls["num_of_jobs"].setValue(nodeData["num_of_jobs"]);
      nodeControl.controls["company_logo_id"].setValue(nodeData["company_logo_id"]);
      nodeControl.controls["company_logo_id"].valueChanges.subscribe((val) => {
        if (nodeControl.value["company_logo_id"] !== val) {
          setTimeout(() => {
            this["submitFormAPI"]();
          }, 200);
        }
      });
    }
    (<FormArray>this.localform.controls["companies"]).push(nodeControl);
  }

  removeNode_companies(index) {
    (<FormArray>this.localform.controls["companies"]).removeAt(index);
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getemployercompanies").subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["post_job"].setValue(this.response_data["post_job"]);
      this.localform.controls["companies"] = new FormArray([]);
      if (this.response_data["companies"] !== undefined) {
        this.response_data["companies"].forEach((node) => {
          this.addNode_companies(node);
        });
      }
    });
  }
}
