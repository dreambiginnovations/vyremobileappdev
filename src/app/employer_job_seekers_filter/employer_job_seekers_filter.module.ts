import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { CustomDropDownModule } from "../common/dropdown/dropdown.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerJobSeekersFilterComponent } from "./employer_job_seekers_filter.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerJobSeekersFilterComponent,
  },
];

@NgModule({
  declarations: [EmployerJobSeekersFilterComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, CustomDropDownModule],
})
export class EmployerJobSeekersFilterModule {
  static components = {
    EmployerJobSeekersFilterComponent: EmployerJobSeekersFilterComponent,
  };
}
