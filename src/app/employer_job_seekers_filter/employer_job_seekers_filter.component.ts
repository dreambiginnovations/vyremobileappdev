import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { CommunicationService } from "../services/communication.service";
import { DialogService } from "../services/dialog.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_job_seekers_filter",
  templateUrl: "./employer_job_seekers_filter.template.html",
  styleUrls: ["./employer_job_seekers_filter.component.scss"],
})
export class EmployerJobSeekersFilterComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__EDIT";
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, private communicationService: CommunicationService, private dialogService: DialogService, private templateutilsService: TemplateutilsService, private toastService: ToastService) {}

  parentid: any;
  filter_data: any;
  ngOnInit() {
    this.parentid = this.communicationService.communication.context.parentid;
    this.filter_data = this.communicationService.communication.obj.filter_data;

    this.initializeFormGroup();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("preferred_job_location_city", new FormControl(""));
    this.load_city_options().subscribe((res) => {
      this.response_city_options = res;
    });
    this.localform.addControl("job_category_id", new FormControl(""));
    this.load_category_id_options().subscribe((res) => {
      this.response_category_id_options = res;
    });
    this.localform.addControl("job_role_id", new FormControl(""));
    this.load_role_id_options().subscribe((res) => {
      this.response_role_id_options = res;
    });
    this.localform.addControl("min_experience", new FormControl(""));
    this.localform.addControl("filter_button", new FormControl("", this.skipsubmitValidator));

    this.initializeFilterData();
  }

  initializeFilterData() {
    for (var key in this.filter_data) {
      if (this.filter_data[key] !== "" && !isNaN(this.filter_data[key])) {
        this.filter_data[key] = +this.filter_data[key];
      }
    }

    this.localform.controls["preferred_job_location_city"].setValue(this.filter_data["preferred_job_location_city"]);
    this.localform.controls["job_category_id"].setValue(this.filter_data["job_category_id"]);
    this.localform.controls["job_role_id"].setValue(this.filter_data["job_role_id"]);
    this.localform.controls["min_experience"].setValue(this.filter_data["min_experience"]);
    this.localform.controls["filter_button"].setValue(this.filter_data["filter_button"]);
  }

  submitFilterData() {
    this.communicationService.broadcastCommunication({ context: { parentid: this.parentid }, obj: { filter_data: this.localform.value } });
    this.dialogService.dismissOpenDialogs();
  }

  response_category_id_options: any;
  load_category_id_options() {
    return this.apiService.post_api("jobportal", "getjobcategoriesoptions");
  }

  response_role_id_options: any;
  load_role_id_options() {
    return this.apiService.post_api("jobportal", "getjobrolesoptions");
  }

  response_city_options: any;
  load_city_options() {
    return this.apiService.post_api("jobportal", "getcandidatecities");
  }
}
