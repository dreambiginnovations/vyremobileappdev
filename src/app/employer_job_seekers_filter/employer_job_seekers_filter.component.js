"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var EmployerJobSeekersFilterComponent = /** @class */ (function () {
    function EmployerJobSeekersFilterComponent(apiService, commonutilsService, communicationService, dialogService, templateutilsService, toastService) {
        this.apiService = apiService;
        this.commonutilsService = commonutilsService;
        this.communicationService = communicationService;
        this.dialogService = dialogService;
        this.templateutilsService = templateutilsService;
        this.toastService = toastService;
        this.hostclasses = "container px-0 viewfor__EDIT";
    }
    EmployerJobSeekersFilterComponent.prototype.ngOnInit = function () {
        this.parentid = this.communicationService.communication.context.parentid;
        this.filter_data = this.communicationService.communication.obj.filter_data;
        this.initializeFormGroup();
    };
    EmployerJobSeekersFilterComponent.prototype.skipsubmitValidator = function (control) {
        return { skip: true };
    };
    EmployerJobSeekersFilterComponent.prototype.processcontrolforskip = function (control) {
        var newControl;
        if (control instanceof forms_1.FormArray) {
            newControl = this.processformgroupforskip(control, true);
        }
        else if (control instanceof forms_1.FormGroup) {
            newControl = this.processformgroupforskip(control, false);
        }
        else {
            newControl = control;
        }
        return newControl;
    };
    EmployerJobSeekersFilterComponent.prototype.processformgroupforskip = function (formgroup, controlsArray) {
        var _a, _b;
        var returnFormGroup;
        if (controlsArray) {
            returnFormGroup = new forms_1.FormArray([]);
            for (var count = 0; count < formgroup.controls.length; count++) {
                var control = formgroup.controls[count];
                if (((_a = control.errors) === null || _a === void 0 ? void 0 : _a.skip) !== true) {
                    var newControl = this.processcontrolforskip(control);
                    returnFormGroup.push(newControl);
                }
            }
        }
        else {
            returnFormGroup = new forms_1.FormGroup({});
            for (var controlName in formgroup.controls) {
                var control = formgroup.controls[controlName];
                if (((_b = control.errors) === null || _b === void 0 ? void 0 : _b.skip) !== true) {
                    var newControl = this.processcontrolforskip(control);
                    returnFormGroup.addControl(controlName, newControl);
                }
            }
        }
        return returnFormGroup;
    };
    EmployerJobSeekersFilterComponent.prototype.initializeFormGroup = function () {
        var _this = this;
        this.localform = new forms_1.FormGroup({});
        this.localform.addControl("preferred_job_location_city", new forms_1.FormControl(""));
        this.load_city_options().subscribe(function (res) {
            _this.response_city_options = res;
        });
        this.localform.addControl("job_category_id", new forms_1.FormControl(""));
        this.load_category_id_options().subscribe(function (res) {
            _this.response_category_id_options = res;
        });
        this.localform.addControl("job_role_id", new forms_1.FormControl(""));
        this.load_role_id_options().subscribe(function (res) {
            _this.response_role_id_options = res;
        });
        this.localform.addControl("min_experience", new forms_1.FormControl(""));
        this.localform.addControl("filter_button", new forms_1.FormControl("", this.skipsubmitValidator));
        this.initializeFilterData();
    };
    EmployerJobSeekersFilterComponent.prototype.initializeFilterData = function () {
        for (var key in this.filter_data) {
            if (this.filter_data[key] !== "" && !isNaN(this.filter_data[key])) {
                this.filter_data[key] = +this.filter_data[key];
            }
        }
        this.localform.controls["preferred_job_location_city"].setValue(this.filter_data["preferred_job_location_city"]);
        this.localform.controls["job_category_id"].setValue(this.filter_data["job_category_id"]);
        this.localform.controls["job_role_id"].setValue(this.filter_data["job_role_id"]);
        this.localform.controls["min_experience"].setValue(this.filter_data["min_experience"]);
        this.localform.controls["filter_button"].setValue(this.filter_data["filter_button"]);
    };
    EmployerJobSeekersFilterComponent.prototype.submitFilterData = function () {
        this.communicationService.broadcastCommunication({ context: { parentid: this.parentid }, obj: { filter_data: this.localform.value } });
        this.dialogService.dismissOpenDialogs();
    };
    EmployerJobSeekersFilterComponent.prototype.load_category_id_options = function () {
        return this.apiService.post_api("jobportal", "getjobcategoriesoptions");
    };
    EmployerJobSeekersFilterComponent.prototype.load_role_id_options = function () {
        return this.apiService.post_api("jobportal", "getjobrolesoptions");
    };
    EmployerJobSeekersFilterComponent.prototype.load_city_options = function () {
        return this.apiService.post_api("jobportal", "getcandidatecities");
    };
    __decorate([
        core_1.HostBinding("class")
    ], EmployerJobSeekersFilterComponent.prototype, "hostclasses");
    EmployerJobSeekersFilterComponent = __decorate([
        core_1.Component({
            selector: ".component__employer_job_seekers_filter",
            templateUrl: "./employer_job_seekers_filter.template.html",
            styleUrls: ["./employer_job_seekers_filter.component.scss"]
        })
    ], EmployerJobSeekersFilterComponent);
    return EmployerJobSeekersFilterComponent;
}());
exports.EmployerJobSeekersFilterComponent = EmployerJobSeekersFilterComponent;
