import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { YouTubePlayerModule } from "@angular/youtube-player";
import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { BaseCourseLessonNodeslistComponent } from "./base_course_lesson_nodeslist.component";

export const routes: Routes = [
  {
    path: "",
    component: BaseCourseLessonNodeslistComponent,
  },
];

@NgModule({
  declarations: [BaseCourseLessonNodeslistComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, YouTubePlayerModule, ImgCropperModule, VideoRecordModule],
})
export class BaseCourseLessonNodeslistModule {
  static components = {
    BaseCourseLessonNodeslistComponent: BaseCourseLessonNodeslistComponent,
  };
}
