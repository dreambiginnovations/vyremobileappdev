import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__base_course_lesson_nodeslist",
  templateUrl: "./base_course_lesson_nodeslist.template.html",
  styleUrls: ["./base_course_lesson_nodeslist.component.scss"],
})
export class BaseCourseLessonNodeslistComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  course_id: any;
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, private toastService: ToastService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.course_id = this.route.snapshot.params["course_id"];

    this.initializeFormGroup();

    this.route.queryParams.subscribe((queryParams) => {
      if (queryParams.page_number) {
        this.page_number = parseInt(queryParams.page_number);
      } else {
        this.page_number = 1;
      }
      this.load_data();
    });
  }

  localform: FormGroup;
  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("lessons", new FormArray([]));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  addNode_lessons(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("content_article_name", new FormControl("", Validators.required));
    nodeControl.addControl("attachments", new FormArray([]));
    nodeControl.addControl("content_article_content", new FormControl("", Validators.required));

    if (nodeData !== undefined) {
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["content_article_name"].setValue(nodeData["content_article_name"]);
      this.localform.controls["attachments"] = new FormArray([]);
      if (this.response_data["attachments"] !== undefined) {
        this.response_data["attachments"].forEach((node) => {
          this.addNode_attachments(node);
        });
      }

      nodeControl.controls["content_article_content"].setValue(nodeData["content_article_content"]);
    }
    (<FormArray>this.localform.controls["lessons"]).push(nodeControl);
  }

  removeNode_lessons(index) {
    (<FormArray>this.localform.controls["lessons"]).removeAt(index);
  }

  records_per_page = 20;
  page_number = 1;
  total_pages;
  load_data_page(page_number) {
    var newQueryParams = Object.assign({}, this.route.snapshot.queryParams);
    newQueryParams["page_number"] = page_number;
    this.router.navigate(["."], { relativeTo: this.route, queryParams: newQueryParams });
  }
  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getlessonsofcourse", { course_id: this.course_id, records_per_page: this.records_per_page, page_number: this.page_number }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res.response;
      if (!isNaN(res.total)) {
        this.total_pages = Math.ceil(res.total / this.records_per_page);
      } else {
        this.total_pages = Math.ceil(res.total[Object.keys(res.total)[0]] / this.records_per_page);
      }

      this.localform.controls["lessons"] = new FormArray([]);
      if (this.response_data["lessons"] !== undefined) {
        this.response_data["lessons"].forEach((node) => {
          this.addNode_lessons(node);
        });
      }
    });
  }
}
