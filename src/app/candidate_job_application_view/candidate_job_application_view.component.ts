import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { AppstateService } from "../services/appstate.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__candidate_job_application_view",
  templateUrl: "./candidate_job_application_view.template.html",
  styleUrls: ["./candidate_job_application_view.component.scss"],
})
export class CandidateJobApplicationViewComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__EDIT";
  id: any;
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, public appstateService: AppstateService, private toastService: ToastService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];

    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;
  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("candidate_video_id", new FormControl(""));
    this.localform.addControl("pan_card", new FormControl(""));
    this.localform.addControl("company_name", new FormControl(""));
    this.localform.addControl("job_title", new FormControl(""));
    this.localform.addControl("message", new FormControl(""));
    this.localform.addControl("screening_questions", new FormArray([]));
  }

  addNode_screening_questions(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("question_id", new FormControl(""));
    nodeControl.addControl("question", new FormControl(""));
    nodeControl.addControl("answer_id", new FormControl(""));

    if (nodeData !== undefined) {
      nodeControl.controls["question_id"].setValue(nodeData["id"]);
      nodeControl.controls["question"].setValue(nodeData["question"]);
      nodeControl.controls["answer_id"].setValue(nodeData["answer_id"]);
    }
    (<FormArray>this.localform.controls["screening_questions"]).push(nodeControl);
  }

  removeNode_screening_questions(index) {
    (<FormArray>this.localform.controls["screening_questions"]).removeAt(index);
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getjobapplication", { id: this.id }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["candidate_video_id"].setValue(this.response_data["candidate_video_id"]);
      this.localform.controls["pan_card"].setValue(this.response_data["pan_card"]);
      this.localform.controls["company_name"].setValue(this.response_data["company_name"]);
      this.localform.controls["job_title"].setValue(this.response_data["job_title"]);
      this.localform.controls["message"].setValue(this.response_data["message"]);
      this.localform.controls["screening_questions"] = new FormArray([]);
      if (this.response_data["screening_questions"] !== undefined) {
        this.response_data["screening_questions"].forEach((node) => {
          this.addNode_screening_questions(node);
        });
      }
    });
  }
}
