import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateJobApplicationViewComponent } from "./candidate_job_application_view.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateJobApplicationViewComponent,
  },
];

@NgModule({
  declarations: [CandidateJobApplicationViewComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, VideoRecordModule],
})
export class CandidateJobApplicationViewModule {
  static components = {
    CandidateJobApplicationViewComponent: CandidateJobApplicationViewComponent,
  };
}
