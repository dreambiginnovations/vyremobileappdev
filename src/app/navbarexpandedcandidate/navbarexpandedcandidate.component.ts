import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { TemplateutilsService } from "../services/templateutils.service";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__navbarexpandedcandidate",
  templateUrl: "./navbarexpandedcandidate.template.html",
  styleUrls: ["./navbarexpandedcandidate.component.scss"],
})
export class NavbarexpandedcandidateComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  constructor(public templateutilsService: TemplateutilsService, public apiService: ApiService, private commonutilsService: CommonutilsService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("photo_id", new FormControl("", Validators.required));
    this.localform.addControl("dashboard", new FormControl(""));
    this.localform.addControl("myprofile", new FormControl(""));
    this.localform.addControl("findjobs", new FormControl(""));
    this.localform.addControl("appliedjobs", new FormControl(""));
    this.localform.addControl("favoritejobs", new FormControl(""));
    this.localform.addControl("mynotifications", new FormControl(""));
    this.localform.addControl("mylms", new FormControl(""));
    this.localform.addControl("logout", new FormControl("", this.skipsubmitValidator));
  }

  collapseMenu() {
    this.templateutilsService.dismissDialogs();
  }
  logout() {
    this.apiService.post_api("common", "logout").subscribe((res) => {
      this.templateutilsService.dismissAndNavigate("login_signup");
      this.apiService.post_api("backend", "getAppState").subscribe();
    });
  }
  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getcandidatecandidate").subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["photo_id"].setValue(this.response_data["photo_id"]);
      this.localform.controls["dashboard"].setValue(this.response_data["dashboard"]);
      this.localform.controls["myprofile"].setValue(this.response_data["myprofile"]);
      this.localform.controls["findjobs"].setValue(this.response_data["findjobs"]);
      this.localform.controls["appliedjobs"].setValue(this.response_data["appliedjobs"]);
      this.localform.controls["favoritejobs"].setValue(this.response_data["favoritejobs"]);
      this.localform.controls["mynotifications"].setValue(this.response_data["mynotifications"]);
      this.localform.controls["mylms"].setValue(this.response_data["mylms"]);
      this.localform.controls["logout"].setValue(this.response_data["logout"]);
    });
  }

  response_category_id_options: any;
  load_category_id_options() {
    return this.apiService.post_api("jobportal", "getjobcategoriesoptions");
  }

  response_role_id_options: any;
  load_role_id_options() {
    return this.apiService.post_api("jobportal", "getjobrolesoptions");
  }
}
