"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var NavbarexpandedcandidateComponent = /** @class */ (function () {
    function NavbarexpandedcandidateComponent(templateutilsService, apiService, commonutilsService, toastService) {
        this.templateutilsService = templateutilsService;
        this.apiService = apiService;
        this.commonutilsService = commonutilsService;
        this.toastService = toastService;
        this.hostclasses = "container px-0 viewfor__DETAILS";
    }
    NavbarexpandedcandidateComponent.prototype.ngOnInit = function () {
        this.initializeFormGroup();
        this.load_data();
    };
    NavbarexpandedcandidateComponent.prototype.skipsubmitValidator = function (control) {
        return { skip: true };
    };
    NavbarexpandedcandidateComponent.prototype.processcontrolforskip = function (control) {
        var newControl;
        if (control instanceof forms_1.FormArray) {
            newControl = this.processformgroupforskip(control, true);
        }
        else if (control instanceof forms_1.FormGroup) {
            newControl = this.processformgroupforskip(control, false);
        }
        else {
            newControl = control;
        }
        return newControl;
    };
    NavbarexpandedcandidateComponent.prototype.processformgroupforskip = function (formgroup, controlsArray) {
        var _a, _b;
        var returnFormGroup;
        if (controlsArray) {
            returnFormGroup = new forms_1.FormArray([]);
            for (var count = 0; count < formgroup.controls.length; count++) {
                var control = formgroup.controls[count];
                if (((_a = control.errors) === null || _a === void 0 ? void 0 : _a.skip) !== true) {
                    var newControl = this.processcontrolforskip(control);
                    returnFormGroup.push(newControl);
                }
            }
        }
        else {
            returnFormGroup = new forms_1.FormGroup({});
            for (var controlName in formgroup.controls) {
                var control = formgroup.controls[controlName];
                if (((_b = control.errors) === null || _b === void 0 ? void 0 : _b.skip) !== true) {
                    var newControl = this.processcontrolforskip(control);
                    returnFormGroup.addControl(controlName, newControl);
                }
            }
        }
        return returnFormGroup;
    };
    NavbarexpandedcandidateComponent.prototype.initializeFormGroup = function () {
        this.localform = new forms_1.FormGroup({});
        this.localform.addControl("photo_id", new forms_1.FormControl("", forms_1.Validators.required));
        this.localform.addControl("dashboard", new forms_1.FormControl(""));
        this.localform.addControl("myprofile", new forms_1.FormControl(""));
        this.localform.addControl("findjobs", new forms_1.FormControl(""));
        this.localform.addControl("appliedjobs", new forms_1.FormControl(""));
        this.localform.addControl("favoritejobs", new forms_1.FormControl(""));
        this.localform.addControl("mynotifications", new forms_1.FormControl(""));
        this.localform.addControl("mylms", new forms_1.FormControl(""));
        this.localform.addControl("logout", new forms_1.FormControl("", this.skipsubmitValidator));
    };
    NavbarexpandedcandidateComponent.prototype.collapseMenu = function () {
        this.templateutilsService.dismissDialogs();
    };
    NavbarexpandedcandidateComponent.prototype.logout = function () {
        var _this = this;
        this.apiService.post_api("common", "logout").subscribe(function (res) {
            _this.templateutilsService.dismissAndNavigate("login_signup");
            _this.apiService.post_api("backend", "getAppState").subscribe();
        });
    };
    NavbarexpandedcandidateComponent.prototype.load_data = function () {
        var _this = this;
        this.apiService.post_api("jobportal", "getcandidatecandidate").subscribe(function (res) {
            if (res.status === "FALSE" || res.status === false) {
                _this.toastService.showError("Error", res.message);
                _this.commonutilsService.processResponse(res);
                return;
            }
            _this.response_data = res;
            _this.localform.controls["photo_id"].setValue(_this.response_data["photo_id"]);
            _this.localform.controls["dashboard"].setValue(_this.response_data["dashboard"]);
            _this.localform.controls["myprofile"].setValue(_this.response_data["myprofile"]);
            _this.localform.controls["findjobs"].setValue(_this.response_data["findjobs"]);
            _this.localform.controls["appliedjobs"].setValue(_this.response_data["appliedjobs"]);
            _this.localform.controls["favoritejobs"].setValue(_this.response_data["favoritejobs"]);
            _this.localform.controls["mynotifications"].setValue(_this.response_data["mynotifications"]);
            _this.localform.controls["mylms"].setValue(_this.response_data["mylms"]);
            _this.localform.controls["logout"].setValue(_this.response_data["logout"]);
        });
    };
    NavbarexpandedcandidateComponent.prototype.load_category_id_options = function () {
        return this.apiService.post_api("jobportal", "getjobcategoriesoptions");
    };
    NavbarexpandedcandidateComponent.prototype.load_role_id_options = function () {
        return this.apiService.post_api("jobportal", "getjobrolesoptions");
    };
    __decorate([
        core_1.HostBinding("class")
    ], NavbarexpandedcandidateComponent.prototype, "hostclasses");
    NavbarexpandedcandidateComponent = __decorate([
        core_1.Component({
            selector: ".component__navbarexpandedcandidate",
            templateUrl: "./navbarexpandedcandidate.template.html",
            styleUrls: ["./navbarexpandedcandidate.component.scss"]
        })
    ], NavbarexpandedcandidateComponent);
    return NavbarexpandedcandidateComponent;
}());
exports.NavbarexpandedcandidateComponent = NavbarexpandedcandidateComponent;
