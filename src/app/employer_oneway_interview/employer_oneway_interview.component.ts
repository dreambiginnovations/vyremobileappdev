import { Component, OnInit, HostBinding } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NavigationService } from "../services/navigation.service";
import { EnvService } from "../services/env.service";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_oneway_interview",
  templateUrl: "./employer_oneway_interview.component.html",
  styleUrls: ["./employer_oneway_interview.component.scss"],
})
export class EmployerOnewayInterviewComponent implements OnInit {
  @HostBinding("class") hostclasses = "viewascardondesktop bg-white px-0 flex-column nonstandardiconcontainer text-center container viewfor__DETAILS";
  constructor(public navigationService: NavigationService, public envService: EnvService, private apiService: ApiService, private commonutilsService: CommonutilsService, private toastService: ToastService, private router: Router, private route: ActivatedRoute) {}

  chosenJobID: any;
  jobIDJobMap: any;
  response_data: any;
  step: string;
  chosenJob: any;
  invitedCandidates: any;
  
  ngOnInit() {
    this.chosenJobID = undefined;
    this.route.queryParams.subscribe((queryParams)=>{
      console.log("queryParams", queryParams);
      this.chosenJobID = queryParams.job_id;
    });
    this.route.params.subscribe((params) => {
      console.log("detected params change", params);
      if(params.step) {
        this.step = params.step;
      } else {
        this.step = "interviewmenu";    
      }
      
      if(this.step=="managejobquestions") {
        this.load_managejobquestions_screen();
      } else if(this.step=="managequestionlibrary") {
        this.load_managequestionlibrary_screen();
      }
    });
  }
  gotostep(value:string, action?:string) {
    if(action===undefined) {
      this.router.navigate(['/employer/oneway_interview', value]);
    } else {
      if(action=="JOBID") {
        this.router.navigate(['/employer/oneway_interview', value], {queryParams: {job_id: this.chosenJobID}});
      }
    }
  }
  modify_job() {
    this.router.navigate(['/employer/editjob', this.chosenJobID]);
  }
  
  
  load_managejobquestions_screen() {
    this.response_data = undefined;
    this.chosenJob = undefined;
    this.load_jobs_data();
  }
  load_jobs_data() {
    this.apiService.post_api("jobportal", "getemployerpostedjobs", {  }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;
      this.jobIDJobMap = {};
      for(var count=0;count<this.response_data.jobs.length;count++) {
        var jobObj = this.response_data.jobs[count];
        this.jobIDJobMap[jobObj.id] = jobObj;
      }
      this.populateChosenJobFromJobsList();
      if(this.chosenJob==undefined) {
        this.chooseJob(this.response_data.jobs[0], false, true);
      }
    });
  }
  populateChosenJobFromJobsList() {
    for(var count=0;count<this.response_data.jobs.length;count++) {
      var jobObj = this.response_data.jobs[count];
      if(jobObj.id==this.chosenJobID) {
        this.chosenJob = jobObj;
        this.chooseJob(this.chosenJob, false, true);
      }
    }
  }
  chooseJob(job?, loadInterviewQuestions?, loadInvitedCandidates?) {
    if(job===undefined) {job = this.chosenJob;}
    this.chosenJobID = job.id;
    this.apiService.post_api("jobportal", "getjob", { id: job.id }).subscribe((res) => {
      this.chosenJob = JSON.parse(JSON.stringify(res));
      if(loadInterviewQuestions) {
        this.load_interview_questions();
      }
      console.log("loadInvitedCandidates", loadInvitedCandidates);
      if(loadInvitedCandidates) {
        this.load_invited_candidates();
      }
    });
  }
  removeQuestionFromJob(ind) {
    if(confirm("Are you sure you want to remove?")) {
      this.chosenJob.screening_questions.splice(ind, 1);
      const deepCopyObject = JSON.parse(JSON.stringify(this.chosenJob));
      delete deepCopyObject["num_of_applications"];
      this.apiService.post_api("jobportal", "updatejob", deepCopyObject).subscribe((res) => {
        this.chooseJob();
      });
    }
  }
  load_invited_candidates() {
    this.apiService.post_api("jobportal", "getinvitedcandidatesforinterview", {"job_id":this.chosenJobID}).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.invitedCandidates = res;
    }, (error)=>{this.toastService.showError("Error occurred", "Please contact administrator");});
  }
  
  
  interviewquestions:any;
  screeningquestionids: any;
  load_managequestionlibrary_screen() {
    if(this.chosenJobID!==undefined) {
      this.chooseJob({id: this.chosenJobID}, true);
    } else {
      this.load_interview_questions();
    }
  }
  load_interview_questions() {
    this.interviewquestions = [];
    this.screeningquestionids = [];
    this.apiService.post_api("jobportal", "getcompanyinterviewquestions", {  }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.interviewquestions = res;
      for(var count=0;count<this.chosenJob.screening_questions.length;count++) {
        this.screeningquestionids.push(this.chosenJob.screening_questions[count].question_id);
      }
      for(var count=0;count<this.interviewquestions.length;count++) {
        if(this.screeningquestionids.indexOf(this.interviewquestions[count].id)>-1) {
          this.interviewquestions[count].disabled = true;
        }
      }
    });
  }
  newquestion: string;
  add_interview_question() {
    if(this.newquestion.length>100) {
      this.toastService.showError("Question too long", "Your entry is exceeding maximum 100 characters.");
      return;
    }
    this.apiService.post_api("jobportal", "addcompanyinterviewquestion", {"question": this.newquestion}).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      if(this.chosenJobID!==undefined) {
        for(var count=0;count<res.length;count++) {
          if(res[count].question==this.newquestion) {
            this.add_question_to_job(res[count]);
          }
        }
      }
      this.newquestion = undefined;
      this.interviewquestions = res;
    });
  }
  add_question_to_job(question) {
    if(question.disabled) {
      this.toastService.showError("Already added", "This question is already added to the Interview questions of this job.");
      return;
    }
    var question_id = question.id;
    this.apiService.post_api("jobportal", "addquestiontojob", {"question_id": question_id, "job_id": this.chosenJob.id}).subscribe((res2) => {
      this.toastService.showSuccess("Succcess!", "Question added successfully.");
      this.gotostep("managejobquestions", "JOBID");
    });
  }
  remove_interview_question(i) {
    if(confirm("Are you sure you want to remove this question from Library?")) {
      this.apiService.post_api("jobportal", "removecompanyinterviewquestion", {"id": i}).subscribe((res) => {
        if (res.status === "FALSE" || res.status === false) {
          this.toastService.showError("Error", res.message);
          this.commonutilsService.processResponse(res);
          return;
        }
        this.newquestion = undefined;
        this.interviewquestions = res;
      });
    }
  }
  
  
  inviteEmail: any;
  inviteMobile: any;
  invitecandidate() {
    var candidateObj: any = {};
    /*if((this.inviteEmail==="" || this.inviteEmail===undefined) && (this.inviteMobile==="" || this.inviteMobile===undefined)) {
      this.toastService.showError("Input missing", "Please enter atleast one of the 2 inputs");
      return;
    }
    if((this.inviteEmail!=="" && this.inviteEmail!==undefined) && !String(this.inviteEmail).toLowerCase().match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    )) {
      this.toastService.showError("Invalid Email", "The email address you entered is invalid");
      return;
    } else {
      candidateObj["email"] = this.inviteEmail;
    }*/
    if((this.inviteMobile==="" || this.inviteMobile===undefined)) {
      this.toastService.showError("Input missing", "Please enter a valid Mobile number");
      return;
    }
    if((this.inviteMobile!=="" && this.inviteMobile!==undefined) && !String(this.inviteMobile).toLowerCase().match(
      /^[6-9]\d{9}$/
    )) {
      this.toastService.showError("Invalid Mobile", "The mobile number you entered is invalid");
      return;
    } else {
      candidateObj["mobile"] = this.inviteMobile;
    }
    
    this.apiService.post_api("jobportal", "invitecandidateforinterview", {"job_id":this.chosenJobID, "candidate": candidateObj}).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.gotostep("managejobquestions", "JOBID");
    }, (error)=>{this.toastService.showError("Error occurred", "Please contact administrator");});
  }
  
  
  
  
}
