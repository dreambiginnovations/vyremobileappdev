import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { EmployerOnewayInterviewComponent } from "./employer_oneway_interview.component";

import { PipesModule } from "../common/pipes/pipes.module";

export const routes: Routes = [
  {
    path: "",
    component: EmployerOnewayInterviewComponent,
  },
  {
    path: ":step",
    component: EmployerOnewayInterviewComponent,
  }
];

@NgModule({
  declarations: [EmployerOnewayInterviewComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, FormsModule],
})

export class EmployerOnewayInterviewModule {
  static components = {
    EmployerOnewayInterviewComponent: EmployerOnewayInterviewComponent,
  };
}
