import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { FileUploaderModule } from "../common/fileuploader/fileuploader.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { MapLocationModule } from "../common/maplocation/maplocation.module";
import { CustomTextAreaModule } from "../common/customtextarea/customtextarea.module";
import { CustomDropDownModule } from "../common/dropdown/dropdown.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerJobEditDetailsComponent } from "./employer_job_edit_details.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerJobEditDetailsComponent,
  },
];

@NgModule({
  declarations: [EmployerJobEditDetailsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, FileUploaderModule, VideoRecordModule, MapLocationModule, CustomTextAreaModule, CustomDropDownModule],
})
export class EmployerJobEditDetailsModule {
  static components = {
    EmployerJobEditDetailsComponent: EmployerJobEditDetailsComponent,
  };
}
