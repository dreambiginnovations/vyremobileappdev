import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { FileUploaderModule } from "../common/fileuploader/fileuploader.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { PublicCompanyViewDetailsComponent } from "./public_company_view_details.component";

export const routes: Routes = [
  {
    path: "",
    component: PublicCompanyViewDetailsComponent,
  },
];

@NgModule({
  declarations: [PublicCompanyViewDetailsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, FileUploaderModule, VideoRecordModule],
})
export class PublicCompanyViewDetailsModule {
  static components = {
    PublicCompanyViewDetailsComponent: PublicCompanyViewDetailsComponent,
  };
}
