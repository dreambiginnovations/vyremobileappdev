import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerPostedJobsComponent } from "./employer_posted_jobs.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerPostedJobsComponent,
  },
];

@NgModule({
  declarations: [EmployerPostedJobsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, VideoRecordModule],
})
export class EmployerPostedJobsModule {
  static components = {
    EmployerPostedJobsComponent: EmployerPostedJobsComponent,
  };
}
