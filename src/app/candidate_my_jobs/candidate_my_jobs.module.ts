import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateMyJobsComponent } from "./candidate_my_jobs.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateMyJobsComponent,
  },
];

@NgModule({
  declarations: [CandidateMyJobsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, VideoRecordModule],
})
export class CandidateMyJobsModule {
  static components = {
    CandidateMyJobsComponent: CandidateMyJobsComponent,
  };
}
