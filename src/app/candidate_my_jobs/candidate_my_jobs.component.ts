import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { CommunicationService } from "../services/communication.service";
import { DialogService } from "../services/dialog.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__candidate_my_jobs",
  templateUrl: "./candidate_my_jobs.template.html",
  styleUrls: ["./candidate_my_jobs.component.scss"],
})
export class CandidateMyJobsComponent implements OnInit {
  @HostBinding("class") hostclasses = "nonstandardiconcontainer container px-0 viewfor__DETAILS viewhas__filters";
  constructor(
    private apiService: ApiService,
    private commonutilsService: CommonutilsService,
    private communicationService: CommunicationService,
    public dialogService: DialogService,
    public templateutilsService: TemplateutilsService,
    private toastService: ToastService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.initializeFormGroup();

    this.route.queryParams.subscribe((queryParams) => {
      if (queryParams.page_number) {
        this.page_number = parseInt(queryParams.page_number);
      } else {
        this.page_number = 1;
      }
      this.load_data();
    });
  }

  filter_data = {};
  filtermodalid: number;
  openFilterModal() {
    if (this.filtermodalid === undefined) {
      this.filtermodalid = Math.floor(Math.random() * 10000 + 1);
    }
    this.communicationService.communication = { context: { parentid: this.filtermodalid }, obj: { filter_data: this.filter_data } };
    this.dialogService.openComponent("CandidateSearchJobsFilterComponent", "CandidateSearchJobsFilterModule", this.dialogService.DIALOGTYPES.LG, undefined, "fullscreendialog");
    let parentSubscription = this.communicationService.detectNewCommunication().subscribe(() => {
      if (this.communicationService.communication.context.parentid != this.filtermodalid) {
        return;
      }
      this.filter_data = this.communicationService.communication.obj.filter_data;
      this.page_number = 1;
      var newQueryParams = Object.assign({}, this.route.snapshot.queryParams);
      for (var paramKey in this.filter_data) {
        if (newQueryParams[paramKey] !== undefined) {
          delete newQueryParams[paramKey];
        }
      }
      if (newQueryParams["page_number"] !== undefined) {
        delete newQueryParams["page_number"];
      }
      this.router.navigate([], { queryParams: newQueryParams });
      this.load_data();
      parentSubscription.unsubscribe();
    });
  }

  sort_data = { sort_by: "id", sort_order: "DESC" };
  sortmodalid: number;
  openSortModal() {
    if (this.sortmodalid === undefined) {
      this.sortmodalid = Math.floor(Math.random() * 10000 + 1);
    }
    this.communicationService.communication = { context: { parentid: this.sortmodalid }, obj: { sort_data: this.sort_data } };
    this.dialogService.openComponent("CandidateSearchJobsSortComponent", "CandidateSearchJobsSortModule", this.dialogService.DIALOGTYPES.LG, undefined, "fullscreendialog");
    let parentSubscription = this.communicationService.detectNewCommunication().subscribe(() => {
      if (this.communicationService.communication.context.parentid != this.sortmodalid) {
        return;
      }
      this.sort_data = this.communicationService.communication.obj.sort_data;
      this.load_data();
      parentSubscription.unsubscribe();
    });
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("filter_button", new FormControl(""));
    this.localform.addControl("jobs", new FormArray([]));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  markJobAsFavorite(formgroup) {
    this.apiService.post_api("jobportal", "markJobAsFavorite", { job_id: formgroup.value["id"] }).subscribe((res) => {
      formgroup.controls["favorite"].setValue(res.inserted_entity_id);
    });
  }
  unmarkJobAsFavorite(formgroup) {
    this.apiService.post_api("jobportal", "unmarkJobAsFavorite", { id: formgroup.value["favorite"] }).subscribe((res) => {
      formgroup.controls["favorite"].setValue(false);
    });
  }
  addNode_jobs(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("video_id", new FormControl(""));
    nodeControl.addControl("company_photo_id", new FormControl(""));
    nodeControl.addControl("job_title", new FormControl(""));
    nodeControl.addControl("employer_mobile", new FormControl(""));
    nodeControl.addControl("mobiletitle", new FormControl(""));
    nodeControl.addControl("job_location_city", new FormControl(""));
    nodeControl.addControl("min_job_salary", new FormControl("", Validators.min(0)));
    nodeControl.addControl("num_of_openings", new FormControl("", Validators.min(0)));
    nodeControl.addControl("experience", new FormControl(""));
    nodeControl.addControl("applied_on", new FormControl(""));
    nodeControl.addControl("favorite_id", new FormControl(""));
    nodeControl.addControl("view_details", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("apply", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("apply", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("not_favorite", new FormControl(""));
    nodeControl.addControl("favorite", new FormControl(""));
    nodeControl.addControl("application_id", new FormControl(""));
    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("num_of_applications", new FormControl("", this.skipsubmitValidator));

    if (nodeData !== undefined) {
      nodeControl.controls["video_id"].setValue(nodeData["video_id"]);
      nodeControl.controls["company_photo_id"].setValue(nodeData["company_photo_id"]);
      nodeControl.controls["job_title"].setValue(nodeData["job_title"]);
      nodeControl.controls["employer_mobile"].setValue(nodeData["employer_mobile"]);
      nodeControl.controls["mobiletitle"].setValue(nodeData["mobiletitle"]);
      nodeControl.controls["job_location_city"].setValue(nodeData["job_location_city"]);
      nodeControl.controls["min_job_salary"].setValue(nodeData["min_job_salary"]);
      nodeControl.controls["num_of_openings"].setValue(nodeData["num_of_openings"]);
      nodeControl.controls["experience"].setValue(nodeData["experience"]);
      nodeControl.controls["applied_on"].setValue(nodeData["applied_on"]);
      nodeControl.controls["favorite_id"].setValue(nodeData["favorite_id"]);
      nodeControl.controls["view_details"].setValue(nodeData["view_details"]);
      nodeControl.controls["apply"].setValue(nodeData["apply"]);
      nodeControl.controls["apply"].setValue(nodeData["apply"]);
      nodeControl.controls["not_favorite"].setValue(nodeData["not_favorite"]);
      nodeControl.controls["favorite"].setValue(nodeData["favorite"]);
      nodeControl.controls["application_id"].setValue(nodeData["application_id"]);
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["num_of_applications"].setValue(nodeData["num_of_applications"]);
    }
    (<FormArray>this.localform.controls["jobs"]).push(nodeControl);
  }

  removeNode_jobs(index) {
    (<FormArray>this.localform.controls["jobs"]).removeAt(index);
  }

  records_per_page = 6;
  page_number = 1;
  total_pages;
  load_data_page(page_number) {
    var newQueryParams = Object.assign({}, this.route.snapshot.queryParams);
    newQueryParams["page_number"] = page_number;
    this.router.navigate(["."], { relativeTo: this.route, queryParams: newQueryParams });
  }
  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getappliedjobsofcandidate", { filters: this.filter_data, order: this.sort_data, records_per_page: this.records_per_page, page_number: this.page_number }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res.response;
      if (!isNaN(res.total)) {
        this.total_pages = Math.ceil(res.total / this.records_per_page);
      } else {
        this.total_pages = Math.ceil(res.total[Object.keys(res.total)[0]] / this.records_per_page);
      }

      this.localform.controls["filter_button"].setValue(this.response_data["filter_button"]);
      this.localform.controls["jobs"] = new FormArray([]);
      if (this.response_data["jobs"] !== undefined) {
        this.response_data["jobs"].forEach((node) => {
          this.addNode_jobs(node);
        });
      }
    });
  }

  response_category_id_options: any;
  load_category_id_options() {
    return this.apiService.post_api("jobportal", "getjobcategoriesoptions");
  }

  response_role_id_options: any;
  load_role_id_options() {
    return this.apiService.post_api("jobportal", "getjobrolesoptions");
  }
}
