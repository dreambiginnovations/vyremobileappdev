import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { LoginSignupComponent } from "./login_signup.component";

export const routes: Routes = [
  {
    path: "",
    component: LoginSignupComponent,
  },
];

@NgModule({
  declarations: [LoginSignupComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class LoginSignupModule {
  static components = {
    LoginSignupComponent: LoginSignupComponent,
  };
}
