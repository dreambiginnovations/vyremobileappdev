import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { DialogService } from "../services/dialog.service";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { EnvService } from "../services/env.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__login_signup",
  templateUrl: "./login_signup.template.html",
  styleUrls: ["./login_signup.component.scss"],
})
export class LoginSignupComponent implements OnInit {
  @HostBinding("class") hostclasses = "viewascardondesktop bg-white px-0 twocolumnleftimage container viewfor__EDIT";
  constructor(private dialogService: DialogService, private apiService: ApiService, private commonutilsService: CommonutilsService, public envService: EnvService, public templateutilsService: TemplateutilsService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("mobile", new FormControl("", Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])));
  }

  success_state = { route: "login_signup_otp" };
  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
      localStorage.removeItem("mobile");
      localStorage.setItem("mobile", this.localform.value["mobile"]);
      this.submitFormAPI();
    }
  }

  submitformres: any;
  submitFormAPI() {
    this.apiService.post_api("jobportal", "generateotp", this.localform.value).subscribe((res) => {
      this.submitformres = res;
      if (this.submitformres.message) {
        if (this.submitformres.status == "TRUE") {
          this.toastService.showSuccess("Success", this.submitformres.message);
        } else if (this.submitformres.status == "FALSE") {
          this.toastService.showError("Error", this.submitformres.message);
        } else {
          this.toastService.showSuccess("Success", this.submitformres.message);
        }
      }
      if (res.status == "TRUE" || res.status === true) {
        let route;
        let routeParams;
        [route, routeParams] = this.commonutilsService.processSuccessState(this.success_state);
        this.templateutilsService.navigateToRoute(route, routeParams);
      } else {
      }
    });
  }
}
