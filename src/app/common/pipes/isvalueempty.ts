import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "isvalueempty" })
export class IsValueEmptyPipe implements PipeTransform {
  transform(value: any): boolean {
    if (value === undefined || value === null || value === "") {
      return true;
    } else {
      if (typeof value === "object") {
        if (value == {}) {
          return true;
        } else {
          let isEmpty = true;
          for (let key in value) {
            if (value[key] !== undefined && value[key] !== "") {
              isEmpty = false;
            }
          }
          return isEmpty;
        }
      } else {
        return false;
      }
    }
  }
}
