import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "fieldvaluetransform" })
export class FieldValuePipe implements PipeTransform {
  transform(value: any, ...args: any[]): number {
    let transform = args[0];
    if (transform == "truncate") {
      return this.transform_truncate(value, args);
    } else if (transform == "formatcurrency") {
      return this.transform_formatcurrency(value, args);
    } else if (transform == "append") {
      return this.transform_append(value, args);
    } else if (transform == "formatdate") {
      return this.transform_formatdate(value, args);
    } else if (transform == "calculateage") {
      return this.transform_calculateage(value, args);
    }
    return value;
  }

  transform_truncate(value: any, args: any) {
    let length = args[1];
    let ending = "...";
    if (args.length >= 3) {
      ending = args[2];
    }
    if (value == null || value == undefined) {
      return value;
    } else if (value.length < length) {
      return value;
    } else {
      return value.substring(0, length) + ending;
    }
  }

  transform_formatcurrency(value, args) {
    if (value == null || value == undefined) {
      return value;
    } else if (isNaN(value)) {
      return value;
    } else {
      let x = value.toString();
      let lastThree = x.substring(x.length - 3);
      let otherNumbers = x.substring(0, x.length - 3);
      if (otherNumbers != "") {
        lastThree = "," + lastThree;
      }
      let res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
      return res;
    }
  }

  transform_append(value, args) {
    if (value == null || value == undefined) {
      return value;
    } else {
      return value + args[1];
    }
  }

  transform_formatdate(value, args) {
    if (value == null || value == undefined) {
      return value;
    }
    const options: Intl.DateTimeFormatOptions = { year: "numeric", month: "long", day: "numeric" };
    //const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    let date = new Date(value);
    let dateString = date.toLocaleDateString("en-US", options);
    return dateString;
  }

  transform_calculateage(value, args) {
    if (value == null || value == undefined) {
      return value;
    }
    let date = new Date(value);
    let ageDifMs = Date.now() - date.getTime();
    let ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970) + " Yrs";
  }
}
