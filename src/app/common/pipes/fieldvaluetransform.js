"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var FieldValuePipe = /** @class */ (function () {
    function FieldValuePipe() {
    }
    FieldValuePipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var transform = args[0];
        if (transform == "truncate") {
            return this.transform_truncate(value, args);
        }
        else if (transform == "formatcurrency") {
            return this.transform_formatcurrency(value, args);
        }
        else if (transform == "append") {
            return this.transform_append(value, args);
        }
        else if (transform == "formatdate") {
            return this.transform_formatdate(value, args);
        }
        else if (transform == "calculateage") {
            return this.transform_calculateage(value, args);
        }
        return value;
    };
    FieldValuePipe.prototype.transform_truncate = function (value, args) {
        var length = args[1];
        var ending = "...";
        if (args.length >= 3) {
            ending = args[2];
        }
        if (value == null || value == undefined) {
            return value;
        }
        else if (value.length < length) {
            return value;
        }
        else {
            return value.substring(0, length) + ending;
        }
    };
    FieldValuePipe.prototype.transform_formatcurrency = function (value, args) {
        if (value == null || value == undefined) {
            return value;
        }
        else if (isNaN(value)) {
            return value;
        }
        else {
            var x = value.toString();
            var lastThree = x.substring(x.length - 3);
            var otherNumbers = x.substring(0, x.length - 3);
            if (otherNumbers != "") {
                lastThree = "," + lastThree;
            }
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            return res;
        }
    };
    FieldValuePipe.prototype.transform_append = function (value, args) {
        if (value == null || value == undefined) {
            return value;
        }
        else {
            return value + args[1];
        }
    };
    FieldValuePipe.prototype.transform_formatdate = function (value, args) {
        if (value == null || value == undefined) {
            return value;
        }
        var options = { year: "numeric", month: "long", day: "numeric" };
        //const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        var date = new Date(value);
        var dateString = date.toLocaleDateString("en-US", options);
        return dateString;
    };
    FieldValuePipe.prototype.transform_calculateage = function (value, args) {
        if (value == null || value == undefined) {
            return value;
        }
        var date = new Date(value);
        var ageDifMs = Date.now() - date.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970) + " Yrs";
    };
    FieldValuePipe = __decorate([
        core_1.Pipe({ name: "fieldvaluetransform" })
    ], FieldValuePipe);
    return FieldValuePipe;
}());
exports.FieldValuePipe = FieldValuePipe;
