import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FieldValuePipe } from "./fieldvaluetransform";
import { IsValueEmptyPipe } from "./isvalueempty";

@NgModule({
  declarations: [FieldValuePipe, IsValueEmptyPipe],
  imports: [CommonModule],
  exports: [FieldValuePipe, IsValueEmptyPipe],
})
export class PipesModule {}
