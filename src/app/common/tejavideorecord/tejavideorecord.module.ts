import { NgModule } from "@angular/core";

import { TejaVideoRecordComponent } from "./tejavideorecord.component";

@NgModule({
  declarations: [ TejaVideoRecordComponent ],
  imports: [],
  exports: [ TejaVideoRecordComponent ],
})
export class TejaVideoRecordModule {
  
}