import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";

@Component({
  selector: ".tejavideo-record",
  templateUrl: "./tejavideorecord.template.html"
})
export class TejaVideoRecordComponent implements OnInit {
    @ViewChild('video') videoElementRef: ElementRef;
    videoElement: HTMLVideoElement;
    stream: MediaStream;
    idx = Math.floor(1000*Math.random()) + "";
    constructor() {}
    checkIfMediaDevicesSupported() {
        if (navigator.mediaDevices?.getUserMedia) {
            this.getAudioAndVideoDevices();
            return true;
        } else {
            alert("Video recording is not supported on this device");
            return false;
        }
    }
    getAudioAndVideoDevices() {
        navigator.mediaDevices
            .enumerateDevices()
            .then((devices) => {
                let audioSource = null;
                let videoSource = null;
                console.log("devices", devices);
                /*devices.forEach((device) => {
                if (device.kind === "audioinput") {
                  audioSource = device.deviceId;
                } else if (device.kind === "videoinput") {
                  videoSource = device.deviceId;
                }
                });
                sourceSelected(audioSource, videoSource);*/
            })
            .catch((err) => {
              console.error(`${err.name}: ${err.message}`);
            });
    }
    captureVideoAndAudioStream() {
        navigator.mediaDevices.getUserMedia({ video: true, audio: true })
            .then((stream) => {
                this.videoElement = this.videoElementRef.nativeElement;
                this.videoElement.srcObject = stream;
                this.videoElement.play();
            })
            .catch((error) => {
                console.log("Rejected!", error);
            });
    }
    async ngOnInit() {
        if(!this.checkIfMediaDevicesSupported()) {
            return;
        }
        this.captureVideoAndAudioStream();
    }
}