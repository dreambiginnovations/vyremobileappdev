import { Component, Input, SimpleChanges } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: "customdropdown",
  templateUrl: "./dropdown.template.html",
})
export class DropDownComponent {
  @Input() field: any;
  @Input() form: FormGroup;
  fieldOptionValuesMap: any;

  constructor() {}
  ngOnInit() {
    var formControl = new FormControl(this.field.value);
    try {
      if (!this.form.controls[this.field.name]) {
        this.form.addControl(this.field.name, formControl);
      }
    } catch (error) {
      //console.log(error, this.field, this.form);
    }
  }
  ngAfterViewInit() {
    if (!this.form.value || !this.form.value[this.field.name]) {
      if (this.field.config.assign_first_option && this.field.options && this.field.options.length > 0) {
        this.form.controls[this.field.name].setValue(this.field.options[0].key);
      }
    }
    if (this.field.config.addblankoption && this.field.options && this.field.options.length > 0) {
      if (this.field.options[0].key !== this.field.config.addblankoption.key) {
        this.field.options.unshift(this.field.config.addblankoption);
      }
    }

    if (!this.field.editable && this.field.options) {
      this.fieldOptionValuesMap = {};
      for (let count = 0; count < this.field.options.length; count++) {
        this.fieldOptionValuesMap[this.field.options[count].key] = this.field.options[count].label;
      }
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    this.ngAfterViewInit();
  }
}
