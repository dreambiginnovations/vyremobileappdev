"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var DropDownComponent = /** @class */ (function () {
    function DropDownComponent() {
    }
    DropDownComponent.prototype.ngOnInit = function () {
        var formControl = new forms_1.FormControl(this.field.value);
        try {
            if (!this.form.controls[this.field.name]) {
                this.form.addControl(this.field.name, formControl);
            }
        }
        catch (error) {
            //console.log(error, this.field, this.form);
        }
    };
    DropDownComponent.prototype.ngAfterViewInit = function () {
        if (!this.form.value || !this.form.value[this.field.name]) {
            if (this.field.config.assign_first_option && this.field.options && this.field.options.length > 0) {
                this.form.controls[this.field.name].setValue(this.field.options[0].key);
            }
        }
        if (this.field.config.addblankoption && this.field.options && this.field.options.length > 0) {
            if (this.field.options[0].key !== this.field.config.addblankoption.key) {
                this.field.options.unshift(this.field.config.addblankoption);
            }
        }
        if (!this.field.editable && this.field.options) {
            this.fieldOptionValuesMap = {};
            for (var count = 0; count < this.field.options.length; count++) {
                this.fieldOptionValuesMap[this.field.options[count].key] = this.field.options[count].label;
            }
        }
    };
    DropDownComponent.prototype.ngOnChanges = function (changes) {
        this.ngAfterViewInit();
    };
    __decorate([
        core_1.Input()
    ], DropDownComponent.prototype, "field");
    __decorate([
        core_1.Input()
    ], DropDownComponent.prototype, "form");
    DropDownComponent = __decorate([
        core_1.Component({
            selector: "customdropdown",
            templateUrl: "./dropdown.template.html"
        })
    ], DropDownComponent);
    return DropDownComponent;
}());
exports.DropDownComponent = DropDownComponent;
