import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { DropDownComponent } from "./dropdown.component";

@NgModule({
  declarations: [DropDownComponent],
  imports: [FormsModule, ReactiveFormsModule, CommonModule],
  exports: [DropDownComponent],
})
export class CustomDropDownModule {
  static components = {
    DropDownComponent,
  };
}
