import { Component, ElementRef, Input, Inject, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import videojs from "video.js";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: ".app-vjs-player",
  templateUrl: "./vjs-player.component.html",
  encapsulation: ViewEncapsulation.None,
})
export class VjsPlayerComponent implements OnInit, OnDestroy {
  @ViewChild("target", { static: true }) target: ElementRef;
  @Input() options: {
    fluid: boolean;
    aspectRatio: string;
    autoplay: boolean;
    poster: string;
    sources: {
      src: string;
      type: string;
    }[];
  };
  player: any;

  constructor(private elementRef: ElementRef, public dialogRef: MatDialogRef<VjsPlayerComponent>, @Inject(MAT_DIALOG_DATA) public data) {}

  ngOnInit() {
    if (this.options === undefined) {
      this.options = { ...this.data.componentContext };
    }
    console.log("this.options", this.options);
  }

  ngAfterViewInit() {
    console.log("videoplayer", this.options, this.data);
    this.player = videojs(this.target.nativeElement, this.options, function onPlayerReady() {
      console.log("Player ready", this.player.controls);
    });
    this.player.on("error", function (e) {
      console.log("videojs error", e);
    });
  }

  ngOnDestroy() {
    if (this.player) {
      this.player.dispose();
    }
  }
}
