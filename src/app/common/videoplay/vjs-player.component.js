"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var video_js_1 = require("video.js");
var dialog_1 = require("@angular/material/dialog");
var VjsPlayerComponent = /** @class */ (function () {
    function VjsPlayerComponent(elementRef, dialogRef, data) {
        this.elementRef = elementRef;
        this.dialogRef = dialogRef;
        this.data = data;
    }
    VjsPlayerComponent.prototype.ngOnInit = function () {
        if (this.options === undefined) {
            this.options = __assign({}, this.data.componentContext);
        }
        console.log("this.options", this.options);
    };
    VjsPlayerComponent.prototype.ngAfterViewInit = function () {
        console.log("videoplayer", this.options, this.data);
        this.player = video_js_1["default"](this.target.nativeElement, this.options, function onPlayerReady() {
            console.log("Player ready", this.player.controls);
        });
        this.player.on("error", function (e) {
            console.log("videojs error", e);
        });
    };
    VjsPlayerComponent.prototype.ngOnDestroy = function () {
        if (this.player) {
            this.player.dispose();
        }
    };
    __decorate([
        core_1.ViewChild("target", { static: true })
    ], VjsPlayerComponent.prototype, "target");
    __decorate([
        core_1.Input()
    ], VjsPlayerComponent.prototype, "options");
    VjsPlayerComponent = __decorate([
        core_1.Component({
            selector: ".app-vjs-player",
            templateUrl: "./vjs-player.component.html",
            encapsulation: core_1.ViewEncapsulation.None
        }),
        __param(2, core_1.Inject(dialog_1.MAT_DIALOG_DATA))
    ], VjsPlayerComponent);
    return VjsPlayerComponent;
}());
exports.VjsPlayerComponent = VjsPlayerComponent;
