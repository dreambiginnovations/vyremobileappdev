import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormControl, FormArray } from "@angular/forms";
import { AWSService } from "../../services/aws.service";
import { ApiService } from "../../services/api.service";
import { EnvService } from "../../services/env.service";
import { ToastService } from "../../services/toast.service";

@Component({
  selector: ".file-upload",
  templateUrl: "./fileuploader.template.html",
})
export class FileuploaderComponent implements OnInit {
  @Input() field: FileuploaderField;
  @Input() localform: FormGroup;

  randomid: string;

  constructor(private awsService: AWSService, private apiService: ApiService, private envService: EnvService, private toastService: ToastService) {}
  ngOnInit() {
    this.generateRandomId();
    this.initializeField();
  }
  ngOnChanges() {
    this.initializeField();
  }

  initializeField() {
    if (this.field.data !== undefined) {
      this.field.file_id = this.field.data[this.field.file_id_key];
      this.field.file_name = this.field.data[this.field.file_name_key];
      this.field.file_url = this.field.data[this.field.file_url_key];
      this.field.file_aws_name = this.field.data[this.field.file_aws_name_key];
    }
  }
  file_upload() {
    document.getElementById(this.randomid).click();
  }
  view_file(url) {
    window.open(url, "_blank");
  }
  generateRandomId() {
    this.randomid = "randomid_" + Math.floor(Math.random() * 1000);
  }

  createFileName(externalFileName) {
    const fileName = "file_" + this.field.file_id + "_" + this.randomid + "_" + externalFileName;
    return fileName;
  }
  handleFileInput(files) {
    var isNewFile, fileName, externalFileName;
    var uploadedFile = files.item(0);
    externalFileName = uploadedFile.name;
    var fileType = this.getFileTypeFromFileName(externalFileName);
    if (this.field.allowedtypes !== undefined && !this.field.allowedtypes.includes(fileType)) {
      this.toastService.showError("Error", "You are uploading an Invalid Format. Only permitted Format(s): " + this.field.allowedtypes.join(", "));
      return;
    }
    if (this.field.file_id !== undefined && this.field.file_id !== null) {
      this.awsService.deleteExistingFile(this.field.file_aws_name, this.field.category);
      fileName = this.createFileName(externalFileName);
      isNewFile = false;
      this.uploadFile(fileName, externalFileName, this.field.category, isNewFile, uploadedFile);
    } else {
      isNewFile = true;
      this.apiService.post_api("jobportal", "insertEmptyMedia", { media_type: fileType }).subscribe((mediaRes) => {
        this.field.file_id = mediaRes.inserted_entity_id;
        fileName = this.createFileName(externalFileName);
        this.localform.controls[this.field.file_id_key].setValue(this.field.file_id);
        this.uploadFile(fileName, externalFileName, this.field.category, isNewFile, uploadedFile);
      });
    }
  }
  getFileTypeFromFileName(externalFileName) {
    var fileTypeExtensionsMap = {
      image: ["png", "gif", "jpg", "jpeg", "bmp"],
      video: ["mp4", "webm"],
      pdf: ["pdf"],
    };
    var uploadedFileType;
    for (var fileType in fileTypeExtensionsMap) {
      for (var extIndex = 0; extIndex < fileTypeExtensionsMap[fileType].length; extIndex++) {
        if (externalFileName.includes("." + fileTypeExtensionsMap[fileType][extIndex])) {
          uploadedFileType = fileType;
        }
      }
    }
    return uploadedFileType;
  }

  refreshing: boolean;
  uploadFile(fileName, externalFileName, fileCategory, isNewFile, uploadedFile) {
    this.refreshing = true;
    this.awsService.prepareToUploadFileToS3(fileName, fileCategory, isNewFile).subscribe((res) => {
      const params = { url: res.url, fileContent: uploadedFile, headers: res.headers };
      this.awsService.uploadFileToS3(params);
      const mediaFileURL = this.envService.values.ugcmediaurl + fileName;
      this.apiService.post_api("jobportal", "updateMedia", { id: this.field.file_id, media_file_internal_name: fileName, media_file_external_name: externalFileName, media_file_url: mediaFileURL }).subscribe((dummyRes) => {
        this.field.file_name = externalFileName;
        this.field.file_aws_name = fileName;
        this.field.file_url = mediaFileURL;
        this.postProcessAfterUpload();
      });
    });
  }
  postProcessAfterUpload() {
    const pollingInterval = setInterval(() => {
      if (!this.awsService.uploadInProgress) {
        clearInterval(pollingInterval);
        this.refreshing = false;
        this.generateRandomId();
      }
    }, 1000);
  }
}

export interface FileuploaderField {
  editable: boolean;
  data: any;
  allowedtypes?: string[];

  file_url_key: string;
  file_name_key: string;
  file_id_key: string;
  file_aws_name_key: string;
  category: string;

  file_url: string;
  file_name: string;
  file_aws_name: string;
  file_id: number;
}
