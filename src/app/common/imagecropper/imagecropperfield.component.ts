import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";

import { DialogService } from "../../services/dialog.service";
import { ApiService } from "../../services/api.service";
import { EnvService } from "../../services/env.service";
import { CommunicationService } from "../../services/communication.service";
import { AWSService } from "../../services/aws.service";

@Component({
  selector: ".image-cropper-field",
  templateUrl: "./imagecropperfield.component.html",
  styleUrls: ["./imagecropper.scss"],
})
export class ImageCropperFieldComponent implements OnInit, OnChanges {
  constructor(private dialog: DialogService, private communicationService: CommunicationService, private awsService: AWSService, private apiService: ApiService, public envService: EnvService) {
    this.generateRandomId();
  }
  @Input() field: ImageCropperField;
  @Input() localform: FormGroup;

  randomid: string;
  imageChangedEvent: any = "";

  refreshingImage = false;

  generateRandomId() {
    this.randomid = "randomid_" + Math.floor(Math.random() * 1000);
  }

  ngOnInit() {
    this.initializeField();
  }

  ngOnChanges() {
    this.initializeField();
  }

  initializeField() {
    if (this.field.data !== undefined) {
      this.field.media_id = this.field.data[this.field.media_id_key];
      this.field.media_name = this.field.data[this.field.media_name_key];
    }
    this.computeFieldName();
  }
  computeFieldName() {
    if (this.field.media_name && this.field.media_name.startsWith("http")) {
      this.field.computed_media_name = this.field.media_name;
    } else {
      this.field.computed_media_name = this.envService.values["ugcmediaurl"] + this.field.media_name;
    }
  }

  imagecropper_upload() {
    document.getElementById(this.randomid).click();
  }
  imagecropper_event(event: any): void {
    const imgValue = document.getElementById(this.randomid)["value"];
    if (imgValue !== "") {
      this.initializeField();
      this.dialog.openImageCropper(this.randomid, event, this.field.roundCropper);
      const imagecropSubscription = this.communicationService.detectNewCommunication().subscribe(() => {
        if (this.communicationService.communication.context.parentid != this.randomid) {
          return;
        }
        const croppedImage = this.communicationService.communication.obj;
        let fileName, isNewFile;
        const fileCategory = this.field.category;

        if (this.field.media_id !== undefined && this.field.media_id !== null && this.field.media_id !== 0) {
          isNewFile = false;
          this.awsService.deleteExistingFile(this.field.media_name, fileCategory);
          fileName = this.createFileName();
          this.uploadFile(fileName, fileCategory, isNewFile, croppedImage);
        } else {
          isNewFile = true;
          this.apiService.post_api("jobportal", "insertEmptyMedia", { media_type: "image" }).subscribe((mediaRes) => {
            this.field.media_id = mediaRes.inserted_entity_id;
            fileName = this.createFileName();
            this.localform.controls[this.field.media_id_key].patchValue(this.field.media_id);
            console.log("updated localform", this.localform.value[this.field.media_id_key]);
            this.uploadFile(fileName, fileCategory, isNewFile, croppedImage);
          });
        }
        imagecropSubscription.unsubscribe();
      });
      setTimeout(() => {
        document.getElementById(this.randomid)["value"] = "";
      }, 2000);
    }
  }
  uploadFile(fileName, fileCategory, isNewFile, croppedImage) {
    this.awsService.prepareToUploadFileToS3(fileName, fileCategory, isNewFile).subscribe((res) => {
      const params = {
        url: res.url,
        fileContent: croppedImage,
        headers: res.headers,
      };
      this.awsService.uploadBase64ImageToS3(params);
      this.apiService
        .post_api("jobportal", "updateMedia", {
          id: this.field.media_id,
          media_file_internal_name: fileName,
          media_file_url: this.envService.values.ugcmediaurl + fileName,
        })
        .subscribe((dummyRes) => {
          this.field.media_name = fileName;
          this.generateRandomId();
          this.refreshingImage = true;
          this.computeFieldName();
          setTimeout(() => {
            this.refreshingImage = false;
          }, 2000);
        });
    });
  }

  createFileName() {
    const fileName = "media_" + this.field.media_id + "_" + this.randomid + ".png";
    return fileName;
  }

  imageNotLoadingTime = 200;
  imageNotLoading(event) {
    this.imageNotLoadingTime += 300;
    if (event.target.altSRC === undefined) {
      event.target.altSRC = event.target.src;
    }
    if (this.imageNotLoadingTime < 1000) {
      this.apiService.get_api_alt(event.target.altSRC).subscribe(
        (res) => {
          event.target.src = event.target.altSRC;
        },
        (error) => {
          setTimeout(() => {
            event.target.src = this.envService.values["staticmediaurl"] + "loading.gif";
            this.imageNotLoading(event);
          }, this.imageNotLoadingTime);
        }
      );
    } else {
      event.target.src = "assets/loadingfailed.png";
    }
  }
}

export interface ImageCropperField {
  editable: boolean;
  media_id_key: string | number;
  media_name_key: string;
  category: string;

  data?: any;
  roundCropper?: boolean;
  media_id?: string | number;
  media_name?: string;
  computed_media_name?: string;
}
