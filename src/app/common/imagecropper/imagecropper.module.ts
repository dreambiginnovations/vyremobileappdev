import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ImageCropperModule } from "ngx-image-cropper";

import { ImageCropperDialogComponent } from "./imagecropperdialog.component";
import { ImageCropperFieldComponent } from "./imagecropperfield.component";

@NgModule({
  declarations: [ImageCropperDialogComponent, ImageCropperFieldComponent],
  imports: [ImageCropperModule, CommonModule],
  exports: [ImageCropperDialogComponent, ImageCropperFieldComponent],
})
export class ImgCropperModule {
  static components = {
    ImageCropperDialogComponent,
    ImageCropperFieldComponent,
  };
}
