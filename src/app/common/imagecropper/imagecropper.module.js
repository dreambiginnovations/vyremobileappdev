"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var ngx_image_cropper_1 = require("ngx-image-cropper");
var imagecropperdialog_component_1 = require("./imagecropperdialog.component");
var imagecropperfield_component_1 = require("./imagecropperfield.component");
var ImgCropperModule = /** @class */ (function () {
    function ImgCropperModule() {
    }
    ImgCropperModule.components = {
        ImageCropperDialogComponent: imagecropperdialog_component_1.ImageCropperDialogComponent,
        ImageCropperFieldComponent: imagecropperfield_component_1.ImageCropperFieldComponent
    };
    ImgCropperModule = __decorate([
        core_1.NgModule({
            declarations: [imagecropperdialog_component_1.ImageCropperDialogComponent, imagecropperfield_component_1.ImageCropperFieldComponent],
            imports: [ngx_image_cropper_1.ImageCropperModule, common_1.CommonModule],
            exports: [imagecropperdialog_component_1.ImageCropperDialogComponent, imagecropperfield_component_1.ImageCropperFieldComponent]
        })
    ], ImgCropperModule);
    return ImgCropperModule;
}());
exports.ImgCropperModule = ImgCropperModule;
