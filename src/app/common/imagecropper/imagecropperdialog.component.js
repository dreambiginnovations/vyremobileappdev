"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var dialog_1 = require("@angular/material/dialog");
var ImageCropperDialogComponent = /** @class */ (function () {
    function ImageCropperDialogComponent(dialogRef, data, communicationService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.communicationService = communicationService;
        this.aspectRatio = 1;
        this.imageChangedEvent = "";
        this.croppedImage = "";
    }
    ImageCropperDialogComponent.prototype.ngOnInit = function () {
        console.log("data in imagecropper", this.data);
        this.imageChangedEvent = this.data.imageChangedEvent;
        this.parentId = this.data.parentId;
        this.roundCropper = this.data.roundCropper;
        // if(this.roundCropper) {this.aspectRatio = "auto";}
    };
    ImageCropperDialogComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    ImageCropperDialogComponent.prototype.done = function () {
        this.communicationService.broadcastCommunication({
            context: { parentid: this.parentId },
            obj: this.croppedImage
        });
        this.closeDialog();
    };
    ImageCropperDialogComponent.prototype.imageCropped = function (event) {
        this.croppedImage = event.base64;
    };
    // possible aspect ratios: 1/1, 4/3, 16/9
    ImageCropperDialogComponent.prototype.imageLoaded = function (image) {
        if (!this.roundCropper) {
            this.aspectRatio = image.original.size.width / image.original.size.height;
        }
    };
    ImageCropperDialogComponent.prototype.cropperReady = function () {
        // cropper ready
    };
    ImageCropperDialogComponent.prototype.loadImageFailed = function () {
        // show message
    };
    ImageCropperDialogComponent = __decorate([
        core_1.Component({
            selector: ".image-cropper-dialog",
            templateUrl: "./imagecropperdialog.component.html"
        }),
        __param(1, core_1.Inject(dialog_1.MAT_DIALOG_DATA))
    ], ImageCropperDialogComponent);
    return ImageCropperDialogComponent;
}());
exports.ImageCropperDialogComponent = ImageCropperDialogComponent;
