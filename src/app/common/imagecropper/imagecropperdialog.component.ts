import { Component, OnInit, Inject } from "@angular/core";
import { ImageCroppedEvent } from "ngx-image-cropper";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { CommunicationService } from "../../services/communication.service";

@Component({
  selector: ".image-cropper-dialog",
  templateUrl: "./imagecropperdialog.component.html",
})
export class ImageCropperDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<ImageCropperDialogComponent>, @Inject(MAT_DIALOG_DATA) public data, private communicationService: CommunicationService) {}
  parentId: number | string;
  roundCropper: boolean;
  aspectRatio = 1;

  imageChangedEvent: any = "";
  croppedImage: any = "";
  ngOnInit() {
    console.log("data in imagecropper", this.data);
    this.imageChangedEvent = this.data.imageChangedEvent;
    this.parentId = this.data.parentId;
    this.roundCropper = this.data.roundCropper;
    // if(this.roundCropper) {this.aspectRatio = "auto";}
  }
  closeDialog() {
    this.dialogRef.close();
  }
  done() {
    this.communicationService.broadcastCommunication({
      context: { parentid: this.parentId },
      obj: this.croppedImage,
    });
    this.closeDialog();
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  // possible aspect ratios: 1/1, 4/3, 16/9
  imageLoaded(image) {
    if (!this.roundCropper) {
      this.aspectRatio = image.original.size.width / image.original.size.height;
    }
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
}
