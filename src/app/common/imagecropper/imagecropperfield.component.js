"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var ImageCropperFieldComponent = /** @class */ (function () {
    function ImageCropperFieldComponent(dialog, communicationService, awsService, apiService, envService) {
        this.dialog = dialog;
        this.communicationService = communicationService;
        this.awsService = awsService;
        this.apiService = apiService;
        this.envService = envService;
        this.imageChangedEvent = "";
        this.refreshingImage = false;
        this.imageNotLoadingTime = 200;
        this.generateRandomId();
    }
    ImageCropperFieldComponent.prototype.generateRandomId = function () {
        this.randomid = "randomid_" + Math.floor(Math.random() * 1000);
    };
    ImageCropperFieldComponent.prototype.ngOnInit = function () {
        this.initializeField();
    };
    ImageCropperFieldComponent.prototype.ngOnChanges = function () {
        this.initializeField();
    };
    ImageCropperFieldComponent.prototype.initializeField = function () {
        if (this.field.data !== undefined) {
            this.field.media_id = this.field.data[this.field.media_id_key];
            this.field.media_name = this.field.data[this.field.media_name_key];
        }
        this.computeFieldName();
    };
    ImageCropperFieldComponent.prototype.computeFieldName = function () {
        if (this.field.media_name && this.field.media_name.startsWith("http")) {
            this.field.computed_media_name = this.field.media_name;
        }
        else {
            this.field.computed_media_name = this.envService.values["ugcmediaurl"] + this.field.media_name;
        }
    };
    ImageCropperFieldComponent.prototype.imagecropper_upload = function () {
        document.getElementById(this.randomid).click();
    };
    ImageCropperFieldComponent.prototype.imagecropper_event = function (event) {
        var _this = this;
        var imgValue = document.getElementById(this.randomid)["value"];
        if (imgValue !== "") {
            this.initializeField();
            this.dialog.openImageCropper(this.randomid, event, this.field.roundCropper);
            var imagecropSubscription_1 = this.communicationService.detectNewCommunication().subscribe(function () {
                if (_this.communicationService.communication.context.parentid != _this.randomid) {
                    return;
                }
                var croppedImage = _this.communicationService.communication.obj;
                var fileName, isNewFile;
                var fileCategory = _this.field.category;
                if (_this.field.media_id !== undefined && _this.field.media_id !== null && _this.field.media_id !== 0) {
                    isNewFile = false;
                    _this.awsService.deleteExistingFile(_this.field.media_name, fileCategory);
                    fileName = _this.createFileName();
                    _this.uploadFile(fileName, fileCategory, isNewFile, croppedImage);
                }
                else {
                    isNewFile = true;
                    _this.apiService.post_api("jobportal", "insertEmptyMedia", { media_type: "image" }).subscribe(function (mediaRes) {
                        _this.field.media_id = mediaRes.inserted_entity_id;
                        fileName = _this.createFileName();
                        _this.localform.controls[_this.field.media_id_key].patchValue(_this.field.media_id);
                        console.log("updated localform", _this.localform.value[_this.field.media_id_key]);
                        _this.uploadFile(fileName, fileCategory, isNewFile, croppedImage);
                    });
                }
                imagecropSubscription_1.unsubscribe();
            });
            setTimeout(function () {
                document.getElementById(_this.randomid)["value"] = "";
            }, 2000);
        }
    };
    ImageCropperFieldComponent.prototype.uploadFile = function (fileName, fileCategory, isNewFile, croppedImage) {
        var _this = this;
        this.awsService.prepareToUploadFileToS3(fileName, fileCategory, isNewFile).subscribe(function (res) {
            var params = {
                url: res.url,
                fileContent: croppedImage,
                headers: res.headers
            };
            _this.awsService.uploadBase64ImageToS3(params);
            _this.apiService
                .post_api("jobportal", "updateMedia", {
                id: _this.field.media_id,
                media_file_internal_name: fileName,
                media_file_url: _this.envService.values.ugcmediaurl + fileName
            })
                .subscribe(function (dummyRes) {
                _this.field.media_name = fileName;
                _this.generateRandomId();
                _this.refreshingImage = true;
                _this.computeFieldName();
                setTimeout(function () {
                    _this.refreshingImage = false;
                }, 2000);
            });
        });
    };
    ImageCropperFieldComponent.prototype.createFileName = function () {
        var fileName = "media_" + this.field.media_id + "_" + this.randomid + ".png";
        return fileName;
    };
    ImageCropperFieldComponent.prototype.imageNotLoading = function (event) {
        var _this = this;
        this.imageNotLoadingTime += 300;
        if (event.target.altSRC === undefined) {
            event.target.altSRC = event.target.src;
        }
        if (this.imageNotLoadingTime < 1000) {
            this.apiService.get_api_alt(event.target.altSRC).subscribe(function (res) {
                event.target.src = event.target.altSRC;
            }, function (error) {
                setTimeout(function () {
                    event.target.src = _this.envService.values["staticmediaurl"] + "loading.gif";
                    _this.imageNotLoading(event);
                }, _this.imageNotLoadingTime);
            });
        }
        else {
            event.target.src = "assets/loadingfailed.png";
        }
    };
    __decorate([
        core_1.Input()
    ], ImageCropperFieldComponent.prototype, "field");
    __decorate([
        core_1.Input()
    ], ImageCropperFieldComponent.prototype, "localform");
    ImageCropperFieldComponent = __decorate([
        core_1.Component({
            selector: ".image-cropper-field",
            templateUrl: "./imagecropperfield.component.html",
            styleUrls: ["./imagecropper.scss"]
        })
    ], ImageCropperFieldComponent);
    return ImageCropperFieldComponent;
}());
exports.ImageCropperFieldComponent = ImageCropperFieldComponent;
