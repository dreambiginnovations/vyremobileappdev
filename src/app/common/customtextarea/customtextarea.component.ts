import { Component, Input, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: ".customtextarea",
  templateUrl: "./customtextarea.component.html",
  styleUrls: ["./customtextarea.scss"],
})
export class CustomTextAreaComponent implements OnInit {
  constructor() {}
  @Input() field: CustomTextAreaField;
  @Input() form: FormGroup;

  placeholder: any;
  initialized = false;

  modules = {
    toolbar: [
      ["bold", "italic", "underline", "strike"], // toggled buttons
      [{ list: "ordered" }, { list: "bullet" }],
    ],
  };

  ngOnInit() {
    this.placeholder = this.getplaceholder();
    this.initialized = true;
  }

  getplaceholder() {
    if (this.field.placeholder) {
      return this.field.placeholder;
    }
    return "";
  }
}

export interface CustomTextAreaField {
  editable: boolean;
  value_key: string | number;

  data?: any;
  value?: string;
  placeholder?: string;
}
