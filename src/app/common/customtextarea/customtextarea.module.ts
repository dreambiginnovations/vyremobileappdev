import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from "@angular/forms";
import { QuillModule } from "ngx-quill";

import { CustomTextAreaComponent } from "./customtextarea.component";

@NgModule({
  declarations: [CustomTextAreaComponent],
  imports: [ReactiveFormsModule, QuillModule.forRoot(), CommonModule],
  exports: [CustomTextAreaComponent],
})
export class CustomTextAreaModule {
  static components = {
    CustomTextAreaComponent,
  };
}
