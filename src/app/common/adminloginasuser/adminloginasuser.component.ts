import { Component, OnInit, HostBinding } from "@angular/core";
import { ApiService } from "../../services/api.service";
import { CommonutilsService } from "../../services/commonutils.service";
import { ActivatedRoute } from "@angular/router";
import { ToastService } from "../../services/toast.service";

@Component({
  selector: ".adminloginuser",
  template: `
    <div class="text-center h2 my-5 col-12">
      <div class="mb-2"><img src="https://thumbs.gfycat.com/ConventionalOblongFairybluebird-size_restricted.gif" style="width: 6em;" /></div>
      <div>Logging in as given User</div>
    </div>
  `,
})
export class AdminLoginasUserComponent implements OnInit {
  @HostBinding("class") hostclasses = "container-fluid";

  constructor(private apiService: ApiService, private toastService: ToastService, private route: ActivatedRoute, private commonutilsService: CommonutilsService) {}

  ngOnInit() {
    console.log("Got into AdminLogin thing");
    this.route.queryParams.subscribe((params) => {
      this.apiService.post_api("common", "logout").subscribe((res) => {
        if (params.id === undefined) {
          this.commonutilsService.processResponse({ gotostate: params.gotostate });
        } else {
          this.apiService.post_api("jobportal", "getuserfromidforadmin", { id: params.id, fromadmin: true }).subscribe((res) => {
            this.apiService.post_api("jobportal", "loginmobile", { mobile: res.mobile }).subscribe((res) => {
              console.log("Got this response", res);
              if (res.status && res.status !== "FALSE") {
                this.commonutilsService.processResponse({ gotostate: params.gotostate });
              } else {
                this.toastService.showError("Error", res.message);
              }
            });
          });
        }
      });
    });
  }
}
