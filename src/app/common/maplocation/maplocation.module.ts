import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MapLocationComponent } from "./maplocation.component";

import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
  declarations: [MapLocationComponent],
  imports: [CommonModule, GooglePlaceModule],
  exports: [MapLocationComponent],
})
export class MapLocationModule {
  static components = {
    MapLocationComponent,
  };
}
