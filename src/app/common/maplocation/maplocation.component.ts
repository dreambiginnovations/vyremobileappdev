import { Component, Input, OnInit, OnChanges } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: "maplocation",
  templateUrl: "./maplocation.component.html",
})
export class MapLocationComponent implements OnInit, OnChanges {
  @Input() field: MapLocationField;
  @Input() form: FormGroup;

  formattedAddress: string;
  formattedAddressTruncated: string;
  options: any = {
    componentRestrictions: {
      country: "IND",
    },
  };

  randomid: string;
  constructor() {
    this.generateRandomId();
  }
  generateRandomId() {
    this.randomid = "randomid_" + Math.floor(Math.random() * 1000);
  }
  ngOnInit() {
    this.initializeField();
  }
  ngOnChanges() {
    console.log("maplocation onchanges triggered", this.field, this.form);
    this.initializeField();
  }

  initializeField() {
    if (this.field.data !== undefined) {
      this.field.location_name = this.field.data[this.field.location_name_key];
    }
    if (this.field.location_name) {
      this.updateFormattedAddress(this.field.location_name);
      this.field.edit = false;
    }
    if (this.field.config) {
      if (this.field.config.type == "city") {
        this.options.types = ["(cities)"];
      } else if (this.field.config.type == "state") {
        this.options.types = ["(regions)"];
      }
    }

    setTimeout(() => {
      this.form.valueChanges.subscribe((changes) => {
        console.log("maplocation formchanges triggered");
        if (this.form.value[this.field.location_name_key] === null) {
          this.field.edit = undefined;
          this.formattedAddressTruncated = undefined;
        }
      });
    }, 1000);
  }
  public handleAddressChange(address: any) {
    this.field.edit = false;
    document.getElementById(this.randomid)["value"] = "";
    this.updateFormattedAddress(address.formatted_address);
    this.form.controls[this.field.location_name_key].setValue(address.formatted_address);
    this.form.controls[this.field.location_latitude_key].setValue(address.geometry.location.lat());
    this.form.controls[this.field.location_longitude_key].setValue(address.geometry.location.lng());
    for (let count = 0; count < address.address_components.length; count++) {
      let addressComponent = address.address_components[count];
      if (addressComponent.types[0] == "locality") {
        this.form.controls[this.field.location_city_key].setValue(addressComponent.long_name);
      }
      if (addressComponent.types[0] == "administrative_area_level_1") {
        this.form.controls[this.field.location_state_key].setValue(addressComponent.long_name);
      }
      if (addressComponent.types[0] == "country") {
        this.form.controls[this.field.location_country_key].setValue(addressComponent.long_name);
      }
    }
  }
  public updateFormattedAddress(val) {
    this.formattedAddress = val;
    this.formattedAddressTruncated = this.formattedAddress.substring(0, 75) + "...";
  }
}

export interface MapLocationField {
  editable: boolean;
  location_name_key: string | number;
  location_city_key: string;
  location_state_key: string;
  location_country_key: string;
  location_latitude_key: string;
  location_longitude_key: string;

  data?: any;
  config?: any;
  location_name?: string | number;
  edit: boolean;
}
