"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var buttons_1 = require("ngx-sharebuttons/buttons");
var icons_1 = require("ngx-sharebuttons/icons");
var sharepopup_component_1 = require("./sharepopup.component");
var SharepopupModule = /** @class */ (function () {
    function SharepopupModule() {
    }
    SharepopupModule.components = {
        SharepopupComponent: sharepopup_component_1.SharepopupComponent
    };
    SharepopupModule = __decorate([
        core_1.NgModule({
            declarations: [sharepopup_component_1.SharepopupComponent],
            imports: [common_1.CommonModule, buttons_1.ShareButtonsModule, icons_1.ShareIconsModule]
        })
    ], SharepopupModule);
    return SharepopupModule;
}());
exports.SharepopupModule = SharepopupModule;
