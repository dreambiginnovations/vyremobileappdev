import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ShareButtonsModule } from "ngx-sharebuttons/buttons";
import { ShareIconsModule } from "ngx-sharebuttons/icons";
import { SharepopupComponent } from "./sharepopup.component";

@NgModule({
  declarations: [SharepopupComponent],
  imports: [CommonModule, ShareButtonsModule, ShareIconsModule],
})
export class SharepopupModule {
  static components = {
    SharepopupComponent,
  };
}
