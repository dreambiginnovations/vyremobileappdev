import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: ".sharepopup",
  templateUrl: "./sharepopup.template.html",
})
export class SharepopupComponent implements OnInit {
  url: string;
  theme: string;
  image: string;
  title: string;
  description: string;
  include: any;

  ngOnInit() {
    console.log("this.data", this.data);
    this.url = this.data.componentContext.url;
    this.theme = "material-dark";
    this.image = this.data.componentContext.image;
    this.title = this.data.componentContext.title;
    this.description = this.data.componentContext.description;
    this.include = this.data.componentContext.include;
  }
  constructor(public dialogRef: MatDialogRef<SharepopupComponent>, @Inject(MAT_DIALOG_DATA) public data) {}
}
