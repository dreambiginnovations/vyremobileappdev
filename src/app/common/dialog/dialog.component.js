"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var dialog_1 = require("@angular/material/dialog");
var DialogComponent = /** @class */ (function () {
    function DialogComponent(compiler, injector, lazyloadService, dialogRef, data) {
        this.compiler = compiler;
        this.injector = injector;
        this.lazyloadService = lazyloadService;
        this.dialogRef = dialogRef;
        this.data = data;
        this.loaded = false;
    }
    DialogComponent.prototype.ngOnInit = function () {
        this.componentContext = this.data.componentContext;
        this.componentName = this.data.componentName;
        this.moduleName = this.data.moduleName;
        this.load();
    };
    DialogComponent.prototype.load = function () {
        var _this = this;
        this.lazyloadService["import" + this.moduleName]()
            .then(function (lazyModule) {
            _this.loadedComponent = lazyModule.components[_this.componentName];
            return lazyModule;
        })
            .then(function (lazyModule) { return _this.compiler.compileModuleAsync(lazyModule); })
            .then(function (factory) {
            var c = factory.create(_this.injector);
            _this.loaded = true;
        });
    };
    DialogComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    DialogComponent = __decorate([
        core_1.Component({
            selector: ".dialog-template",
            templateUrl: "./dialog.component.html"
        }),
        __param(4, core_1.Inject(dialog_1.MAT_DIALOG_DATA))
    ], DialogComponent);
    return DialogComponent;
}());
exports.DialogComponent = DialogComponent;
