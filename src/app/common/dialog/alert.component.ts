import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: ".alert-template",
  templateUrl: "./alert.component.html",
})
export class AlertComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<AlertComponent>, @Inject(MAT_DIALOG_DATA) public data) {}

  title = "Alert";
  message: string;
  ngOnInit() {
    this.message = this.data.message;
    if (this.data.title !== undefined) {
      this.title = this.data.title;
    }
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
