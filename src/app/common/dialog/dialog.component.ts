import { Component, OnInit, Compiler, Injector, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { LazyloadService } from "../../services/lazyload.service";

@Component({
  selector: ".dialog-template",
  templateUrl: "./dialog.component.html",
})
export class DialogComponent implements OnInit {
  componentName: string;
  componentContext: any;
  moduleName: string;
  loadedComponent: any;

  loaded = false;

  constructor(private compiler: Compiler, private injector: Injector, private lazyloadService: LazyloadService, private dialogRef: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) public data) {}

  ngOnInit() {
    this.componentContext = this.data.componentContext;
    this.componentName = this.data.componentName;
    this.moduleName = this.data.moduleName;
    this.load();
  }

  load() {
    this.lazyloadService["import" + this.moduleName]()
      .then((lazyModule) => {
        this.loadedComponent = lazyModule.components[this.componentName];
        return lazyModule;
      })
      .then((lazyModule) => this.compiler.compileModuleAsync(lazyModule))
      .then((factory) => {
        let c = factory.create(this.injector);
        this.loaded = true;
      });
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
