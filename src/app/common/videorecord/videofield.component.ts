import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { FormControl } from "@angular/forms";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";

import { DialogService } from "../../services/dialog.service";
import { CommunicationService } from "../../services/communication.service";
import { AWSService } from "../../services/aws.service";
import { ApiService } from "../../services/api.service";
import { EnvService } from "../../services/env.service";
import { AppstateService } from "../../services/appstate.service";
import { TemplateutilsService } from "../../services/templateutils.service";

@Component({
  selector: ".video-record-field",
  templateUrl: "./videofield.component.html",
  styleUrls: ["../imagecropper/imagecropper.scss"],
})
export class VideoRecordFieldComponent implements OnInit, OnChanges {
  constructor(private dialog: DialogService, private communicationService: CommunicationService, private awsService: AWSService, private apiService: ApiService, public envService: EnvService, public sanitizer: DomSanitizer, private templateutilsService: TemplateutilsService, private appstateService: AppstateService) {}

  @Input() field: VideoUploadField;
  @Input() localform: any;

  randomid: string;

  refreshing = false;

  ngOnInit() {
    this.generateRandomId();
    this.initializeField();
    console.log("printing for debugging", this.field, this.localform);
  }

  ngOnChanges() {
    this.initializeField();
    console.log("printing for debugging onchange", this.field, this.localform);
  }

  initializeField() {
    if (this.field.data !== undefined) {
      this.field.media_id = this.field.data[this.field.media_id_key];
      this.field.media_name = this.field.data[this.field.media_name_key];
      this.field.media_url = this.field.data[this.field.media_url_key];
      this.field.media_frame_url = this.field.data[this.field.media_frame_key];
      this.field.sprout_embedded_url = this.field.data[this.field.sprout_embedded_url_key];
    }
    if (this.field.media_url !== undefined && this.field.media_url !== null) {
      if (this.field.sprout_embedded_url===null) {
        this.field.is_sprout_embed = false;
      } else {
        this.field.is_sprout_embed = true;
        if(this.field.sprout_embedded_url && this.field.sprout_embedded_url.includes("playlist")) {
          this.field.media_url = "https://videos.sproutvideo.com/playlist/" + this.field.media_url;  
          this.field.is_play_list = true;
        } else {
          this.field.media_url = "https://videos.sproutvideo.com/embed/" + this.field.media_url;
          this.field.is_play_list = false;
        }
        console.log("this.field.media_url", this.field.media_url);
        this.field.media_url_sanitized = this.sanitizer.bypassSecurityTrustResourceUrl(this.field.media_url);
        console.log("this.field.media_url_sanitized", this.field.media_url_sanitized);
      }
    }
    //console.log("this.field", this.field);
  }
  generateRandomId() {
    this.randomid = "randomid_" + Math.floor(Math.random() * 1000);
  }

  createFrameAfterUpload = false;
  video_upload() {
    this.initializeField();
    if(this.field.navigate_for_video_upload!==undefined && !this.appstateService.appState.fromadmin) {
      this.templateutilsService.navigateToRoute(this.field.navigate_for_video_upload);
      return;
    } 
    this.dialog.openVideoUploader(this.randomid);
    const parentSubscription = this.communicationService.detectNewCommunication().subscribe(() => {
      if (this.communicationService.communication.context.parentid != this.randomid) {
        return;
      }
      //console.log("this.field", this.field);
      //console.log("this.localform", this.localform);
      var recordedVideo = this.communicationService.communication.obj.video;
      var recordedVideoFrame = this.communicationService.communication.obj.frame;
      let fileName, frameName, isNewFile;
      const fileCategory = this.field.category;

      if (this.field.media_id !== undefined && this.field.media_id !== null && this.field.media_id !== 0) {
        isNewFile = false;
        this.awsService.deleteExistingFile(this.field.media_name, fileCategory);
        fileName = this.createFileName();
        frameName = this.createFrameName();
        if (recordedVideoFrame === undefined) {
          this.createFrameAfterUpload = true;
        } else {
          this.uploadFrame(frameName, fileCategory, isNewFile, recordedVideoFrame);
        }
        this.uploadFile(fileName, fileCategory, isNewFile, recordedVideo);
      } else {
        isNewFile = true;
        this.apiService.post_api("jobportal", "insertEmptyMedia", { media_type: "video" }).subscribe((mediaRes) => {
          this.field.media_id = mediaRes.inserted_entity_id;
          fileName = this.createFileName();
          frameName = this.createFrameName();
          this.localform.controls[this.field.media_id_key].setValue(this.field.media_id);
          if (recordedVideoFrame === undefined) {
            this.createFrameAfterUpload = true;
          } else {
            this.uploadFrame(frameName, fileCategory, isNewFile, recordedVideoFrame);
          }
          this.uploadFile(fileName, fileCategory, isNewFile, recordedVideo);
        });
      }
      parentSubscription.unsubscribe();
    });
  }
  frame_upload() {
    const parentSubscription = this.communicationService.detectNewCommunication().subscribe(() => {
      if (this.communicationService.communication.context.parentid != "framecaptured_" + this.randomid) {
        return;
      }
      console.log("frame captured", this.communicationService.communication);
      var recordedVideoFrame = this.communicationService.communication.obj.frame;
      var frameName = this.createFrameName();
      const fileCategory = this.field.category;
      this.uploadFrame(frameName, fileCategory, false, recordedVideoFrame);
      parentSubscription.unsubscribe();
    });
  }
  uploadFile(fileName, fileCategory, isNewFile, recordedVideo) {
    this.refreshing = true;
    this.awsService.prepareToUploadFileToS3(fileName, fileCategory, isNewFile).subscribe((res) => {
      const params = {
        url: res.url,
        fileContent: recordedVideo,
        headers: res.headers,
      };
      this.awsService.uploadFileToS3(params);
      const mediaFileURL = this.envService.values.ugcmediaurl + fileName;
      this.apiService
        .post_api("jobportal", "updateMedia", {
          id: this.field.media_id,
          media_file_internal_name: fileName,
          media_file_url: mediaFileURL,
          media_type: "video",
          sprout_video_status: 0,
          sprout_video_id: null,
          sprout_video_token: null,
          sprout_embedded_url: null,
          to_be_deleted: null
        })
        .subscribe((dummyRes) => {
          this.field.media_name = fileName;
          this.field.media_url = mediaFileURL;
          if (this.createFrameAfterUpload) {
            this.createFrameAfterUpload = false;
            this.communicationService.broadcastCommunication({
              context: { parentid: "frame_" + this.randomid },
              obj: { media_url: this.field.media_url },
            });
            this.refreshing = false;
            this.frame_upload();
          }
        });
    });
  }
  uploadFrame(fileName, fileCategory, isNewFile, recordedVideoFrame) {
    this.refreshing = true;
    this.awsService.prepareToUploadFileToS3(fileName, fileCategory, isNewFile).subscribe((res) => {
      const params = {
        url: res.url,
        fileContent: recordedVideoFrame,
        headers: res.headers,
      };
      this.awsService.uploadBase64ImageToS3(params);
      const mediaFileURL = this.envService.values.ugcmediaurl + fileName;
      this.field.media_frame_url = undefined;
      this.apiService
        .post_api("jobportal", "updateMedia", {
          id: this.field.media_id,
          media_frame_url: mediaFileURL,
        })
        .subscribe((dummyRes) => {
          this.field.media_frame_url = mediaFileURL;
          console.log("updated frame url", this.field.media_frame_url);
          this.postProcessAfterUpload();
        });
    });
  }

  postProcessAfterUpload() {
    const pollingInterval = setInterval(() => {
      if (!this.awsService.uploadInProgress) {
        clearInterval(pollingInterval);
        this.refreshing = false;
        this.generateRandomId();
      }
    }, 1000);
  }

  createFileName() {
    const fileName = "media_" + this.field.media_id + "_" + this.randomid + ".webm";
    return fileName;
  }
  createFrameName() {
    const fileName = "frame_media_" + this.field.media_id + "_" + this.randomid + ".png";
    return fileName;
  }

  video_display() {
    this.dialog.openVideoDisplay(this.field.media_url, this.field.media_frame_url);
  }
}

export interface VideoUploadField {
  editable: boolean;
  media_id_key: string | number;
  media_name_key: string;
  media_url_key: string;
  media_frame_key: string;
  sprout_embedded_url_key?: string;
  category: string;
  embedvideodirectly?: boolean;
  is_sprout_embed?: boolean;

  data?: any;
  media_id?: string | number;
  media_name?: string;
  media_url?: string;
  media_url_sanitized?: SafeResourceUrl;
  media_frame_url?: string;
  sprout_embedded_url?: string;
  is_play_list?: boolean;
  navigate_for_video_upload?: string;
  
}
