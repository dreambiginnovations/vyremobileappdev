"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var dialog_1 = require("@angular/material/dialog");
var video_js_1 = require("video.js");
var Record = require("videojs-record/dist/videojs.record.js");
var capture_video_frame_1 = require("capture-video-frame");
var VideoRecordDialogComponent = /** @class */ (function () {
    function VideoRecordDialogComponent(dialogRef, data, elementRef, communicationService, toastService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.communicationService = communicationService;
        this.toastService = toastService;
        this.idx = "video_" + Math.floor(Math.random() * 1000);
        this.randomUploaderID = "upload" + this.idx;
        this.recordingmaxcount = 5;
        this.RECORDING = {
            UNINITIALIZED: 0,
            INITIALIZING: 1,
            INPROGRESS: 2,
            PAUSED: 3,
            STOPPED: 4,
            PLAYING: 5,
            PLAYINGPAUSED: 6
        };
        this.facingMode = "user";
        this.selected = "record";
        this.showCanvas = false;
        this.captureRecordedFrame = false;
        this.initializeRecorderOptions();
    }
    VideoRecordDialogComponent.prototype.switchCamera = function () {
        var _this = this;
        if (this.facingMode == "user") {
            this.facingMode = "environment";
        }
        else if (this.facingMode == "environment") {
            this.facingMode = "user";
        }
        console.log("this.player", this.player, this.player.record());
        this.initializeRecorderOptions();
        this.player.record().stop();
        this.player.record().loadOptions(this.config.plugins.record);
        setTimeout(function () {
            _this.ngAfterViewInit();
        }, 300);
        /*clearInterval(this.readyinterval);
        this.stopRecording();
        this.recordingStatus = this.RECORDING.UNINITIALIZED;
        this.readymessage = this.recordingmaxcount + "";
        
        videojs(document.getElementById(this.idx)).dispose();
        
        this.initializeRecorderDevice();
        setTimeout(() => {
          this.ngAfterViewInit();
        }, 300);*/
        /*this.player.record().stopDevice();
        this.player.reset();
        this.initializeRecorderOptions();
        this.player.record().loadOptions(this.config);
        this.player.record().getDevice();
        setTimeout(() => {
          this.ngAfterViewInit();
        }, 300);*/
    };
    VideoRecordDialogComponent.prototype.initializeRecorderOptions = function () {
        this.player = false;
        this.plugin = Record;
        this.config = {
            controls: false,
            autoplay: false,
            fluid: false,
            loop: false,
            width: 320,
            height: 240,
            bigPlayButton: false,
            controlBar: {
                volumePanel: false,
                fullscreenToggle: false,
                pictureInPictureToggle: false,
                progressControl: false
            },
            plugins: {
                record: {
                    audio: true,
                    debug: true,
                    screen: true,
                    maxLength: 120,
                    video: {
                        width: 320,
                        height: 240,
                        facingMode: {
                            ideal: this.facingMode
                        }
                    },
                    frameWidth: 320,
                    frameHeight: 240
                }
            }
        };
    };
    VideoRecordDialogComponent.prototype.initializeRecorderDevice = function () {
        var _this = this;
        setTimeout(function () {
            console.log("Initializing device start...");
            _this.player.record().getDevice();
            console.log("Initializing device finish...");
        }, 300);
    };
    VideoRecordDialogComponent.prototype.ngOnInit = function () {
        // console.log("this.data on videorecord", this.data);
        this.recordingStatus = this.RECORDING.UNINITIALIZED;
        this.readymessage = this.recordingmaxcount + "";
        this.parentId = this.data.parentId;
    };
    VideoRecordDialogComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.player = video_js_1["default"](document.getElementById(this.idx), this.config, function () {
            console.log("player ready! id");
        });
        this.initializeRecorderDevice();
        this.player.on("deviceReady", function () {
            console.log("device is ready!");
        });
        this.player.on("startRecord", function () {
            console.log("started recording!");
        });
        this.player.on("finishRecord", function () {
            console.log("finished recording: ", _this.player.recordedData);
        });
        this.player.on("ended", function () {
            console.log("firing ended");
            _this.recordingStatus = _this.RECORDING.STOPPED;
        });
        this.player.on("error", function (error) {
            console.warn("error", error);
            _this.processRecordingError(error, "");
        });
        this.player.on("deviceError", function () {
            console.error("device error:", _this.player.deviceErrorCode);
            console.log(_this.player);
            _this.processRecordingError(undefined, "device");
        });
    };
    VideoRecordDialogComponent.prototype.processRecordingError = function (errorObj, errorType) {
        if (errorType == "device") {
            if (this.player.deviceErrorCode != undefined && this.player.deviceErrorCode.message.toLowerCase().includes("permission")) {
                this.recordingErrorMessage = "ERROR! Your permissions to access camera and microphone are turned off. Please turn on the permission. If having trouble, please reload/reinstall the app to get the permissions screen.";
            }
            else {
                this.recordingErrorMessage = this.player.deviceErrorCode;
            }
        }
        else {
            this.recordingErrorMessage = "ERROR! " + errorObj;
        }
    };
    VideoRecordDialogComponent.prototype.startRecording = function () {
        var _this = this;
        this.title = "Initializing recording";
        this.subtitle = "Hope you are ready...";
        this.readyintervalcounter = 1;
        this.recordingStatus = this.RECORDING.INITIALIZING;
        this.readyinterval = setInterval(function () {
            _this.readymessage = _this.recordingmaxcount - _this.readyintervalcounter + "";
            _this.readyintervalcounter++;
            if (_this.readyintervalcounter === _this.recordingmaxcount) {
                _this.player.record().start();
                _this.recordingStatus = _this.RECORDING.INPROGRESS;
                clearInterval(_this.readyinterval);
            }
        }, 1000);
    };
    VideoRecordDialogComponent.prototype.restartRecording = function () {
        var _this = this;
        clearInterval(this.readyinterval);
        this.stopRecording();
        this.recordingStatus = this.RECORDING.UNINITIALIZED;
        this.readymessage = this.recordingmaxcount + "";
        this.player.record().reset();
        this.initializeRecorderDevice();
        setTimeout(function () {
            _this.startRecording();
        }, 1000);
    };
    VideoRecordDialogComponent.prototype.pauseRecording = function () {
        this.player.record().pause();
        this.recordingStatus = this.RECORDING.PAUSED;
    };
    VideoRecordDialogComponent.prototype.unpauseRecording = function () {
        this.player.record().resume();
        this.recordingStatus = this.RECORDING.INPROGRESS;
    };
    VideoRecordDialogComponent.prototype.stopRecording = function () {
        this.player.record().stop();
        this.recordingStatus = this.RECORDING.STOPPED;
    };
    VideoRecordDialogComponent.prototype.playRecordedVideo = function () {
        this.player.play();
        this.recordingStatus = this.RECORDING.PLAYING;
    };
    VideoRecordDialogComponent.prototype.pauseRecordedVideo = function () {
        this.player.pause();
        this.recordingStatus = this.RECORDING.PLAYINGPAUSED;
    };
    VideoRecordDialogComponent.prototype.discardRecording = function () {
        clearInterval(this.readyinterval);
        this.stopRecording();
        this.recordingStatus = this.RECORDING.UNINITIALIZED;
        this.readymessage = this.recordingmaxcount + "";
        this.player.record().reset();
        this.initializeRecorderDevice();
    };
    VideoRecordDialogComponent.prototype.useRecording = function () {
        var _this = this;
        this.playRecordedVideo();
        this.showCanvas = true;
        setTimeout(function () {
            _this.captureVideoFrame();
            _this.communicationService.broadcastCommunication({
                context: { parentid: _this.parentId },
                obj: { video: _this.player.recordedData, frame: _this.capturedFrame }
            });
            _this.closeDialog();
        }, 200);
    };
    VideoRecordDialogComponent.prototype.captureVideoFrame = function (idString) {
        if (idString === undefined) {
            idString = this.idx;
        }
        /*var canvas = <HTMLCanvasElement>document.getElementById("canvas_" + idString);
        var video = <HTMLVideoElement>document.getElementById();
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        console.log("videoelement", video);
        canvas.getContext("2d").drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
        this.capturedFrame = canvas.toDataURL();*/
        var capturedFrameObj = capture_video_frame_1["default"](idString + "_html5_api", "png");
        this.capturedFrame = capturedFrameObj.dataUri;
    };
    VideoRecordDialogComponent.prototype.ngOnDestroy = function () {
        if (this.player) {
            this.player.dispose();
            this.player = false;
        }
    };
    VideoRecordDialogComponent.prototype.closeDialog = function () {
        this.dialogRef.close();
    };
    VideoRecordDialogComponent.prototype.chooseSelected = function (newSelected) {
        var _this = this;
        if (this.selected == newSelected) {
            return;
        }
        this.selected = newSelected;
        if (newSelected == "record") {
            setTimeout(function () {
                _this.ngAfterViewInit();
            }, 300);
        }
    };
    VideoRecordDialogComponent.prototype.video_upload = function () {
        document.getElementById(this.randomUploaderID).click();
    };
    VideoRecordDialogComponent.prototype.handleFileInput = function (files) {
        var _this = this;
        var itemBeingUploaded = files.item(0);
        if (itemBeingUploaded.type != "video/mp4" && itemBeingUploaded.type != "video/webm") {
            this.toastService.showError("Wrong Video Format", "You can only upload mp4 and webm formats. Please change the file and try again.");
            return;
        }
        this.communicationService.broadcastCommunication({
            context: { parentid: this.parentId },
            obj: { video: files.item(0) }
        });
        this.communicationSubscription = this.communicationService.detectNewCommunication().subscribe(function () {
            if (_this.communicationService.communication.obj.progress && _this.communicationService.communication.obj.progress == 100) {
                _this.isUploadingDone = true;
                //TODO temporairy stop this subscription and close dialog
                //this.captureRecordedVideoFrame();
                console.log("Closing dialog after upload done without capturing frame");
                _this.closeDialog();
            }
            if (_this.communicationService.communication.context.parentid != "frame_" + _this.parentId) {
                return;
            }
            console.log("received frame communication", _this.communicationService.communication);
            _this.uploadedFileURL = _this.communicationService.communication.obj.media_url;
            //this.captureRecordedVideoFrame();
        });
    };
    VideoRecordDialogComponent.prototype.captureRecordedVideoFrame = function () {
        var _this = this;
        if (this.uploadedFileURL !== undefined && this.isUploadingDone) {
            console.log("begin capturing uploaded video");
            this.frameidx = "frame_" + this.idx;
            this.captureRecordedFrame = true;
            this.communicationSubscription.unsubscribe();
            setTimeout(function () {
                var options = { autoplay: true, sources: [{ src: _this.uploadedFileURL, type: "video/webm" }] };
                _this.player = video_js_1["default"](document.getElementById(_this.frameidx), options, function onPlayerReady() {
                    console.log("Player ready", this.player.controls);
                });
                _this.player.on("error", function (e) {
                    console.log("videojs error", e);
                });
                setTimeout(function () {
                    _this.captureVideoFrame(_this.frameidx);
                    _this.communicationService.broadcastCommunication({
                        context: { parentid: "framecaptured_" + _this.parentId },
                        obj: { frame: _this.capturedFrame }
                    });
                    _this.closeDialog();
                }, 3000);
            }, 200);
        }
    };
    VideoRecordDialogComponent = __decorate([
        core_1.Component({
            selector: ".video-record-dialog",
            templateUrl: "./videorecord.component.html",
            styleUrls: ["../imagecropper/imagecropper.scss", "./videorecord.scss"]
        }),
        __param(1, core_1.Inject(dialog_1.MAT_DIALOG_DATA))
    ], VideoRecordDialogComponent);
    return VideoRecordDialogComponent;
}());
exports.VideoRecordDialogComponent = VideoRecordDialogComponent;
