import { Component, OnInit, OnDestroy, ElementRef, Inject } from "@angular/core";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ToastService } from "../../services/toast.service";

import videojs from "video.js";
import * as adapter from "webrtc-adapter/out/adapter_no_global.js";
import * as RecordRTC from "recordrtc";
import * as Record from "videojs-record/dist/videojs.record.js";
import captureVideoFrame from "capture-video-frame";

import { CommunicationService } from "../../services/communication.service";

@Component({
  selector: ".video-record-dialog",
  templateUrl: "./videorecord.component.html",
  styleUrls: ["../imagecropper/imagecropper.scss", "./videorecord.scss"],
})
export class VideoRecordDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<VideoRecordDialogComponent>, @Inject(MAT_DIALOG_DATA) public data, elementRef: ElementRef, private communicationService: CommunicationService, private toastService: ToastService) {
    this.initializeRecorderOptions();
  }

  private _elementRef: ElementRef;
  idx = "video_" + Math.floor(Math.random() * 1000);
  randomUploaderID = "upload" + this.idx;
  private config: any;
  private player: any;
  private plugin: any;
  parentId: string | number;

  title: string;
  subtitle: string;
  readymessage: string;
  readyinterval: any;
  readyintervalcounter: number;
  recordingStatus: number;
  recordingmaxcount = 5;
  RECORDING = {
    UNINITIALIZED: 0,
    INITIALIZING: 1,
    INPROGRESS: 2,
    PAUSED: 3,
    STOPPED: 4,
    PLAYING: 5,
    PLAYINGPAUSED: 6,
  };

  facingMode = "user";
  switchCamera() {
    if(this.facingMode == "user") {this.facingMode = "environment"}
    else if(this.facingMode == "environment") {this.facingMode = "user"}
    
    console.log("this.player", this.player, this.player.record());
    this.initializeRecorderOptions();
    this.player.record().stop();
    this.player.record().loadOptions(this.config.plugins.record);
    setTimeout(() => {
      this.ngAfterViewInit();
    }, 300);
    
    
    /*clearInterval(this.readyinterval);
    this.stopRecording();
    this.recordingStatus = this.RECORDING.UNINITIALIZED;
    this.readymessage = this.recordingmaxcount + "";
    
    videojs(document.getElementById(this.idx)).dispose();
    
    this.initializeRecorderDevice();
    setTimeout(() => {
      this.ngAfterViewInit();
    }, 300);*/
    
    
    /*this.player.record().stopDevice();
    this.player.reset();
    this.initializeRecorderOptions();
    this.player.record().loadOptions(this.config);
    this.player.record().getDevice();
    setTimeout(() => {
      this.ngAfterViewInit();
    }, 300);*/
  }

  selected = "record";
  initializeRecorderOptions() {
    this.player = false;
    this.plugin = Record;
    this.config = {
      controls: false ,
      autoplay: false,
      fluid: false,
      loop: false,
      width: 320,
      height: 240,
      bigPlayButton: false,
      controlBar: {
        volumePanel: false,
        fullscreenToggle: false,
        pictureInPictureToggle: false,
        progressControl: false,
      },
      plugins: {
        record: {
          audio: true,
          debug: true,
          screen: true,
          maxLength: 120,
          video: {
            width: 320,
            height: 240,
            facingMode: {
              ideal: this.facingMode
            },
          },
          frameWidth: 320,
          frameHeight: 240,
        },
      },
    };
  }
  initializeRecorderDevice() {
    setTimeout(() => {
      console.log("Initializing device start...");
      this.player.record().getDevice();
      console.log("Initializing device finish...");
    }, 300);
  }

  ngOnInit() {
    // console.log("this.data on videorecord", this.data);
    this.recordingStatus = this.RECORDING.UNINITIALIZED;
    this.readymessage = this.recordingmaxcount + "";
    this.parentId = this.data.parentId;
  }

  recordingErrorMessage: string;
  ngAfterViewInit() {
    this.player = videojs(document.getElementById(this.idx), this.config, () => {
      console.log("player ready! id");
    });
    this.initializeRecorderDevice();

    this.player.on("deviceReady", () => {
      console.log("device is ready!");
    });

    this.player.on("startRecord", () => {
      console.log("started recording!");
    });

    this.player.on("finishRecord", () => {
      console.log("finished recording: ", this.player.recordedData);
    });

    this.player.on("ended", () => {
      console.log("firing ended");
      this.recordingStatus = this.RECORDING.STOPPED;
    });

    this.player.on("error", (error) => {
      console.warn("error", error);
      this.processRecordingError(error, "");
    });

    this.player.on("deviceError", () => {
      console.error("device error:", this.player.deviceErrorCode);
      console.log(this.player);
      this.processRecordingError(undefined, "device");
    });
  }
  processRecordingError(errorObj, errorType) {
    if (errorType == "device") {
      if (this.player.deviceErrorCode != undefined && this.player.deviceErrorCode.message.toLowerCase().includes("permission")) {
        this.recordingErrorMessage = "ERROR! Your permissions to access camera and microphone are turned off. Please turn on the permission. If having trouble, please reload/reinstall the app to get the permissions screen.";
      } else {
        this.recordingErrorMessage = this.player.deviceErrorCode;
      }
    } else {
      this.recordingErrorMessage = "ERROR! " + errorObj;
    }
  }
  startRecording() {
    this.title = "Initializing recording";
    this.subtitle = "Hope you are ready...";
    this.readyintervalcounter = 1;
    this.recordingStatus = this.RECORDING.INITIALIZING;
    this.readyinterval = setInterval(() => {
      this.readymessage = this.recordingmaxcount - this.readyintervalcounter + "";
      this.readyintervalcounter++;
      if (this.readyintervalcounter === this.recordingmaxcount) {
        this.player.record().start();
        this.recordingStatus = this.RECORDING.INPROGRESS;
        clearInterval(this.readyinterval);
      }
    }, 1000);
  }
  restartRecording() {
    clearInterval(this.readyinterval);
    this.stopRecording();
    this.recordingStatus = this.RECORDING.UNINITIALIZED;
    this.readymessage = this.recordingmaxcount + "";
    this.player.record().reset();
    this.initializeRecorderDevice();
    setTimeout(() => {
      this.startRecording();
    }, 1000);
  }
  pauseRecording() {
    this.player.record().pause();
    this.recordingStatus = this.RECORDING.PAUSED;
  }
  unpauseRecording() {
    this.player.record().resume();
    this.recordingStatus = this.RECORDING.INPROGRESS;
  }
  stopRecording() {
    this.player.record().stop();
    this.recordingStatus = this.RECORDING.STOPPED;
  }

  playRecordedVideo() {
    this.player.play();
    this.recordingStatus = this.RECORDING.PLAYING;
  }
  pauseRecordedVideo() {
    this.player.pause();
    this.recordingStatus = this.RECORDING.PLAYINGPAUSED;
  }
  discardRecording() {
    clearInterval(this.readyinterval);
    this.stopRecording();
    this.recordingStatus = this.RECORDING.UNINITIALIZED;
    this.readymessage = this.recordingmaxcount + "";
    this.player.record().reset();
    this.initializeRecorderDevice();
  }

  showCanvas = false;
  capturedFrame: any;
  useRecording() {
    this.playRecordedVideo();
    this.showCanvas = true;
    setTimeout(() => {
      this.captureVideoFrame();
      this.communicationService.broadcastCommunication({
        context: { parentid: this.parentId },
        obj: { video: this.player.recordedData, frame: this.capturedFrame },
      });
      this.closeDialog();
    }, 200);
  }

  captureVideoFrame(idString?) {
    if (idString === undefined) {
      idString = this.idx;
    }
    /*var canvas = <HTMLCanvasElement>document.getElementById("canvas_" + idString);
    var video = <HTMLVideoElement>document.getElementById();
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    console.log("videoelement", video);
    canvas.getContext("2d").drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
    this.capturedFrame = canvas.toDataURL();*/
    var capturedFrameObj = captureVideoFrame(idString + "_html5_api", "png");
    this.capturedFrame = capturedFrameObj.dataUri;
  }

  ngOnDestroy() {
    if (this.player) {
      this.player.dispose();
      this.player = false;
    }
  }
  closeDialog() {
    this.dialogRef.close();
  }
  chooseSelected(newSelected: string): void {
    if (this.selected == newSelected) {
      return;
    }
    this.selected = newSelected;
    if (newSelected == "record") {
      setTimeout(() => {
        this.ngAfterViewInit();
      }, 300);
    }
  }
  video_upload() {
    document.getElementById(this.randomUploaderID).click();
  }

  uploadedFileURL: string;
  isUploadingDone: boolean;
  communicationSubscription: any;
  handleFileInput(files) {
    var itemBeingUploaded = files.item(0);
    if (itemBeingUploaded.type != "video/mp4" && itemBeingUploaded.type != "video/webm") {
      this.toastService.showError("Wrong Video Format", "You can only upload mp4 and webm formats. Please change the file and try again.");
      return;
    }
    this.communicationService.broadcastCommunication({
      context: { parentid: this.parentId },
      obj: { video: files.item(0) },
    });

    this.communicationSubscription = this.communicationService.detectNewCommunication().subscribe(() => {
      if (this.communicationService.communication.obj.progress && this.communicationService.communication.obj.progress == 100) {
        this.isUploadingDone = true;
        //TODO temporairy stop this subscription and close dialog
        //this.captureRecordedVideoFrame();
        console.log("Closing dialog after upload done without capturing frame");
        this.closeDialog();
      }
      if (this.communicationService.communication.context.parentid != "frame_" + this.parentId) {
        return;
      }
      console.log("received frame communication", this.communicationService.communication);
      this.uploadedFileURL = this.communicationService.communication.obj.media_url;
      //this.captureRecordedVideoFrame();
    });
  }
  captureRecordedFrame = false;
  frameidx: any;
  captureRecordedVideoFrame() {
    if (this.uploadedFileURL !== undefined && this.isUploadingDone) {
      console.log("begin capturing uploaded video");
      this.frameidx = "frame_" + this.idx;
      this.captureRecordedFrame = true;
      this.communicationSubscription.unsubscribe();

      setTimeout(() => {
        var options = { autoplay: true, sources: [{ src: this.uploadedFileURL, type: "video/webm" }] };
        this.player = videojs(document.getElementById(this.frameidx), options, function onPlayerReady() {
          console.log("Player ready", this.player.controls);
        });
        this.player.on("error", function (e) {
          console.log("videojs error", e);
        });

        setTimeout(() => {
          this.captureVideoFrame(this.frameidx);
          this.communicationService.broadcastCommunication({
            context: { parentid: "framecaptured_" + this.parentId },
            obj: { frame: this.capturedFrame },
          });
          this.closeDialog();
        }, 3000);
      }, 200);
    }
  }
}
