import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { VideoRecordDialogComponent } from "./videorecord.component";
import { VideoRecordFieldComponent } from "./videofield.component";

import { VjsPlayerModule } from "../videoplay/vjs-player.module";

@NgModule({
  declarations: [VideoRecordDialogComponent, VideoRecordFieldComponent],
  imports: [CommonModule, VjsPlayerModule],
  exports: [VideoRecordDialogComponent, VideoRecordFieldComponent],
})
export class VideoRecordModule {
  static components = {
    VideoRecordDialogComponent,
    VideoRecordFieldComponent,
  };
}
