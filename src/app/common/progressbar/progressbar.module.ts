import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MatProgressBarModule } from "@angular/material/progress-bar";

import { ProgressBarComponent } from "./progressbar.component";

@NgModule({
  declarations: [ProgressBarComponent],
  imports: [MatProgressBarModule, CommonModule],
  exports: [ProgressBarComponent],
})
export class ProgressBarModule {
  static components = {
    ProgressBarComponent,
  };
}
