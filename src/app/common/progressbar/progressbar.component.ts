import { Component, OnInit, Input } from "@angular/core";

import { CommunicationService } from "../../services/communication.service";

@Component({
  selector: ".progressbar",
  templateUrl: "./progressbar.template.html",
})
export class ProgressBarComponent implements OnInit {
  constructor(private communicationService: CommunicationService) {}

  progress: number;
  title: string;

  ngOnInit() {
    this.communicationService.detectNewCommunication().subscribe(() => {
      let communication = this.communicationService.communication;
      if (communication.context.parentid == "progressbarparent") {
        this.progress = communication.obj.progress;
        this.title = communication.obj.title;
      }
    });
  }
}
