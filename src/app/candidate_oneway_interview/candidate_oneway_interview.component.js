"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var CandidateOnewayInterviewComponent = /** @class */ (function () {
    function CandidateOnewayInterviewComponent(apiService, envService, appstateService, commonutilsService, communicationService, dialogService, templateutilsService, toastService, route, router, navigationService) {
        this.apiService = apiService;
        this.envService = envService;
        this.appstateService = appstateService;
        this.commonutilsService = commonutilsService;
        this.communicationService = communicationService;
        this.dialogService = dialogService;
        this.templateutilsService = templateutilsService;
        this.toastService = toastService;
        this.route = route;
        this.router = router;
        this.navigationService = navigationService;
        this.hostclasses = "nonstandardiconcontainer container px-0 viewfor__DETAILS viewhas__filters";
        this.step = "1.2";
    }
    CandidateOnewayInterviewComponent.prototype.gotostep = function (value) {
        this.step = value;
    };
    CandidateOnewayInterviewComponent.prototype.ngOnInit = function () {
        if (this.appstateService.appState.tempdata) {
            this.jobapplication = this.appstateService.appState.tempdata;
            this.load_data();
        }
        else {
            this.navigationService.back();
        }
    };
    CandidateOnewayInterviewComponent.prototype.load_data = function () {
        var _this = this;
        this.apiService.post_api("jobportal", "getjobtoapply", { id: this.jobapplication.id }).subscribe(function (res) {
            if (res.status === "FALSE" || res.status === false) {
                _this.toastService.showError("Error", res.message);
                _this.commonutilsService.processResponse(res);
                return;
            }
            _this.response_data = res;
            _this.response_data["company_photo_id_media_file_internal_name"] = _this.response_data["company_photo_id_media_file_internal_name"];
        });
    };
    __decorate([
        core_1.HostBinding("class")
    ], CandidateOnewayInterviewComponent.prototype, "hostclasses");
    CandidateOnewayInterviewComponent = __decorate([
        core_1.Component({
            selector: ".component__candidate_oneway_interview",
            templateUrl: "./candidate_oneway_interview.template.html",
            styleUrls: ["./candidate_oneway_interview.component.scss"]
        })
    ], CandidateOnewayInterviewComponent);
    return CandidateOnewayInterviewComponent;
}());
exports.CandidateOnewayInterviewComponent = CandidateOnewayInterviewComponent;
