import { Component, OnInit, HostBinding, HostListener } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { ApiService } from "../services/api.service";
import { AppstateService } from "../services/appstate.service";
import { CommonutilsService } from "../services/commonutils.service";
import { CameraService } from '../services/camera.service';
import { CommunicationService } from "../services/communication.service";
import { NavigationService } from "../services/navigation.service";
import { DialogService } from "../services/dialog.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";
import { EnvService } from "../services/env.service";

import videojs from "video.js";
import "videojs-playlist";
import * as adapter from "webrtc-adapter/out/adapter_no_global.js";
import * as RecordRTC from "recordrtc";
import * as Record from "videojs-record/dist/videojs.record.js";
import captureVideoFrame from "capture-video-frame";
import 'videojs-watermark';

@Component({
  selector: ".component__candidate_oneway_interview",
  templateUrl: "./candidate_oneway_interview.template.html",
  styleUrls: ["./candidate_oneway_interview.component.scss"],
})
export class CandidateOnewayInterviewComponent implements OnInit {
  @HostBinding("class") hostclasses = "nonstandardiconcontainer container px-0 viewfor__DETAILS viewhas__filters";
  constructor(
    private apiService: ApiService,
    public envService: EnvService,
    public appstateService: AppstateService,
    private commonutilsService: CommonutilsService,
    private communicationService: CommunicationService,
    public dialogService: DialogService,
    public templateutilsService: TemplateutilsService,
    private toastService: ToastService,
    private route: ActivatedRoute,
    private cameraService: CameraService,
    private router: Router, private navigationService: NavigationService
  ) {}
  
  checkIfReload() {
    if(this.appstateService.tempdata) {
      if(this.response_data===undefined) {
        this.jobapplication = this.appstateService.tempdata;
        this.load_data();
      }
      return false;
    }
    return true;
  }
  step = "1.2";
  jobapplication: any;
  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.steptype = undefined;
      this.showInitializeRecordingScreen = false;
      this.recordingStatus = this.RECORDING.UNINITIALIZED;
      if(params.step) {this.step = params.step;}
      var checkIfReload = this.checkIfReload();
      if(checkIfReload && this.step!="1.2") {
        this.navigationService.back();
        return;
      }
      if(this.step=="1.4") {
        this.communicationService.broadcastCommunication({
          context: { parentid: "job_application_video_upload" },
          obj: {},
        });
      } else if(this.step.includes("recordingselection")) {
        this.steptype = "recordingselection";
        this.processChooseCamerasScreen();
      } else if(this.step.includes("recordingdescription_")) {
        this.steptype = "recordingdescription";
        this.stepnumber = parseInt(this.step.replace("recordingdescription_", ""));
      } else if(this.step.includes("recording_")) {
        this.steptype = "recording";
        this.stepnumber = parseInt(this.step.replace("recording_", ""));
        this.recordingQuestion = this.response_data.screening_questions[this.stepnumber].question;
        this.idx = this.step;
        this.scrollToTop();
        setTimeout(()=>{
          this.initializeRecorderOptions();
        }, 500);
        setTimeout(()=>{
          this.initializeVideoJS();
        }, 1500);
        this.initializeRecordingTimerNum = 5;
        this.processInitializeRecordingScreen();
      }
    });
  }
  
  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getjobtoapply", { id: this.jobapplication.id }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;
      this.response_data["company_photo_id_media_file_internal_name"] = this.envService.values["ugcmediaurl"] +  this.response_data["company_photo_id_media_file_internal_name"];
    });
  }
  
  steptype: string;
  stepnumber: number;
  scrollToTop(): void {
    window.scroll(0, 0);
  }
  initializeRecordingTimerInterval: any;
  audioplay: any;
  processInitializeRecordingScreen() {
    this.showInitializeRecordingScreen = true;
    this.recordingStatus = this.RECORDING.UNINITIALIZED;
    this.initializeRecordingTimerInterval = setInterval(()=>{
      if(this.initializeRecordingTimerNum>1) {--this.initializeRecordingTimerNum;}
      else {
        clearInterval(this.initializeRecordingTimerInterval);
        this.showInitializeRecordingScreen = false;
        this.startRecording();
      }
    }, 1000);
  }
  gotostep(value) {
    this.router.navigate(['candidate/oneway_interview', value]);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    console.log('Back button pressed');
    if(this.player!==undefined) {videojs(this.player).dispose();}
    if(this.recordingStatus === this.RECORDING.UNINITIALIZED) {videojs(this.player).dispose();}
  }
  cameraList: any;
  chosenDeviceId: any = '';
  async processChooseCamerasScreen() {
    this.cameraList = null;
    if ('mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices) {
      this.cameraService.getCameraList().then((value)=>{
        this.cameraList = value;
        console.log("this.cameraList inside then", this.cameraList);
        if(this.cameraList.length===0) {
          this.toastService.showError("No Cameras Found", "There are no cameras found on your device.");      
        } else if(this.cameraList.length===1) {
          this.chosenDeviceId = this.cameraList[0].deviceId;
          this.gotostep('recordingdescription_0');
        }
      });
    } else {
      this.toastService.showError("No Cameras Found", "There are no cameras found on your device.");      
    }
  }
  
  
  recordingQuestion: string;
  showInitializeRecordingScreen = false;
  initializeRecordingTimerNum: number;
  idx: string;
  gotorecordingstep(value) {
    this.step = "recording_" + value;
    this.showCanvas = false;
    this.router.navigate(['/candidate/oneway_interview', this.step]);
    this.recordingQuestion = this.response_data.screening_questions[value].question;
  }
  
  recordingStatus: number;
  initializeVideoJS() {
    this.player = videojs(document.getElementById(this.idx), this.config, () => {
      console.log("player ready! id", this.idx);
    });
    console.log("this.player", this.player);
    setTimeout(() => {
      console.log("Initializing device start...");
      this.player.record().getDevice();
      console.log("Initializing device finish...");
    }, 300);

    this.player.on("deviceReady", () => {
      console.log("device is ready!");
    });

    this.player.on("startRecord", () => {
      console.log("started recording!");
    });

    this.player.on("finishRecord", () => {
      console.log("finished recording: ", this.player.recordedData);
      this.recordingStatus = this.RECORDING.COMPLETED;
    });

    this.player.on("ended", () => {
      console.log("firing ended");
      this.recordingStatus = this.RECORDING.STOPPED;
    });

    this.player.on("error", (error) => {
      console.warn("error", error);
      this.processRecordingError(error, "");
    });

    this.player.on("deviceError", () => {
      console.error("device error:", this.player.deviceErrorCode, this.player);
      this.processRecordingError(undefined, "device");
    });
  }
  
  recordingInterval: any;
  recordingTimeInSeconds: number;
  numOfMinutesPerQuestion = 2;
  maxRecordingTimeInSeconds = this.numOfMinutesPerQuestion*60;
  startRecording() {
    if(this.recordingInterval) {clearInterval(this.recordingInterval);}
    this.recordingTimeInSeconds = this.maxRecordingTimeInSeconds;
    this.recordingInterval = setInterval(() => {
      this.recordingTimeInSeconds--;
      if(this.recordingTimeInSeconds<=0) {
        clearInterval(this.recordingInterval);
      }
    }, 1000);
    this.player.record().start();
    this.recordingStatus = this.RECORDING.INPROGRESS;
  }
  useRecording() {
    const videoPlayer:any = document.getElementById(this.idx).getElementsByTagName('video')[0];
    const stream = videoPlayer.captureStream();
    this.cameraService.stopStream(stream);
    
    this.showCanvas = true;
    clearInterval(this.recordingInterval);
    setTimeout(() => {
      this.captureVideoFrame();
      if(this.appstateService.guided_recording===undefined) {
        this.appstateService.guided_recording = {};
      }
      this.appstateService.guided_recording[this.step] = { video: this.player.recordedData, frame: this.capturedFrame, seconds: (this.maxRecordingTimeInSeconds-this.recordingTimeInSeconds), question: this.recordingQuestion, question_id: this.response_data.screening_questions[this.stepnumber].id };
      if(this.stepnumber < (this.response_data.screening_questions.length-1)) {
        this.gotostep('recordingdescription_' + (this.stepnumber + 1));
      } else {this.gotostep('1.4');}
      this.recordingStatus = this.RECORDING.UNINITIALIZED;
    }, 200);
  }
  capturedFrame: any;
  showCanvas = false;
  captureVideoFrame(idString?) {
    if (idString === undefined) {
      idString = this.idx;
    }
    var capturedFrameObj = captureVideoFrame(idString + "_html5_api", "png");
    this.capturedFrame = capturedFrameObj.dataUri;
  }
  stopRecording() {
    this.player.record().stop();
    this.recordingStatus = this.RECORDING.COMPLETED;
    clearInterval(this.recordingInterval);
    setTimeout(()=>{
      this.playRecordedVideo();
      setTimeout(()=>{
        this.pauseRecordedVideo();
      }, 500);
    }, 500);
  }
  
  parentId: string | number;
  finalRecordedData = {};
  recordingErrorMessage: string;
  private config: any;
  private player: any;
  private plugin: any;
  
  switchCamera() {}
  
  restartRecording() {
    clearInterval(this.recordingInterval);
    this.stopRecording();
    this.recordingStatus = this.RECORDING.UNINITIALIZED;
    this.player.record().reset();
    setTimeout(() => {
      this.startRecording();
    }, 1000);
  }
  pauseRecording() {
    this.player.record().pause();
    this.recordingStatus = this.RECORDING.PAUSED;
  }
  unpauseRecording() {
    this.player.record().resume();
    this.recordingStatus = this.RECORDING.INPROGRESS;
  }
  

  playRecordedVideo() {
    this.player.play();
    this.recordingStatus = this.RECORDING.PLAYING;
  }
  pauseRecordedVideo() {
    this.player.pause();
    this.recordingStatus = this.RECORDING.PLAYINGPAUSED;
  }
  
  
  
  
  
  RECORDING = {
    UNINITIALIZED: 0,
    INITIALIZING: 1,
    INPROGRESS: 2,
    PAUSED: 3,
    STOPPED: 4,
    PLAYING: 5,
    PLAYINGPAUSED: 6,
    COMPLETED: 7
  };
  initializeRecorderOptions() {
    var video_parent = document.getElementById("parent_" + this.idx);
    var video = document.createElement('video');
    video.id = this.idx;
    video.className="video-js vjs-default-skin shadow";
    video.preload="auto";
    video.setAttribute("playsinline","true");
    video_parent.appendChild(video);
    
    this.plugin = Record;
    this.config = {
      controls: false,
      autoplay: false,
      fluid: false,
      loop: false,
      width: window.innerWidth,
      height: window.innerHeight*0.75,
      bigPlayButton: false,
      controlBar: {
        volumePanel: false,
        fullscreenToggle: false,
        pictureInPictureToggle: false,
        progressControl: false,
      },
      plugins: {
        record: {
          audio: true,
          screen: true,
          videoMimeType: 'video/webm;codecs=vp8',
          maxLength: this.recordingTimeInSeconds,
          video: {
            width: 320,
            height: 240,
            deviceId: this.chosenDeviceId
          }
        }
      }
    };
  }
  processRecordingError(errorObj, errorType) {
    if (errorType == "device") {
      if (this.player.deviceErrorCode != undefined && this.player.deviceErrorCode.message.toLowerCase().includes("permission")) {
        this.recordingErrorMessage = "ERROR! Your permissions to access camera and microphone are turned off. Please turn on the permission. If having trouble, please reload/reinstall the app to get the permissions screen.";
      } else {
        this.recordingErrorMessage = this.player.deviceErrorCode;
      }
    } else {
      this.recordingErrorMessage = "ERROR! " + errorObj;
    }
  }
  previewPlayer: any;
  createPreviewVideo() {
    var currentRunningVideo:any = document.getElementById("previewvideo");
    if (this.previewPlayer && currentRunningVideo) {
      const videoPlayer:any = document.getElementById('previewvideo').getElementsByTagName('video')[0];
      const stream2 = videoPlayer.captureStream();
      this.cameraService.stopStream(stream2);  
      this.previewPlayer.dispose();
    }
    if(this.chosenDeviceId==='') {return;}
    var finalParent = document.getElementById("previewvideocontainer");
    var video = document.createElement('video');
    video.id = "previewvideo";
    video.className = "video-js vjs-default-skin shadow";
    video.preload = "auto";
    video.setAttribute("playsinline", "true");
    finalParent.appendChild(video);
    setTimeout(()=>{
      const options = {
        width: 320,
        height: 240,
        controls: false,
        autoplay: true,
        fluid: true
      };
      this.previewPlayer = videojs(document.getElementById(video.id), options);
      navigator.mediaDevices.getUserMedia({
        video: {
          deviceId: { exact: this.chosenDeviceId }
        }
      })
      .then(stream => {
        console.log('Camera stream obtained:', stream);
        video.srcObject = stream;
        video.onloadedmetadata = (e) => {
            video.play();
        };
      })
      .catch(error => {
        console.error('Error accessing camera:', error);
      });
    }, 500);
  }


  processFinalRecordingScreen() {
    var config = {
      controls: true,
      autoplay: false,
      fluid: false,
      loop: false,
      width: 320,
      height: 240,
      bigPlayButton: true,
      controlBar: {
        volumePanel: false,
        fullscreenToggle: false,
        pictureInPictureToggle: false,
        progressControl: false,
      }
    };
    var finalParentID = "finalrecording_parent_" + this.idx;
    var finalParent = document.getElementById(finalParentID);
    for(var count=0;count<this.response_data.screening_questions.length;count++) {
      var finalParentIDCount = "finalrecording_container_" + count + "_" + this.idx;
      var finalParentIDCountDiv = document.createElement('div');
      finalParentIDCountDiv.id = finalParentIDCount;
      finalParent.appendChild(finalParentIDCountDiv);
      var video_parent = document.getElementById(finalParentID);
      var titleElement = document.createElement('div');
      titleElement.className="videotitle";
      titleElement.innerText = this.appstateService.guided_recording["recording_" + count]["question"];
      finalParentIDCountDiv.appendChild(titleElement);
      var video = document.createElement('video');
      video.id = "finalrecording_" + count + "_" + this.idx;
      video.className="video-js vjs-default-skin shadow";
      video.preload="auto";
      video.setAttribute("playsinline", "true");
      finalParentIDCountDiv.appendChild(video);
      this.initializeFinalPlayers(video.id, config, count);
    }
  }
  initializeFinalPlayers(videoID, config, count) {
    setTimeout(()=>{
      var finalplayer = videojs(document.getElementById(videoID), config, () => {
        console.log("final video player ready! id", videoID);
      });
      setTimeout(()=>{
        finalplayer.src({ type: this.appstateService.guided_recording["recording_" + count]["video"].type, src: URL.createObjectURL(this.appstateService.guided_recording["recording_" + count]["video"])});
        finalplayer.load();
      }, 500);
    }, 500);
  }
  
}
