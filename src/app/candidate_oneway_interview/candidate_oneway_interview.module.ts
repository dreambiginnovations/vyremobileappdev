import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateOnewayInterviewComponent } from "./candidate_oneway_interview.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateOnewayInterviewComponent,
  },
  {
    path: ":step",
    component: CandidateOnewayInterviewComponent,
  }
];

@NgModule({
  declarations: [CandidateOnewayInterviewComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, FormsModule, ImgCropperModule, VideoRecordModule],
})
export class CandidateOnewayInterviewModule {
  static components = {
    CandidateOnewayInterviewComponent: CandidateOnewayInterviewComponent,
  };
}
