import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { VideoUploadComponent } from "./video_upload.component";

export const routes: Routes = [
  {
    path: "",
    component: VideoUploadComponent,
  },
];

@NgModule({
  declarations: [VideoUploadComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class VideoUploadModule {
  static components = {
    VideoUploadComponent: VideoUploadComponent,
  };
}
