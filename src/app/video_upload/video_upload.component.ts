import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { DialogService } from "../services/dialog.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__video_upload",
  templateUrl: "./video_upload.template.html",
  styleUrls: ["./video_upload.component.scss"],
})
export class VideoUploadComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, public dialogService: DialogService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
  }

  localform: FormGroup;
  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("browse", new FormControl(""));
    this.localform.controls["browse"].setValue("Browse Video");
    this.localform.addControl("record", new FormControl(""));
    this.localform.controls["record"].setValue("Record Video");
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }
}
