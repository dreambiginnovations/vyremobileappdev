import { Component } from "@angular/core";
import { ActivatedRoute, Router, NavigationStart, NavigationEnd } from "@angular/router";

import { NavigationService } from "./services/navigation.service";
import { ApiService } from "./services/api.service";
import { CommunicationService } from "./services/communication.service";
import { AWSService } from "./services/aws.service";
import { AppstateService } from "./services/appstate.service";
import { JsonLDService } from "./services/jsonld.service";
import { ToastService } from "./services/toast.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "Swaayam";

  loading = false;
  constructor(private router: Router, private navigationService: NavigationService, public apiService: ApiService, public appstateService: AppstateService, private jsonLDService: JsonLDService, private activatedRoute: ActivatedRoute, private communicationService: CommunicationService,private toastService: ToastService, private awsService: AWSService) {
    this.apiService.isLoading.subscribe((v) => {
      this.loading = v;
    });
  }
  firstNavigationDone = false;
  backHiddenStates = ["/login_signup"];

  onActivate(event) {
    window.scroll(0, 0);
  }

  ngOnInit() {
    //this.appstateService.central_upload_obj.uploadInProgress = true;
    //this.appstateService.central_upload_obj.uploadingProgress = 100;
    //this.appstateService.central_upload_obj.uploadingTitle = "Testing title here";
    
    this.communicationService.detectNewCommunication().subscribe(() => {
      var communicationParentID = this.communicationService.communication.context.parentid;
      var communication_communicationid = this.communicationService.communication.context.communicationid;
      var communication_obj = this.communicationService.communication.obj;
      if (communicationParentID === undefined) {
        return;
      } else if(communicationParentID=="candidate_profile_guided_video_upload") {
        this.handleCandidateGuideProfileVideoUpload(communication_communicationid, communication_obj);
      } else if(communicationParentID=="job_application_video_upload") {
        this.handleCandidateOnewayInterviewVideoUpload();
      }
    });
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationStart) {
        console.log("Removing Structured Data");
        this.jsonLDService.removeStructuredData();
      }
      if (val instanceof NavigationEnd) {
        if (val.urlAfterRedirects.includes("/job/")) {
          var jobId = val.urlAfterRedirects.split("/job/")[1];
          this.apiService.post_api("jobportal", "getjob", { id: jobId }).subscribe((res) => {
            console.log("Adding Structured Data for Job", res);
            this.jsonLDService.insertSchema(this.jsonLDService.createJobSchemaFromJob(res));
          });
        }
        if (this.firstNavigationDone) {
          this.navigationService.enableBack = true;
        }
        if (this.backHiddenStates.includes(val.url)) {
          this.navigationService.enableBack = false;
        }
        this.firstNavigationDone = true;
        var currentURLQueryParams = this.activatedRoute.snapshot.queryParams;
        for (var key in this.appstateService.importantqueryparamsmaster) {
          var value = this.appstateService.importantqueryparamsmaster[key];
          if (currentURLQueryParams[value] !== undefined) {
            this.appstateService.importantqueryparams[value] = currentURLQueryParams[value];
          }
        }
        console.log("this.appstateService.importantqueryparams", this.appstateService.importantqueryparams);
        this.apiService.post_api("jobportal", "getunreadnotificationsinfo").subscribe((res) => {
          if (res.count > 0) {
            this.appstateService.hasunreadnotifications = true;
          } else {
            this.appstateService.hasunreadnotifications = false;
          }
        });
        if (currentURLQueryParams.SUPER__loadpagewithmessage) {
          this.toastService.showSuccess("Alert!", currentURLQueryParams.SUPER__loadpagewithmessage);
        }
      }
    });
  }
  
  handleCandidateGuideProfileVideoUpload(questionNumber: number, communication_obj?: any) {
    this.awsService.handleCandidateGuideProfileVideoUpload(questionNumber, communication_obj);
  }
  handleCandidateOnewayInterviewVideoUpload() {
    this.awsService.handleCandidateOnewayInterviewVideoUpload();
  }
}
