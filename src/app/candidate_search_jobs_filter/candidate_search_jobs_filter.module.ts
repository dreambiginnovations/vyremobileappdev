import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { CustomDropDownModule } from "../common/dropdown/dropdown.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateSearchJobsFilterComponent } from "./candidate_search_jobs_filter.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateSearchJobsFilterComponent,
  },
];

@NgModule({
  declarations: [CandidateSearchJobsFilterComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, CustomDropDownModule],
})
export class CandidateSearchJobsFilterModule {
  static components = {
    CandidateSearchJobsFilterComponent: CandidateSearchJobsFilterComponent,
  };
}
