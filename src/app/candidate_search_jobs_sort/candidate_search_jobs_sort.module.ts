import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { CustomDropDownModule } from "../common/dropdown/dropdown.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateSearchJobsSortComponent } from "./candidate_search_jobs_sort.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateSearchJobsSortComponent,
  },
];

@NgModule({
  declarations: [CandidateSearchJobsSortComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, CustomDropDownModule],
})
export class CandidateSearchJobsSortModule {
  static components = {
    CandidateSearchJobsSortComponent: CandidateSearchJobsSortComponent,
  };
}
