"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var CandidateSearchJobsSortComponent = /** @class */ (function () {
    function CandidateSearchJobsSortComponent(apiService, commonutilsService, communicationService, dialogService, templateutilsService, toastService) {
        this.apiService = apiService;
        this.commonutilsService = commonutilsService;
        this.communicationService = communicationService;
        this.dialogService = dialogService;
        this.templateutilsService = templateutilsService;
        this.toastService = toastService;
        this.hostclasses = "px-0 container viewfor__EDIT";
    }
    CandidateSearchJobsSortComponent.prototype.ngOnInit = function () {
        this.parentid = this.communicationService.communication.context.parentid;
        this.sort_data = this.communicationService.communication.obj.sort_data;
        this.initializeFormGroup();
    };
    CandidateSearchJobsSortComponent.prototype.skipsubmitValidator = function (control) {
        return { skip: true };
    };
    CandidateSearchJobsSortComponent.prototype.processcontrolforskip = function (control) {
        var newControl;
        if (control instanceof forms_1.FormArray) {
            newControl = this.processformgroupforskip(control, true);
        }
        else if (control instanceof forms_1.FormGroup) {
            newControl = this.processformgroupforskip(control, false);
        }
        else {
            newControl = control;
        }
        return newControl;
    };
    CandidateSearchJobsSortComponent.prototype.processformgroupforskip = function (formgroup, controlsArray) {
        var _a, _b;
        var returnFormGroup;
        if (controlsArray) {
            returnFormGroup = new forms_1.FormArray([]);
            for (var count = 0; count < formgroup.controls.length; count++) {
                var control = formgroup.controls[count];
                if (((_a = control.errors) === null || _a === void 0 ? void 0 : _a.skip) !== true) {
                    var newControl = this.processcontrolforskip(control);
                    returnFormGroup.push(newControl);
                }
            }
        }
        else {
            returnFormGroup = new forms_1.FormGroup({});
            for (var controlName in formgroup.controls) {
                var control = formgroup.controls[controlName];
                if (((_b = control.errors) === null || _b === void 0 ? void 0 : _b.skip) !== true) {
                    var newControl = this.processcontrolforskip(control);
                    returnFormGroup.addControl(controlName, newControl);
                }
            }
        }
        return returnFormGroup;
    };
    CandidateSearchJobsSortComponent.prototype.initializeFormGroup = function () {
        this.localform = new forms_1.FormGroup({});
        this.localform.addControl("sort_by", new forms_1.FormControl("", forms_1.Validators.required));
        this.localform.addControl("sort_order", new forms_1.FormControl("", forms_1.Validators.required));
        this.localform.addControl("sort_button", new forms_1.FormControl("", this.skipsubmitValidator));
        this.initializeSortData();
    };
    CandidateSearchJobsSortComponent.prototype.initializeSortData = function () {
        this.localform.controls["sort_by"].setValue(this.sort_data["sort_by"]);
        this.localform.controls["sort_order"].setValue(this.sort_data["sort_order"]);
        this.localform.controls["sort_button"].setValue(this.sort_data["sort_button"]);
    };
    CandidateSearchJobsSortComponent.prototype.submitSortData = function () {
        this.communicationService.broadcastCommunication({ context: { parentid: this.parentid }, obj: { sort_data: this.localform.value } });
        this.dialogService.dismissOpenDialogs();
    };
    __decorate([
        core_1.HostBinding("class")
    ], CandidateSearchJobsSortComponent.prototype, "hostclasses");
    CandidateSearchJobsSortComponent = __decorate([
        core_1.Component({
            selector: ".component__candidate_search_jobs_sort",
            templateUrl: "./candidate_search_jobs_sort.template.html",
            styleUrls: ["./candidate_search_jobs_sort.component.scss"]
        })
    ], CandidateSearchJobsSortComponent);
    return CandidateSearchJobsSortComponent;
}());
exports.CandidateSearchJobsSortComponent = CandidateSearchJobsSortComponent;
