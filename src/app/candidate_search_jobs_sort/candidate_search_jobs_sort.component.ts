import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { CommunicationService } from "../services/communication.service";
import { DialogService } from "../services/dialog.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__candidate_search_jobs_sort",
  templateUrl: "./candidate_search_jobs_sort.template.html",
  styleUrls: ["./candidate_search_jobs_sort.component.scss"],
})
export class CandidateSearchJobsSortComponent implements OnInit {
  @HostBinding("class") hostclasses = "px-0 container viewfor__EDIT";
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, private communicationService: CommunicationService, private dialogService: DialogService, private templateutilsService: TemplateutilsService, private toastService: ToastService) {}

  parentid: any;
  sort_data: any;
  ngOnInit() {
    this.parentid = this.communicationService.communication.context.parentid;
    this.sort_data = this.communicationService.communication.obj.sort_data;

    this.initializeFormGroup();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("sort_by", new FormControl("", Validators.required));
    this.localform.addControl("sort_order", new FormControl("", Validators.required));
    this.localform.addControl("sort_button", new FormControl("", this.skipsubmitValidator));

    this.initializeSortData();
  }

  initializeSortData() {
    this.localform.controls["sort_by"].setValue(this.sort_data["sort_by"]);
    this.localform.controls["sort_order"].setValue(this.sort_data["sort_order"]);
    this.localform.controls["sort_button"].setValue(this.sort_data["sort_button"]);
  }

  submitSortData() {
    this.communicationService.broadcastCommunication({ context: { parentid: this.parentid }, obj: { sort_data: this.localform.value } });
    this.dialogService.dismissOpenDialogs();
  }
}
