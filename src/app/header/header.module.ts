import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { HeaderComponent } from "./header.component";

export const routes: Routes = [
  {
    path: "",
    component: HeaderComponent,
  },
];

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class HeaderModule {
  static components = {
    HeaderComponent: HeaderComponent,
  };
}
