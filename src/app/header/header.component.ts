import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { NavigationService } from "../services/navigation.service";
import { EnvService } from "../services/env.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { DialogService } from "../services/dialog.service";
import { AppstateService } from "../services/appstate.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__header",
  templateUrl: "./header.template.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  @HostBinding("class") hostclasses = "px-2 py-3 bg-white container px-0 viewfor__DETAILS";
  constructor(
    private apiService: ApiService,
    private commonutilsService: CommonutilsService,
    public navigationService: NavigationService,
    public envService: EnvService,
    public templateutilsService: TemplateutilsService,
    public dialogService: DialogService,
    public appstateService: AppstateService,
    private toastService: ToastService
  ) {}

  ngOnInit() {
    this.initializeFormGroup();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }
}
