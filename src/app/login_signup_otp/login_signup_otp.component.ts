import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ApiService } from "../services/api.service";
import { EnvService } from "../services/env.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { AppstateService } from "../services/appstate.service";
import { CommonutilsService } from "../services/commonutils.service";
import { DialogService } from "../services/dialog.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__login_signup_otp",
  templateUrl: "./login_signup_otp.template.html",
  styleUrls: ["./login_signup_otp.component.scss"],
})
export class LoginSignupOtpComponent implements OnInit {
  @HostBinding("class") hostclasses = "viewascardondesktop bg-white px-0 twocolumnleftimage container viewfor__EDIT";
  mobile = localStorage.getItem("mobile");
  constructor(private apiService: ApiService, public envService: EnvService, public templateutilsService: TemplateutilsService, public appstateService: AppstateService, private commonutilsService: CommonutilsService, private dialogService: DialogService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();

    if (this.appstateService.appState["username"]) {
      this.localform.controls["name"].setValue(this.appstateService.appState["username"]);
    }

    if (this.appstateService.appState["current_location"]) {
      this.localform.removeControl("current_location");
      this.localform.removeControl("current_location_city");
      this.localform.removeControl("current_location_state");
      this.localform.removeControl("current_location_country");
      this.localform.removeControl("current_latitude_location");
      this.localform.removeControl("current_longitude_location");
    }
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("mobile", new FormControl("", Validators.compose([Validators.minLength(10), Validators.maxLength(10)])));
    this.localform.controls["mobile"].setValue(this.mobile);
    this.localform.addControl("name", new FormControl("", Validators.required));
    this.localform.addControl("otp", new FormControl("", Validators.required));
    this.localform.addControl("acceptterms", new FormControl(""));
    this.localform.addControl("accepttermstext", new FormControl("", this.skipsubmitValidator));
    this.localform.controls["accepttermstext"].setValue("Terms and Conditions");
    this.localform.addControl("accepttermslink", new FormControl("", this.skipsubmitValidator));
    this.localform.controls["accepttermslink"].setValue("https://swaayam.co/terms/");
    this.localform.addControl("accepttermstext", new FormControl("", this.skipsubmitValidator));
    this.localform.controls["accepttermstext"].setValue("Privacy Policy");
    this.localform.addControl("accepttermslink", new FormControl("", this.skipsubmitValidator));
    this.localform.controls["accepttermslink"].setValue("https://swaayam.co/privacy-policy/");
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
      this.submitFormAPI();
    }
  }

  submitformres: any;
  submitFormAPI() {
    if ((this.appstateService.appState.userexists === false || this.appstateService.appState.userexists === undefined || this.appstateService.appState.userexists === null) && this.localform.value["acceptterms"] !== true) {
      alert("Please accept the terms to proceed.");
      return;
    }
    this.apiService.post_api("jobportal", "loginmobile", this.localform.value).subscribe((res) => {
      this.submitformres = res;
      if (this.submitformres.message) {
        if (this.submitformres.status == "TRUE") {
          this.toastService.showSuccess("Success", this.submitformres.message);
        } else if (this.submitformres.status == "FALSE") {
          this.toastService.showError("Error", this.submitformres.message);
        } else {
          this.toastService.showSuccess("Success", this.submitformres.message);
        }
      }
      if (res.status == "TRUE" || res.status === true) {
        if (this.appstateService.postloginstate !== undefined) {
          var postloginstate = this.appstateService.postloginstate.routeConfig.path;
          if (res.gotostate === "candidate/home" || res.gotostate === "employer/home") {
            res.gotostate = postloginstate;
          }
          this.appstateService.postloginstate = undefined;
        }
        this.appstateService.tempdata = this.submitformres;
        this.commonutilsService.processResponse(res);
        this.ngOnInit();
      } else {
      }
    });
  }
}
