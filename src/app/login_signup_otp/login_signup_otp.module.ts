import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { LoginSignupOtpComponent } from "./login_signup_otp.component";

export const routes: Routes = [
  {
    path: "",
    component: LoginSignupOtpComponent,
  },
];

@NgModule({
  declarations: [LoginSignupOtpComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class LoginSignupOtpModule {
  static components = {
    LoginSignupOtpComponent: LoginSignupOtpComponent,
  };
}
