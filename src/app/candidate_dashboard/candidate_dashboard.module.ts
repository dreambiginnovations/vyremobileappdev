import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateDashboardComponent } from "./candidate_dashboard.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateDashboardComponent,
  },
];

@NgModule({
  declarations: [CandidateDashboardComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class CandidateDashboardModule {
  static components = {
    CandidateDashboardComponent: CandidateDashboardComponent,
  };
}
