import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../services/api.service";
import { AppstateService } from "../services/appstate.service";
import { CommonutilsService } from "../services/commonutils.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_job_application_view",
  templateUrl: "./employer_job_application_view.template.html",
  styleUrls: ["./employer_job_application_view.component.scss"],
})
export class EmployerJobApplicationViewComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  id: any;
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, private templateutilsService: TemplateutilsService, private toastService: ToastService, private route: ActivatedRoute, private appstateService: AppstateService) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];
    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("video_id", new FormControl(""));
    this.localform.addControl("photo_id", new FormControl(""));
    this.localform.addControl("name", new FormControl(""));
    this.localform.addControl("mobile", new FormControl("", Validators.compose([Validators.minLength(10), Validators.maxLength(10)])));
    this.localform.addControl("mobiletitle", new FormControl("", Validators.compose([Validators.minLength(10), Validators.maxLength(10)])));
    this.localform.addControl("preferred_job_location_city", new FormControl(""));
    this.localform.addControl("current_salary", new FormControl("", Validators.min(0)));
    this.localform.addControl("dob", new FormControl(""));
    this.localform.addControl("social_share", new FormControl(""));
    this.localform.addControl("job_application_status", new FormControl(""));
    this.localform.addControl("select", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("onhold", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("reject", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("pan_card", new FormControl(""));
    this.localform.addControl("email", new FormControl(""));
    this.localform.addControl("current_location", new FormControl(""));
    this.localform.addControl("current_latitude_location", new FormControl(""));
    this.localform.addControl("current_longitude_location", new FormControl(""));
    this.localform.addControl("current_location_city", new FormControl(""));
    this.localform.addControl("current_location_state", new FormControl(""));
    this.localform.addControl("current_location_country", new FormControl(""));
    this.localform.addControl("preferred_job_location", new FormControl(""));
    this.localform.addControl("preferred_job_latitude_location", new FormControl(""));
    this.localform.addControl("preferred_job_longitude_location", new FormControl(""));
    this.localform.addControl("preferred_job_location_city", new FormControl(""));
    this.localform.addControl("preferred_job_location_state", new FormControl(""));
    this.localform.addControl("preferred_job_location_country", new FormControl(""));
    this.localform.addControl("job_category_id", new FormControl(""));
    this.load_category_id_options().subscribe((res) => {
      this.response_category_id_options = res;
    });
    this.localform.addControl("job_role_id", new FormControl(""));
    this.load_role_id_options().subscribe((res) => {
      this.response_role_id_options = res;
    });
    this.localform.addControl("languages_known", new FormControl(""));
    this.localform.addControl("resume_id", new FormControl(""));
    this.localform.addControl("message", new FormControl(""));
    this.localform.addControl("id", new FormControl(""));
    this.localform.addControl("about_yourself", new FormControl(""));
    this.localform.addControl("attachments", new FormArray([]));
    this.localform.addControl("screening_questions", new FormArray([]));
  }

  markApplication(formgroup, new_job_application_status) {
    this.apiService.post_api("jobportal", "updatejobapplication", { id: this.id, job_application_status: new_job_application_status }).subscribe((res) => {
      formgroup.controls["job_application_status"].setValue(new_job_application_status);
    });
  }
  addNode_attachments(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("attachment_media_id", new FormControl(""));

    if (nodeData !== undefined) {
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["attachment_media_id"].setValue(nodeData["attachment_media_id"]);
    }
    (<FormArray>this.localform.controls["attachments"]).push(nodeControl);
  }

  removeNode_attachments(index) {
    (<FormArray>this.localform.controls["attachments"]).removeAt(index);
  }

  addNode_screening_questions(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("question", new FormControl(""));
    nodeControl.addControl("answer_id", new FormControl(""));

    if (nodeData !== undefined) {
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["question"].setValue(nodeData["question"]);
      nodeControl.controls["answer_id"].setValue(nodeData["answer_id"]);
    }
    (<FormArray>this.localform.controls["screening_questions"]).push(nodeControl);
  }

  removeNode_screening_questions(index) {
    (<FormArray>this.localform.controls["screening_questions"]).removeAt(index);
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getjobapplication", { id: this.id }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;
      this.get_rating_related_info();
      this.processRatingsInfo();
      
      this.localform.controls["video_id"].setValue(this.response_data["video_id"]);
      this.localform.controls["photo_id"].setValue(this.response_data["photo_id"]);
      this.localform.controls["name"].setValue(this.response_data["name"]);
      this.localform.controls["mobile"].setValue(this.response_data["mobile"]);
      this.localform.controls["mobiletitle"].setValue(this.response_data["mobiletitle"]);
      this.localform.controls["preferred_job_location_city"].setValue(this.response_data["preferred_job_location_city"]);
      this.localform.controls["current_salary"].setValue(this.response_data["current_salary"]);
      this.localform.controls["dob"].setValue(this.response_data["dob"]);
      this.localform.controls["social_share"].setValue(this.response_data["social_share"]);
      this.localform.controls["job_application_status"].setValue(this.response_data["job_application_status"]);
      this.localform.controls["select"].setValue(this.response_data["select"]);
      this.localform.controls["onhold"].setValue(this.response_data["onhold"]);
      this.localform.controls["reject"].setValue(this.response_data["reject"]);
      this.localform.controls["pan_card"].setValue(this.response_data["pan_card"]);
      this.localform.controls["email"].setValue(this.response_data["email"]);
      this.localform.controls["current_location"].setValue(this.response_data["current_location"]);
      this.localform.controls["current_latitude_location"].setValue(this.response_data["current_latitude_location"]);
      this.localform.controls["current_longitude_location"].setValue(this.response_data["current_longitude_location"]);
      this.localform.controls["current_location_city"].setValue(this.response_data["current_location_city"]);
      this.localform.controls["current_location_state"].setValue(this.response_data["current_location_state"]);
      this.localform.controls["current_location_country"].setValue(this.response_data["current_location_country"]);
      this.localform.controls["preferred_job_location"].setValue(this.response_data["preferred_job_location"]);
      this.localform.controls["preferred_job_latitude_location"].setValue(this.response_data["preferred_job_latitude_location"]);
      this.localform.controls["preferred_job_longitude_location"].setValue(this.response_data["preferred_job_longitude_location"]);
      this.localform.controls["preferred_job_location_city"].setValue(this.response_data["preferred_job_location_city"]);
      this.localform.controls["preferred_job_location_state"].setValue(this.response_data["preferred_job_location_state"]);
      this.localform.controls["preferred_job_location_country"].setValue(this.response_data["preferred_job_location_country"]);
      this.localform.controls["job_category_id"].setValue(this.response_data["job_category_id"]);
      this.localform.controls["job_role_id"].setValue(this.response_data["job_role_id"]);
      this.localform.controls["languages_known"].setValue(this.response_data["languages_known"]);
      this.localform.controls["resume_id"].setValue(this.response_data["resume_id"]);
      this.localform.controls["message"].setValue(this.response_data["message"]);
      this.localform.controls["id"].setValue(this.response_data["id"]);
      this.localform.controls["about_yourself"].setValue(this.response_data["about_yourself"]);
      this.localform.controls["attachments"] = new FormArray([]);
      if (this.response_data["attachments"] !== undefined) {
        this.response_data["attachments"].forEach((node) => {
          this.addNode_attachments(node);
        });
      }

      this.localform.controls["screening_questions"] = new FormArray([]);
      if (this.response_data["screening_questions"] !== undefined) {
        this.response_data["screening_questions"].forEach((node) => {
          this.addNode_screening_questions(node);
        });
      }
    });
  }
  communicationUserRating: number;
  communicationAvgRating: number = 0;
  expertiseUserRating: number;
  expertiseAvgRating: number = 0;
  presentabilityUserRating: number;
  presentabilityAvgRating: number = 0;
  showRating = false;
  processRatingsInfo(ratings?) {
    if(ratings===undefined) {ratings = this.response_data.ratings;}
    var currentUserID = this.appstateService.appState.user.id;
    var communicationCount = 0, expertiseCount = 0, presentabilityCount = 0;
    
    this.communicationAvgRating = 0;
    this.expertiseAvgRating = 0;
    this.presentabilityAvgRating = 0;
    
    for(var count=0;count<ratings.length;count++) {
      var obj = ratings[count];
      if(obj["rating_category"]=="COMMUNICATION") {
        this.communicationAvgRating += obj["rating"];
        communicationCount++;
        if(currentUserID==obj["employer_user_id"]) {this.communicationUserRating = obj["rating"];}
      } else if(obj["rating_category"]=="EXPERTISE") {
        this.expertiseAvgRating += obj["rating"];
        expertiseCount++;
        if(currentUserID==obj["employer_user_id"]) {this.expertiseUserRating = obj["rating"];}
      } else if(obj["rating_category"]=="PRESENTABILITY") {
        this.presentabilityAvgRating += obj["rating"];
        presentabilityCount++;
        if(currentUserID==obj["employer_user_id"]) {this.presentabilityUserRating = obj["rating"];}
      }
    }
    if(this.communicationAvgRating!=0) {this.communicationAvgRating = Math.ceil(this.communicationAvgRating/communicationCount);}
    if(this.expertiseAvgRating!=0) {this.expertiseAvgRating = Math.ceil(this.expertiseAvgRating/expertiseCount);}
    if(this.presentabilityAvgRating!=0) {this.presentabilityAvgRating = Math.ceil(this.presentabilityAvgRating/presentabilityCount);}
    
    this.showRating = true;
  }

  response_category_id_options: any;
  load_category_id_options() {
    return this.apiService.post_api("jobportal", "getjobcategoriesoptions");
  }

  response_role_id_options: any;
  load_role_id_options() {
    return this.apiService.post_api("jobportal", "getjobrolesoptions");
  }
  
  request_video_from_user() {
    this.apiService.post_api("jobportal", "requestvideofromuser", { candidate_user_id: this.response_data["candidate_user_id"], requested_by_company_id:  this.appstateService.appState.user.company_id, requested_from_url: this.route.snapshot["_routerState"]["url"]}).subscribe((res) => {
      this.toastService.showSuccess("Request sent", "We sent a notification to the candidate requesting to upload the video. We shall notify you as soon as he/she uploads.");
      this.ngOnInit();
    });    
  }
  employerRating:any;
  avgRating:any;
  get_rating_related_info() {
    this.apiService.post_api("jobportal", "processcandidateprofilevideoratingforemployer", {candidate_user_id: this.response_data["candidate_user_id"]}).subscribe((res) => {
      this.employerRating = res.employerRating;
      this.avgRating = res.avgRating;
    });
  };
  onRatingChange(event: any) {
    console.log("onRatingChange", event);
    this.apiService.post_api("jobportal", "processcandidateprofilevideoratingforemployer", {candidate_user_id: this.response_data["candidate_user_id"], "rating": event.rating}).subscribe((res) => {
      this.employerRating = res.employerRating;
      this.avgRating = res.avgRating;
    });
  }
  onInterviewRatingChange(ratingCategory, event) {
    console.log("onInterviewRatingChange", event);
    this.apiService.post_api("jobportal", "processcandidateinterviewratingforemployer", {job_application_id: this.id, "rating_category": ratingCategory, "rating": event.rating}).subscribe((res) => {
      this.processRatingsInfo(res.ratings);
    });
  }
}
