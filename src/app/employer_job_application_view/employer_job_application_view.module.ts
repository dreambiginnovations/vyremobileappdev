import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { StarRatingModule } from 'angular-star-rating';

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { FileUploaderModule } from "../common/fileuploader/fileuploader.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { CustomDropDownModule } from "../common/dropdown/dropdown.module";
import { ShareButtonsModule } from "ngx-sharebuttons/buttons";
import { ShareIconsModule } from "ngx-sharebuttons/icons";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerJobApplicationViewComponent } from "./employer_job_application_view.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerJobApplicationViewComponent,
  },
];

@NgModule({
  declarations: [EmployerJobApplicationViewComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, FileUploaderModule, VideoRecordModule, CustomDropDownModule, ShareButtonsModule, ShareIconsModule, StarRatingModule.forRoot()],
})
export class EmployerJobApplicationViewModule {
  static components = {
    EmployerJobApplicationViewComponent: EmployerJobApplicationViewComponent,
  };
}
