import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { NotificationsComponent } from "./notifications.component";

export const routes: Routes = [
  {
    path: "",
    component: NotificationsComponent,
  },
];

@NgModule({
  declarations: [NotificationsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class NotificationsModule {
  static components = {
    NotificationsComponent: NotificationsComponent,
  };
}
