import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { TemplateutilsService } from "../services/templateutils.service";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__notifications",
  templateUrl: "./notifications.template.html",
  styleUrls: ["./notifications.component.scss"],
})
export class NotificationsComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  constructor(public templateutilsService: TemplateutilsService, private apiService: ApiService, private commonutilsService: CommonutilsService, private toastService: ToastService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.initializeFormGroup();

    this.route.queryParams.subscribe((queryParams) => {
      if (queryParams.page_number) {
        this.page_number = parseInt(queryParams.page_number);
      } else {
        this.page_number = 1;
      }
      this.load_data();
    });
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("notifications", new FormArray([]));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  markNotification(formgroup, notification_read_status, status_id) {
    if (status_id === 0 && !confirm("Are you sure?")) {
      return;
    }
    formgroup.controls["notification_read_status"].setValue(notification_read_status);
    formgroup.controls["status_id"].setValue(status_id);
    this.apiService.post_api("jobportal", "updatenotification", { id: formgroup.value["id"], notification_read_status: formgroup.value["notification_read_status"], status_id: formgroup.value["status_id"] }).subscribe((res) => {
      this.ngOnInit();
    });
  }
  evalCode(code) {
    console.log("eval", code);
    eval(code);
  }
  addNode_notifications(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("created_at", new FormControl(""));
    nodeControl.addControl("notification_title", new FormControl(""));
    nodeControl.addControl("notification_text", new FormControl(""));
    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("status_id", new FormControl(""));
    nodeControl.addControl("notification_read_status", new FormControl(""));
    nodeControl.addControl("notification_action_onclick", new FormControl(""));
    nodeControl.addControl("select", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("onhold", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("reject", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("reject", new FormControl("", this.skipsubmitValidator));

    if (nodeData !== undefined) {
      nodeControl.controls["created_at"].setValue(nodeData["created_at"]);
      nodeControl.controls["notification_title"].setValue(nodeData["notification_title"]);
      nodeControl.controls["notification_text"].setValue(nodeData["notification_text"]);
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["status_id"].setValue(nodeData["status_id"]);
      nodeControl.controls["notification_read_status"].setValue(nodeData["notification_read_status"]);
      nodeControl.controls["notification_action_onclick"].setValue(nodeData["notification_action_onclick"]);
      nodeControl.controls["select"].setValue(nodeData["select"]);
      nodeControl.controls["onhold"].setValue(nodeData["onhold"]);
      nodeControl.controls["reject"].setValue(nodeData["reject"]);
      nodeControl.controls["reject"].setValue(nodeData["reject"]);
    }
    (<FormArray>this.localform.controls["notifications"]).push(nodeControl);
  }

  removeNode_notifications(index) {
    (<FormArray>this.localform.controls["notifications"]).removeAt(index);
  }

  records_per_page = 6;
  page_number = 1;
  total_pages;
  load_data_page(page_number) {
    var newQueryParams = Object.assign({}, this.route.snapshot.queryParams);
    newQueryParams["page_number"] = page_number;
    this.router.navigate(["."], { relativeTo: this.route, queryParams: newQueryParams });
  }
  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getusernotifications", { records_per_page: this.records_per_page, page_number: this.page_number }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res.response;
      if (!isNaN(res.total)) {
        this.total_pages = Math.ceil(res.total / this.records_per_page);
      } else {
        this.total_pages = Math.ceil(res.total[Object.keys(res.total)[0]] / this.records_per_page);
      }

      this.localform.controls["notifications"] = new FormArray([]);
      if (this.response_data["notifications"] !== undefined) {
        this.response_data["notifications"].forEach((node) => {
          this.addNode_notifications(node);
        });
      }
    });
  }
}
