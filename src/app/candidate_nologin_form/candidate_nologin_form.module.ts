import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateNologinFormComponent } from "./candidate_nologin_form.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateNologinFormComponent,
  },
];

@NgModule({
  declarations: [CandidateNologinFormComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class CandidateNologinFormModule {
  static components = {
    CandidateNologinFormComponent: CandidateNologinFormComponent,
  };
}
