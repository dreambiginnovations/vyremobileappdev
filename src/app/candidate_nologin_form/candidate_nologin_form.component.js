"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var CandidateNologinFormComponent = /** @class */ (function () {
    function CandidateNologinFormComponent(dialogService, apiService, commonutilsService, toastService) {
        this.dialogService = dialogService;
        this.apiService = apiService;
        this.commonutilsService = commonutilsService;
        this.toastService = toastService;
        this.hostclasses = "container px-0 viewfor__EDIT";
    }
    CandidateNologinFormComponent.prototype.ngOnInit = function () {
        this.initializeFormGroup();
    };
    CandidateNologinFormComponent.prototype.initializeFormGroup = function () {
        this.localform = new forms_1.FormGroup({});
        this.localform.addControl("name", new forms_1.FormControl("", forms_1.Validators.required));
        this.localform.addControl("mobile", new forms_1.FormControl("", forms_1.Validators.compose([forms_1.Validators.minLength(10), forms_1.Validators.maxLength(10)])));
    };
    CandidateNologinFormComponent.prototype.submitForm = function () {
        this.localform.updateValueAndValidity();
        if (this.localform.status == "INVALID") {
            this.hostclasses += " showvalidationerrors";
            alert("Errors in the form");
            return;
        }
        else {
            this.submitFormAPI();
        }
    };
    CandidateNologinFormComponent.prototype.submitFormAPI = function () {
        var _this = this;
        this.apiService.post_api("jobportal", "submitcandidateformfromstatic", this.localform.value).subscribe(function (res) {
            _this.submitformres = res;
            if (_this.submitformres.message) {
                if (_this.submitformres.status == "TRUE") {
                    _this.toastService.showSuccess("Success", _this.submitformres.message);
                }
                else if (_this.submitformres.status == "FALSE") {
                    _this.toastService.showError("Error", _this.submitformres.message);
                }
                else {
                    _this.toastService.showSuccess("Success", _this.submitformres.message);
                }
            }
            if (res.status == "TRUE" || res.status === true) {
                _this.commonutilsService.processResponse(res);
                _this.ngOnInit();
            }
            else {
            }
        });
        this.dialogService.dismissOpenDialogs();
    };
    __decorate([
        core_1.HostBinding("class")
    ], CandidateNologinFormComponent.prototype, "hostclasses");
    CandidateNologinFormComponent = __decorate([
        core_1.Component({
            selector: ".component__candidate_nologin_form",
            templateUrl: "./candidate_nologin_form.template.html",
            styleUrls: ["./candidate_nologin_form.component.scss"]
        })
    ], CandidateNologinFormComponent);
    return CandidateNologinFormComponent;
}());
exports.CandidateNologinFormComponent = CandidateNologinFormComponent;
