import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { FileUploaderModule } from "../common/fileuploader/fileuploader.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { CustomDropDownModule } from "../common/dropdown/dropdown.module";
import { ShareButtonsModule } from "ngx-sharebuttons/buttons";
import { ShareIconsModule } from "ngx-sharebuttons/icons";
import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateCandidateViewDetailsComponent } from "./candidate_candidate_view_details.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateCandidateViewDetailsComponent,
  },
];

@NgModule({
  declarations: [CandidateCandidateViewDetailsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, FileUploaderModule, VideoRecordModule, CustomDropDownModule, ShareButtonsModule, ShareIconsModule],
})
export class CandidateCandidateViewDetailsModule {
  static components = {
    CandidateCandidateViewDetailsComponent: CandidateCandidateViewDetailsComponent,
  };
}
