import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AdminLoginasUserComponent } from "./common/adminloginasuser/adminloginasuser.component";

import { SkipIfAlreadyLoggedIn } from "./services/authguards/skipifloggedin.service";
import { AllowForCandidateLogin } from "./services/authguards/allowforcandidatelogin.service";
import { AllowForEmployerLogin } from "./services/authguards/allowforemployerlogin.service";

const routes: Routes = [
  {
    path: "choose_language",
    loadChildren: () => import("./choose_language/choose_language.module").then((m) => m.ChooseLanguageModule),
  },
  {
    path: "login_signup",
    loadChildren: () => import("./login_signup/login_signup.module").then((m) => m.LoginSignupModule),
    canActivate: [SkipIfAlreadyLoggedIn],
  },
  {
    path: "login_signup_otp",
    loadChildren: () => import("./login_signup_otp/login_signup_otp.module").then((m) => m.LoginSignupOtpModule),
    canActivate: [SkipIfAlreadyLoggedIn],
  },
  {
    path: "choose_login_type",
    loadChildren: () => import("./choose_login_type/choose_login_type.module").then((m) => m.ChooseLoginTypeModule),
  },
  {
    path: "candidate/searchjobs",
    loadChildren: () => import("./candidate_search_jobs/candidate_search_jobs.module").then((m) => m.CandidateSearchJobsModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/guided_profile",
    loadChildren: () => import("./candidate_guided_profile/candidate_guided_profile.module").then((m) => m.CandidateGuidedProfileModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/oneway_interview",
    loadChildren: () => import("./candidate_oneway_interview/candidate_oneway_interview.module").then((m) => m.CandidateOnewayInterviewModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/searchvideojobstemp",
    loadChildren: () => import("./candidate_searchvideo_jobstemp/candidate_searchvideo_jobstemp.module").then((m) => m.CandidateSearchvideoJobstempModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/myjobs",
    loadChildren: () => import("./candidate_my_jobs/candidate_my_jobs.module").then((m) => m.CandidateMyJobsModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/favoritejobs",
    loadChildren: () => import("./candidate_favorite_jobs/candidate_favorite_jobs.module").then((m) => m.CandidateFavoriteJobsModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/job/:id",
    loadChildren: () => import("./candidate_job_view_details/candidate_job_view_details.module").then((m) => m.CandidateJobViewDetailsModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/insuff_profile",
    loadChildren: () => import("./candidate_insuff_profile/candidate_insuff_profile.module").then((m) => m.CandidateInsuffProfileModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/editprofile",
    loadChildren: () => import("./candidate_candidate_edit_details/candidate_candidate_edit_details.module").then((m) => m.CandidateCandidateEditDetailsModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/tempeditprofile/:session_id",
    loadChildren: () => import("./candidate_candidate_edit_details_tempsession/candidate_candidate_edit_details_tempsession.module").then((m) => m.CandidateCandidateEditDetailsTempsessionModule),
  },
  {
    path: "candidate/myprofile",
    loadChildren: () => import("./candidate_candidate_view_details/candidate_candidate_view_details.module").then((m) => m.CandidateCandidateViewDetailsModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/profile/:id",
    loadChildren: () => import("./candidate_public_view_details/candidate_public_view_details.module").then((m) => m.CandidatePublicViewDetailsModule),
  },
  {
    path: "candidate/dashboard",
    loadChildren: () => import("./candidate_dashboard/candidate_dashboard.module").then((m) => m.CandidateDashboardModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/home",
    loadChildren: () => import("./navbarexpandedcandidate/navbarexpandedcandidate.module").then((m) => m.NavbarexpandedcandidateModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/applyforjob/:id",
    loadChildren: () => import("./candidate_job_application_add/candidate_job_application_add.module").then((m) => m.CandidateJobApplicationAddModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/jobapplication/:id",
    loadChildren: () => import("./candidate_job_application_view/candidate_job_application_view.module").then((m) => m.CandidateJobApplicationViewModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/notifications",
    loadChildren: () => import("./notifications/notifications.module").then((m) => m.NotificationsModule),
    canActivate: [AllowForCandidateLogin],
  },

  {
    path: "employer/editprofile",
    loadChildren: () => import("./employer_employer_edit_details/employer_employer_edit_details.module").then((m) => m.EmployerEmployerEditDetailsModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/tempeditprofile/:session_id",
    loadChildren: () => import("./employer_employer_edit_details_tempsession/employer_employer_edit_details_tempsession.module").then((m) => m.EmployerEmployerEditDetailsTempsessionModule),
  },
  {
    path: "employer/myprofile",
    loadChildren: () => import("./employer_employer_view_details/employer_employer_view_details.module").then((m) => m.EmployerEmployerViewDetailsModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/editcompany/:id",
    loadChildren: () => import("./employer_company_edit_details/employer_company_edit_details.module").then((m) => m.EmployerCompanyEditDetailsModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/mycompany",
    loadChildren: () => import("./employer_company_view_details/employer_company_view_details.module").then((m) => m.EmployerCompanyViewDetailsModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/addcompany",
    loadChildren: () => import("./employer_company_add/employer_company_add.module").then((m) => m.EmployerCompanyAddModule),
    canActivate: [AllowForEmployerLogin],
  },

  {
    path: "employer/findjobseekers",
    loadChildren: () => import("./employer_job_seekers/employer_job_seekers.module").then((m) => m.EmployerJobSeekersModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/jobseeker/:id",
    loadChildren: () => import("./employer_job_seeker_view/employer_job_seeker_view.module").then((m) => m.EmployerJobSeekerViewModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/myjobapplications",
    loadChildren: () => import("./employer_job_applications/employer_job_applications.module").then((m) => m.EmployerJobApplicationsModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/jobapplication/:id",
    loadChildren: () => import("./employer_job_application_view/employer_job_application_view.module").then((m) => m.EmployerJobApplicationViewModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/insuff_profile",
    loadChildren: () => import("./employer_insuff_profile/employer_insuff_profile.module").then((m) => m.EmployerInsuffProfileModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/home",
    loadChildren: () => import("./navbarexpandedemployer/navbarexpandedemployer.module").then((m) => m.NavbarexpandedemployerModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/dashboard",
    loadChildren: () => import("./employer_dashboard/employer_dashboard.module").then((m) => m.EmployerDashboardModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/myjobs",
    loadChildren: () => import("./employer_posted_jobs/employer_posted_jobs.module").then((m) => m.EmployerPostedJobsModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/editjob/:id",
    loadChildren: () => import("./employer_job_edit_details/employer_job_edit_details.module").then((m) => m.EmployerJobEditDetailsModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/addjob/:company_id",
    loadChildren: () => import("./employer_company_job_add/employer_company_job_add.module").then((m) => m.EmployerCompanyJobAddModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/job/:id",
    loadChildren: () => import("./employer_job_view_details/employer_job_view_details.module").then((m) => m.EmployerJobViewDetailsModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/notifications",
    loadChildren: () => import("./notifications/notifications.module").then((m) => m.NotificationsModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "employer/employer_manage_team",
    loadChildren: () => import("./employer_manage_team/employer_manage_team.module").then((m) => m.EmployerManageTeamModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "candidate/request_video_resume",
    loadChildren: () => import("./employer_request_video_resume/employer_request_video_resume.module").then((m) => m.EmployerRequestVideoResumeModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "candidate/request_for_interview",
    loadChildren: () => import("./candidate_request_for_interview/candidate_request_for_interview.module").then((m) => m.CandidateRequestForInterviewModule),
    canActivate: [AllowForCandidateLogin],
  },
  {
    path: "employer/oneway_interview",
    loadChildren: () => import("./employer_oneway_interview/employer_oneway_interview.module").then((m) => m.EmployerOnewayInterviewModule),
    canActivate: [AllowForEmployerLogin],
  },
  {
    path: "company/:id",
    loadChildren: () => import("./public_company_view_details/public_company_view_details.module").then((m) => m.PublicCompanyViewDetailsModule),
  },
  {
    path: "job/:id",
    loadChildren: () => import("./public_job_view_details/public_job_view_details.module").then((m) => m.PublicJobViewDetailsModule),
  },
  {
    path: "jobs",
    loadChildren: () => import("./public_jobs/public_jobs.module").then((m) => m.PublicJobsModule),
  },
  {
    path: "lms",
    loadChildren: () => import("./lms/lms.module").then((m) => m.LmsModule),
  },
  {
    path: "coursesincategory/:id",
    loadChildren: () => import("./courses/courses.module").then((m) => m.CoursesModule),
  },
  {
    path: "courses",
    loadChildren: () => import("./courses/courses.module").then((m) => m.CoursesModule),
  },
  {
    path: "course/:id",
    loadChildren: () => import("./course_view/course_view.module").then((m) => m.CourseViewModule),
  },
  {
    path: "lessons/:course_id",
    loadChildren: () => import("./lessons/lessons.module").then((m) => m.LessonsModule),
  },
  {
    path: "lesson/:id",
    loadChildren: () => import("./lesson_view/lesson_view.module").then((m) => m.LessonViewModule),
  },

  {
    path: "admin/loginasuser",
    component: AdminLoginasUserComponent,
  },
  //TODO for Aneesh
  /*
  {
    path: "admin/login",
    loadChildren: () => import("./admin_login/admin_login.module").then((m) => m.AdminLoginModule),
    //loginAsAdmin: on success go to dashboard
  },
  {
    path: "admin/dashboard",
    loadChildren: () => import("./admin_dashboard/admin_dashboard.module").then((m) => m.AdminDashboardModule),
    //API will be getAdminDashboardSummary
  },
  {
    path: "admin/users/:context",
    //context can be manage or whatsapp
    loadChildren: () => import("./admin_users/admin_users.module").then((m) => m.AdminUsersModule),
    //APIs will be getAdminUsersKPIData and getAdminUsersList
  },
  {
    path: "admin/candidates/:context",
    //context can be status_wise or channel_wise or activity_wise
    loadChildren: () => import("./admin_candidates/admin_candidates.module").then((m) => m.AdminCandidatesModule),
    //APIs will be getAdminCandidatesKPIData and getAdminCandidatesList
  },
  {
    path: "admin/employers/:context",
    //context can be status_wise or activity_wise
    loadChildren: () => import("./admin_employers/admin_employers.module").then((m) => m.AdminEmployersModule),
    //APIs will be getAdminEmployersKPIData and getAdminEmployersList
  },
  {
    path: "admin/jobs/:context",
    //context can be status_wise
    loadChildren: () => import("./admin_jobs/admin_jobs.module").then((m) => m.AdminJobsModule),
    //APIs will be getAdminJobsKPIData and getAdminJobsList
  },
  {
    path: "admin/jobapplications/:context",
    //context can be status_wise
    loadChildren: () => import("./admin_jobapplications/admin_jobapplications.module").then((m) => m.AdminJobApplicationsModule),
    //APIs will be getAdminJobApplicationsKPIData and getAdminJobApplicationsList
  },
  
  {
    path: "admin/lms/coursecategories",
    loadChildren: () => import("./admin_lms_coursecategories/admin_lms_coursecategories.module").then((m) => m.AdminCourseCategoriesModule),
    //APIs will be getAdminLMSCourseCategoriesKPIData and getAdminLMSCourseCategoriesList
  },
  {
    path: "admin/lms/courses",
    loadChildren: () => import("./admin_lms_courses/admin_lms_courses.module").then((m) => m.AdminCoursesModule),
    //APIs will be getAdminLMSCoursesKPIData and getAdminLMSCoursesList
  },
  {
    path: "admin/lms/lessons",
    loadChildren: () => import("./admin_lms_lessons/admin_lms_lessons.module").then((m) => m.AdminLessonsModule),
    //APIs will be getAdminLMSLessonsKPIData and getAdminLMSLessonsList
  },
  
  {
    path: "admin/notificationtemplates",
    loadChildren: () => import("./admin_notificationtemplates/admin_notificationtemplates.module").then((m) => m.AdminNotificationTemplatesModule),
    //APIs will be getAdminNotificationTemplatesKPIData and getAdminNotificationTemplatesList
  },
  {
    path: "admin/notifications",
    loadChildren: () => import("./admin_notifications/admin_notifications.module").then((m) => m.AdminNotificationsModule),
    //APIs will be getAdminNotificationsKPIData and getAdminNotificationsList
  },
  
  {
    path: "admin/bulkupload",
    loadChildren: () => import("./admin_bulkupload/admin_bulkupload.module").then((m) => m.AdminBulkUploadModule),
    //APIs will be decided later
  },
  */

  { path: "", redirectTo: "login_signup", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
