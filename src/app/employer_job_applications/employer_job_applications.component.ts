import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { ApiService } from "../services/api.service";
import { AppstateService } from "../services/appstate.service";
import { CommonutilsService } from "../services/commonutils.service";
import { CommunicationService } from "../services/communication.service";
import { DialogService } from "../services/dialog.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_job_applications",
  templateUrl: "./employer_job_applications.template.html",
  styleUrls: ["./employer_job_applications.component.scss"],
})
export class EmployerJobApplicationsComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS viewhas__filters";
  id: any;
  constructor(
    private apiService: ApiService,
    private appstateService: AppstateService,
    private commonutilsService: CommonutilsService,
    private communicationService: CommunicationService,
    public dialogService: DialogService,
    public templateutilsService: TemplateutilsService,
    private toastService: ToastService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];

    if (this.route.snapshot.queryParams.job_id) {
      this.filter_data = { job_id: this.route.snapshot.queryParams.job_id };
    }

    this.initializeFormGroup();

    this.route.queryParams.subscribe((queryParams) => {
      if (queryParams.page_number) {
        this.page_number = parseInt(queryParams.page_number);
      } else {
        this.page_number = 1;
      }
      this.load_data();
    });
  }

  filter_data = {};
  filtermodalid: number;
  openFilterModal() {
    if (this.filtermodalid === undefined) {
      this.filtermodalid = Math.floor(Math.random() * 10000 + 1);
    }
    this.communicationService.communication = { context: { parentid: this.filtermodalid }, obj: { filter_data: this.filter_data } };
    this.dialogService.openComponent("EmployerJobApplicationsFilterComponent", "EmployerJobApplicationsFilterModule", this.dialogService.DIALOGTYPES.LG, undefined, "fullscreendialog");
    let parentSubscription = this.communicationService.detectNewCommunication().subscribe(() => {
      if (this.communicationService.communication.context.parentid != this.filtermodalid) {
        return;
      }
      this.filter_data = this.communicationService.communication.obj.filter_data;
      this.page_number = 1;
      var newQueryParams = Object.assign({}, this.route.snapshot.queryParams);
      for (var paramKey in this.filter_data) {
        if (newQueryParams[paramKey] !== undefined) {
          delete newQueryParams[paramKey];
        }
      }
      if (newQueryParams["page_number"] !== undefined) {
        delete newQueryParams["page_number"];
      }
      this.router.navigate([], { queryParams: newQueryParams });
      this.load_data();
      parentSubscription.unsubscribe();
    });
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("filter_button", new FormControl(""));
    this.localform.addControl("applications", new FormArray([]));
  }

  markApplication(formgroup, new_job_application_status) {
    this.apiService.post_api("jobportal", "updatejobapplication", { id: formgroup.value["id"], job_application_status: new_job_application_status }).subscribe((res) => {
      this.toastService.showSuccess("Done", res.message);
      formgroup.controls["job_application_status"].setValue(new_job_application_status);
    });
  }
  addNode_applications(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("video_id", new FormControl(""));
    nodeControl.addControl("photo_id", new FormControl(""));
    nodeControl.addControl("name", new FormControl(""));
    nodeControl.addControl("mobile", new FormControl("", Validators.compose([Validators.minLength(10), Validators.maxLength(10)])));
    nodeControl.addControl("mobiletitle", new FormControl("", Validators.compose([Validators.minLength(10), Validators.maxLength(10)])));
    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("sort_button", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("preferred_job_location_city", new FormControl(""));
    nodeControl.addControl("job_title", new FormControl(""));
    nodeControl.addControl("current_salary", new FormControl("", Validators.min(0)));
    nodeControl.addControl("dob", new FormControl(""));
    nodeControl.addControl("candidate_user_id", new FormControl(""));
    nodeControl.addControl("screeningquestions", new FormControl(false));
    nodeControl.addControl("video_requested_id", new FormControl(""));
    nodeControl.addControl("video_requested_created_at", new FormControl(""));
    nodeControl.addControl("created_at", new FormControl(""));
    nodeControl.addControl("social_share", new FormControl(""));
    nodeControl.addControl("job_application_status", new FormControl(""));
    nodeControl.addControl("select", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("onhold", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("reject", new FormControl("", this.skipsubmitValidator));

    if (nodeData !== undefined) {
      nodeControl.controls["video_id"].setValue(nodeData["video_id"]);
      nodeControl.controls["photo_id"].setValue(nodeData["photo_id"]);
      nodeControl.controls["name"].setValue(nodeData["name"]);
      nodeControl.controls["mobile"].setValue(nodeData["mobile"]);
      nodeControl.controls["mobiletitle"].setValue(nodeData["mobiletitle"]);
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["candidate_user_id"].setValue(nodeData["candidate_user_id"]);
      nodeControl.controls["screeningquestions"].setValue(nodeData["screeningquestions"]);
      nodeControl.controls["video_requested_id"].setValue(nodeData["video_requested_id"]);
      nodeControl.controls["video_requested_created_at"].setValue(nodeData["video_requested_created_at"]);
      nodeControl.controls["created_at"].setValue(nodeData["created_at"]);
      nodeControl.controls["sort_button"].setValue(nodeData["sort_button"]);
      nodeControl.controls["preferred_job_location_city"].setValue(nodeData["preferred_job_location_city"]);
      nodeControl.controls["job_title"].setValue(nodeData["job_title"]);
      nodeControl.controls["current_salary"].setValue(nodeData["current_salary"]);
      nodeControl.controls["dob"].setValue(nodeData["dob"]);
      nodeControl.controls["social_share"].setValue(nodeData["social_share"]);
      nodeControl.controls["job_application_status"].setValue(nodeData["job_application_status"]);
      nodeControl.controls["select"].setValue(nodeData["select"]);
      nodeControl.controls["onhold"].setValue(nodeData["onhold"]);
      nodeControl.controls["reject"].setValue(nodeData["reject"]);
    }
    (<FormArray>this.localform.controls["applications"]).push(nodeControl);
  }

  removeNode_applications(index) {
    (<FormArray>this.localform.controls["applications"]).removeAt(index);
  }

  records_per_page = 6;
  page_number = 1;
  total_pages;
  load_data_page(page_number) {
    var newQueryParams = Object.assign({}, this.route.snapshot.queryParams);
    newQueryParams["page_number"] = page_number;
    this.router.navigate(["."], { relativeTo: this.route, queryParams: newQueryParams });
  }
  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getemployerjobapplications", { id: this.id, filters: this.filter_data, records_per_page: this.records_per_page, page_number: this.page_number }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res.response;
      if (!isNaN(res.total)) {
        this.total_pages = Math.ceil(res.total / this.records_per_page);
      } else {
        this.total_pages = Math.ceil(res.total[Object.keys(res.total)[0]] / this.records_per_page);
      }

      this.localform.controls["filter_button"].setValue(this.response_data["filter_button"]);
      this.localform.controls["applications"] = new FormArray([]);
      if (this.response_data["applications"] !== undefined) {
        this.response_data["applications"].forEach((node) => {
          this.addNode_applications(node);
        });
      }
    });
  }
  
  request_video_from_user(candidate_user_id) {
    this.apiService.post_api("jobportal", "requestvideofromuser", { candidate_user_id: candidate_user_id, requested_by_company_id:  this.appstateService.appState.user.company_id, requested_from_url: this.route.snapshot["_routerState"]["url"]}).subscribe((res) => {
      this.toastService.showSuccess("Request sent", "We sent a notification to the candidate requesting to upload the video. We shall notify you as soon as the candidate uploads.");
      this.ngOnInit();
    });    
  }
}
