import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { ShareButtonsModule } from "ngx-sharebuttons/buttons";
import { ShareIconsModule } from "ngx-sharebuttons/icons";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerJobApplicationsComponent } from "./employer_job_applications.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerJobApplicationsComponent,
  },
];

@NgModule({
  declarations: [EmployerJobApplicationsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, VideoRecordModule, ShareButtonsModule, ShareIconsModule],
})
export class EmployerJobApplicationsModule {
  static components = {
    EmployerJobApplicationsComponent: EmployerJobApplicationsComponent,
  };
}
