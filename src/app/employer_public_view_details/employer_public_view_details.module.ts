import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { FileUploaderModule } from "../common/fileuploader/fileuploader.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerPublicViewDetailsComponent } from "./employer_public_view_details.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerPublicViewDetailsComponent,
  },
];

@NgModule({
  declarations: [EmployerPublicViewDetailsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, FileUploaderModule, VideoRecordModule],
})
export class EmployerPublicViewDetailsModule {
  static components = {
    EmployerPublicViewDetailsComponent: EmployerPublicViewDetailsComponent,
  };
}
