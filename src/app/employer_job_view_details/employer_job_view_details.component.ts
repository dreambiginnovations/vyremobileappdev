import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_job_view_details",
  templateUrl: "./employer_job_view_details.template.html",
  styleUrls: ["./employer_job_view_details.component.scss"],
})
export class EmployerJobViewDetailsComponent implements OnInit {
  @HostBinding("class") hostclasses = "nonstandardiconcontainer container px-0 viewfor__DETAILS";
  id: any;
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, public templateutilsService: TemplateutilsService, private toastService: ToastService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];

    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("video_id", new FormControl(""));
    this.localform.addControl("company_photo_id", new FormControl(""));
    this.localform.addControl("job_title", new FormControl(""));
    this.localform.addControl("job_location_city", new FormControl(""));
    this.localform.addControl("min_job_salary", new FormControl("", Validators.min(0)));
    this.localform.addControl("num_of_openings", new FormControl("", Validators.min(0)));
    this.localform.addControl("experience", new FormControl(""));
    this.localform.addControl("social_share", new FormControl(""));
    this.localform.addControl("view_details", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("view_applications", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("unpublish_details", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("publish_details", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("status_id", new FormControl(""));
    this.localform.addControl("dummyunpublished", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("job_category_id", new FormControl(""));
    this.load_category_id_options().subscribe((res) => {
      this.response_category_id_options = res;
    });
    this.localform.addControl("job_role_id", new FormControl(""));
    this.load_role_id_options().subscribe((res) => {
      this.response_role_id_options = res;
    });
    this.localform.addControl("min_experience", new FormControl("", Validators.min(0)));
    this.localform.addControl("max_experience", new FormControl("", Validators.min(0)));
    this.localform.addControl("is_remote_job", new FormControl(""));
    this.localform.addControl("job_location", new FormControl(""));
    this.localform.addControl("job_latitude_location", new FormControl(""));
    this.localform.addControl("job_longitude_location", new FormControl(""));
    this.localform.addControl("job_location_city", new FormControl(""));
    this.localform.addControl("job_location_state", new FormControl(""));
    this.localform.addControl("job_location_country", new FormControl(""));
    this.localform.addControl("min_job_salary", new FormControl("", Validators.min(0)));
    this.localform.addControl("max_job_salary", new FormControl("", Validators.min(0)));
    this.localform.addControl("education_requirement", new FormControl(""));
    this.localform.addControl("num_of_openings", new FormControl("", Validators.min(0)));
    this.localform.addControl("id", new FormControl(""));
    this.localform.addControl("num_of_applications", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("job_description", new FormControl(""));
    this.localform.addControl("job_description_clean", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("attachments", new FormArray([], Validators.min(1)));
    this.localform.addControl("screening_questions", new FormArray([]));
  }

  unpublishJob(jobid) {
    if (!confirm("Are you sure you want to delete this job?")) {
      return;
    }
    this.apiService.post_api("jobportal", "unpublishemployerjob", { id: jobid }).subscribe((res) => {
      this.ngOnInit();
    });
  }
  publishJob(jobid) {
    if (!confirm("Are you sure you want to delete this job?")) {
      return;
    }
    this.apiService.post_api("jobportal", "publishemployerjob", { id: jobid }).subscribe((res) => {
      this.ngOnInit();
    });
  }
  addNode_attachments(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("attachment_media_id", new FormControl(""));

    if (nodeData !== undefined) {
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["attachment_media_id"].setValue(nodeData["attachment_media_id"]);
    }
    (<FormArray>this.localform.controls["attachments"]).push(nodeControl);
  }

  removeNode_attachments(index) {
    (<FormArray>this.localform.controls["attachments"]).removeAt(index);
  }

  addNode_screening_questions(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("question", new FormControl(""));

    if (nodeData !== undefined) {
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["question"].setValue(nodeData["question"]);
    }
    (<FormArray>this.localform.controls["screening_questions"]).push(nodeControl);
  }

  removeNode_screening_questions(index) {
    (<FormArray>this.localform.controls["screening_questions"]).removeAt(index);
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getjob", { id: this.id }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["video_id"].setValue(this.response_data["video_id"]);
      this.localform.controls["company_photo_id"].setValue(this.response_data["company_photo_id"]);
      this.localform.controls["job_title"].setValue(this.response_data["job_title"]);
      this.localform.controls["job_location_city"].setValue(this.response_data["job_location_city"]);
      this.localform.controls["min_job_salary"].setValue(this.response_data["min_job_salary"]);
      this.localform.controls["num_of_openings"].setValue(this.response_data["num_of_openings"]);
      this.localform.controls["experience"].setValue(this.response_data["experience"]);
      this.localform.controls["social_share"].setValue(this.response_data["social_share"]);
      this.localform.controls["view_details"].setValue(this.response_data["view_details"]);
      this.localform.controls["view_applications"].setValue(this.response_data["view_applications"]);
      this.localform.controls["unpublish_details"].setValue(this.response_data["unpublish_details"]);
      this.localform.controls["publish_details"].setValue(this.response_data["publish_details"]);
      this.localform.controls["status_id"].setValue(this.response_data["status_id"]);
      this.localform.controls["dummyunpublished"].setValue(this.response_data["dummyunpublished"]);
      this.localform.controls["job_category_id"].setValue(this.response_data["job_category_id"]);
      this.localform.controls["job_role_id"].setValue(this.response_data["job_role_id"]);
      this.localform.controls["min_experience"].setValue(this.response_data["min_experience"]);
      this.localform.controls["max_experience"].setValue(this.response_data["max_experience"]);
      this.localform.controls["is_remote_job"].setValue(this.response_data["is_remote_job"]);
      this.localform.controls["job_location"].setValue(this.response_data["job_location"]);
      this.localform.controls["job_latitude_location"].setValue(this.response_data["job_latitude_location"]);
      this.localform.controls["job_longitude_location"].setValue(this.response_data["job_longitude_location"]);
      this.localform.controls["job_location_city"].setValue(this.response_data["job_location_city"]);
      this.localform.controls["job_location_state"].setValue(this.response_data["job_location_state"]);
      this.localform.controls["job_location_country"].setValue(this.response_data["job_location_country"]);
      this.localform.controls["min_job_salary"].setValue(this.response_data["min_job_salary"]);
      this.localform.controls["max_job_salary"].setValue(this.response_data["max_job_salary"]);
      this.localform.controls["education_requirement"].setValue(this.response_data["education_requirement"]);
      this.localform.controls["num_of_openings"].setValue(this.response_data["num_of_openings"]);
      this.localform.controls["id"].setValue(this.response_data["id"]);
      this.localform.controls["num_of_applications"].setValue(this.response_data["num_of_applications"]);
      this.localform.controls["job_description"].setValue(this.response_data["job_description"]);
      this.localform.controls["job_description_clean"].setValue(this.response_data["job_description_clean"]);
      this.localform.controls["attachments"] = new FormArray([]);
      if (this.response_data["attachments"] !== undefined) {
        this.response_data["attachments"].forEach((node) => {
          this.addNode_attachments(node);
        });
      }

      this.localform.controls["screening_questions"] = new FormArray([]);
      if (this.response_data["screening_questions"] !== undefined) {
        this.response_data["screening_questions"].forEach((node) => {
          this.addNode_screening_questions(node);
        });
      }
    });
  }

  response_category_id_options: any;
  load_category_id_options() {
    return this.apiService.post_api("jobportal", "getjobcategoriesoptions");
  }

  response_role_id_options: any;
  load_role_id_options() {
    return this.apiService.post_api("jobportal", "getjobrolesoptions");
  }
}
