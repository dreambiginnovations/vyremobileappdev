import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { CourseViewComponent } from "./course_view.component";

export const routes: Routes = [
  {
    path: "",
    component: CourseViewComponent,
  },
];

@NgModule({
  declarations: [CourseViewComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule],
})
export class CourseViewModule {
  static components = {
    CourseViewComponent: CourseViewComponent,
  };
}
