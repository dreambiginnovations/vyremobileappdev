import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__course_view",
  templateUrl: "./course_view.template.html",
  styleUrls: ["./course_view.component.scss"],
})
export class CourseViewComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  id: any;
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, public templateutilsService: TemplateutilsService, private toastService: ToastService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];

    this.initializeFormGroup();
    this.load_data();

    this.apiService.post_api("jobportal", "registerview", { content_category_id: this.id, content_type: "COURSE" }).subscribe((viewres) => {});
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("id", new FormControl(""));
    this.localform.addControl("content_category_name", new FormControl(""));
    this.localform.addControl("course_image_dummy_id", new FormControl(""));
    this.localform.addControl("view_lessons_button", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("content_category_short_description", new FormControl(""));
    this.localform.addControl("content_category_full_description", new FormControl(""));
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getcourse", { id: this.id }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["id"].setValue(this.response_data["id"]);
      this.localform.controls["content_category_name"].setValue(this.response_data["content_category_name"]);
      this.localform.controls["course_image_dummy_id"].setValue(this.response_data["course_image_dummy_id"]);
      this.localform.controls["view_lessons_button"].setValue(this.response_data["view_lessons_button"]);
      this.localform.controls["content_category_short_description"].setValue(this.response_data["content_category_short_description"]);
      this.localform.controls["content_category_full_description"].setValue(this.response_data["content_category_full_description"]);
    });
  }
}
