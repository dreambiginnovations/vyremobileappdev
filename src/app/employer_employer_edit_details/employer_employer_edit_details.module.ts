import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { FileUploaderModule } from "../common/fileuploader/fileuploader.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { MapLocationModule } from "../common/maplocation/maplocation.module";
import { CustomTextAreaModule } from "../common/customtextarea/customtextarea.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerEmployerEditDetailsComponent } from "./employer_employer_edit_details.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerEmployerEditDetailsComponent,
  },
];

@NgModule({
  declarations: [EmployerEmployerEditDetailsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, FileUploaderModule, VideoRecordModule, MapLocationModule, CustomTextAreaModule],
})
export class EmployerEmployerEditDetailsModule {
  static components = {
    EmployerEmployerEditDetailsComponent: EmployerEmployerEditDetailsComponent,
  };
}
