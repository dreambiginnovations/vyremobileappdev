import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { TemplateutilsService } from "../services/templateutils.service";
import { ApiService } from "../services/api.service";
import { AppstateService } from "../services/appstate.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__navbarexpandedemployer",
  templateUrl: "./navbarexpandedemployer.template.html",
  styleUrls: ["./navbarexpandedemployer.component.scss"],
})
export class NavbarexpandedemployerComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  constructor(public templateutilsService: TemplateutilsService, public apiService: ApiService, public appstateService: AppstateService, private commonutilsService: CommonutilsService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("photo_id", new FormControl("", Validators.required));
    this.localform.addControl("dashboard", new FormControl(""));
    this.localform.addControl("myprofile", new FormControl(""));
    this.localform.addControl("myjobs", new FormControl(""));
    this.localform.addControl("myapplications", new FormControl(""));
    this.localform.addControl("findjobseekers", new FormControl(""));
    this.localform.addControl("mynotifications", new FormControl(""));
    this.localform.addControl("logout", new FormControl("", this.skipsubmitValidator));
  }

  collapseMenu() {
    this.templateutilsService.dismissDialogs();
  }
  logout() {
    this.apiService.post_api("common", "logout").subscribe((res) => {
      this.templateutilsService.dismissAndNavigate("login_signup");
      this.apiService.post_api("backend", "getAppState").subscribe();
    });
  }
  response_data: any;
  showManageTeam = false;
  load_data() {
    this.apiService.post_api("jobportal", "getemployeremployer").subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;
      if(!this.response_data.is_user_employee) {
        this.showManageTeam = true;
      }

      this.localform.controls["photo_id"].setValue(this.response_data["photo_id"]);
      this.localform.controls["dashboard"].setValue(this.response_data["dashboard"]);
      this.localform.controls["myprofile"].setValue(this.response_data["myprofile"]);
      this.localform.controls["myjobs"].setValue(this.response_data["myjobs"]);
      this.localform.controls["myapplications"].setValue(this.response_data["myapplications"]);
      this.localform.controls["findjobseekers"].setValue(this.response_data["findjobseekers"]);
      this.localform.controls["mynotifications"].setValue(this.response_data["mynotifications"]);
      this.localform.controls["logout"].setValue(this.response_data["logout"]);
    });
  }
}
