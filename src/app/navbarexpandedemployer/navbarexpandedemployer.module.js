"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var imagecropper_module_1 = require("../common/imagecropper/imagecropper.module");
var pipes_module_1 = require("../common/pipes/pipes.module");
var navbarexpandedemployer_component_1 = require("./navbarexpandedemployer.component");
exports.routes = [
    {
        path: "",
        component: navbarexpandedemployer_component_1.NavbarexpandedemployerComponent
    },
];
var NavbarexpandedemployerModule = /** @class */ (function () {
    function NavbarexpandedemployerModule() {
    }
    NavbarexpandedemployerModule.components = {
        NavbarexpandedemployerComponent: navbarexpandedemployer_component_1.NavbarexpandedemployerComponent
    };
    NavbarexpandedemployerModule = __decorate([
        core_1.NgModule({
            declarations: [navbarexpandedemployer_component_1.NavbarexpandedemployerComponent],
            imports: [common_1.CommonModule, pipes_module_1.PipesModule, router_1.RouterModule.forChild(exports.routes), forms_1.ReactiveFormsModule, imagecropper_module_1.ImgCropperModule]
        })
    ], NavbarexpandedemployerModule);
    return NavbarexpandedemployerModule;
}());
exports.NavbarexpandedemployerModule = NavbarexpandedemployerModule;
