"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var NavbarexpandedemployerComponent = /** @class */ (function () {
    function NavbarexpandedemployerComponent(templateutilsService, apiService, appstateService, commonutilsService, toastService) {
        this.templateutilsService = templateutilsService;
        this.apiService = apiService;
        this.appstateService = appstateService;
        this.commonutilsService = commonutilsService;
        this.toastService = toastService;
        this.hostclasses = "container px-0 viewfor__DETAILS";
        this.showManageTeam = false;
    }
    NavbarexpandedemployerComponent.prototype.ngOnInit = function () {
        this.initializeFormGroup();
        this.load_data();
    };
    NavbarexpandedemployerComponent.prototype.skipsubmitValidator = function (control) {
        return { skip: true };
    };
    NavbarexpandedemployerComponent.prototype.processcontrolforskip = function (control) {
        var newControl;
        if (control instanceof forms_1.FormArray) {
            newControl = this.processformgroupforskip(control, true);
        }
        else if (control instanceof forms_1.FormGroup) {
            newControl = this.processformgroupforskip(control, false);
        }
        else {
            newControl = control;
        }
        return newControl;
    };
    NavbarexpandedemployerComponent.prototype.processformgroupforskip = function (formgroup, controlsArray) {
        var _a, _b;
        var returnFormGroup;
        if (controlsArray) {
            returnFormGroup = new forms_1.FormArray([]);
            for (var count = 0; count < formgroup.controls.length; count++) {
                var control = formgroup.controls[count];
                if (((_a = control.errors) === null || _a === void 0 ? void 0 : _a.skip) !== true) {
                    var newControl = this.processcontrolforskip(control);
                    returnFormGroup.push(newControl);
                }
            }
        }
        else {
            returnFormGroup = new forms_1.FormGroup({});
            for (var controlName in formgroup.controls) {
                var control = formgroup.controls[controlName];
                if (((_b = control.errors) === null || _b === void 0 ? void 0 : _b.skip) !== true) {
                    var newControl = this.processcontrolforskip(control);
                    returnFormGroup.addControl(controlName, newControl);
                }
            }
        }
        return returnFormGroup;
    };
    NavbarexpandedemployerComponent.prototype.initializeFormGroup = function () {
        this.localform = new forms_1.FormGroup({});
        this.localform.addControl("photo_id", new forms_1.FormControl("", forms_1.Validators.required));
        this.localform.addControl("dashboard", new forms_1.FormControl(""));
        this.localform.addControl("myprofile", new forms_1.FormControl(""));
        this.localform.addControl("myjobs", new forms_1.FormControl(""));
        this.localform.addControl("myapplications", new forms_1.FormControl(""));
        this.localform.addControl("findjobseekers", new forms_1.FormControl(""));
        this.localform.addControl("mynotifications", new forms_1.FormControl(""));
        this.localform.addControl("logout", new forms_1.FormControl("", this.skipsubmitValidator));
    };
    NavbarexpandedemployerComponent.prototype.collapseMenu = function () {
        this.templateutilsService.dismissDialogs();
    };
    NavbarexpandedemployerComponent.prototype.logout = function () {
        var _this = this;
        this.apiService.post_api("common", "logout").subscribe(function (res) {
            _this.templateutilsService.dismissAndNavigate("login_signup");
            _this.apiService.post_api("backend", "getAppState").subscribe();
        });
    };
    NavbarexpandedemployerComponent.prototype.load_data = function () {
        var _this = this;
        this.apiService.post_api("jobportal", "getemployeremployer").subscribe(function (res) {
            if (res.status === "FALSE" || res.status === false) {
                _this.toastService.showError("Error", res.message);
                _this.commonutilsService.processResponse(res);
                return;
            }
            _this.response_data = res;
            if (!_this.response_data.is_user_employee) {
                this.showManageTeam = true;
            }
            _this.localform.controls["photo_id"].setValue(_this.response_data["photo_id"]);
            _this.localform.controls["dashboard"].setValue(_this.response_data["dashboard"]);
            _this.localform.controls["myprofile"].setValue(_this.response_data["myprofile"]);
            _this.localform.controls["myjobs"].setValue(_this.response_data["myjobs"]);
            _this.localform.controls["myapplications"].setValue(_this.response_data["myapplications"]);
            _this.localform.controls["findjobseekers"].setValue(_this.response_data["findjobseekers"]);
            _this.localform.controls["mynotifications"].setValue(_this.response_data["mynotifications"]);
            _this.localform.controls["logout"].setValue(_this.response_data["logout"]);
        });
    };
    __decorate([
        core_1.HostBinding("class")
    ], NavbarexpandedemployerComponent.prototype, "hostclasses");
    NavbarexpandedemployerComponent = __decorate([
        core_1.Component({
            selector: ".component__navbarexpandedemployer",
            templateUrl: "./navbarexpandedemployer.template.html",
            styleUrls: ["./navbarexpandedemployer.component.scss"]
        })
    ], NavbarexpandedemployerComponent);
    return NavbarexpandedemployerComponent;
}());
exports.NavbarexpandedemployerComponent = NavbarexpandedemployerComponent;
