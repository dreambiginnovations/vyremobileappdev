import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { NavbarexpandedemployerComponent } from "./navbarexpandedemployer.component";

export const routes: Routes = [
  {
    path: "",
    component: NavbarexpandedemployerComponent,
  },
];

@NgModule({
  declarations: [NavbarexpandedemployerComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule],
})
export class NavbarexpandedemployerModule {
  static components = {
    NavbarexpandedemployerComponent: NavbarexpandedemployerComponent,
  };
}
