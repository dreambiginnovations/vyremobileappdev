import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { DialogService } from "../services/dialog.service";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__khoj_kyc_pending",
  templateUrl: "./khoj_kyc_pending.template.html",
  styleUrls: ["./khoj_kyc_pending.component.scss"],
})
export class KhojKycPendingComponent implements OnInit {
  @HostBinding("class") hostclasses = "bg-white px-0 container viewfor__EDIT";
  constructor(private dialogService: DialogService, private apiService: ApiService, private commonutilsService: CommonutilsService, public templateutilsService: TemplateutilsService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("khoj_intro_question", new FormControl(""));
    this.localform.addControl("khoj_intro_buyers", new FormControl(""));
    this.localform.addControl("khoj_intro_makers", new FormControl(""));
    this.localform.addControl("khoj_intro_sellers", new FormControl(""));
    this.localform.addControl("khoj_intro_influencers", new FormControl(""));
    this.localform.addControl("about_video_id", new FormControl(""));
  }

  success_state = { route: "login_signup_otp" };
  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
      localStorage.removeItem("mobile");
      localStorage.setItem("mobile", this.localform.value["mobile"]);
      this.submitFormAPI();
    }
  }

  submitformres: any;
  submitFormAPI() {
    this.apiService.post_api("jobportal", "generateotp", this.localform.value).subscribe((res) => {
      this.submitformres = res;
      if (this.submitformres.message) {
        if (this.submitformres.status == "TRUE") {
          this.toastService.showSuccess("Success", this.submitformres.message);
        } else if (this.submitformres.status == "FALSE") {
          this.toastService.showError("Error", this.submitformres.message);
        } else {
          this.toastService.showSuccess("Success", this.submitformres.message);
        }
      }
      if (res.status == "TRUE" || res.status === true) {
        let route;
        let routeParams;
        [route, routeParams] = this.commonutilsService.processSuccessState(this.success_state);
        this.templateutilsService.navigateToRoute(route, routeParams);
      } else {
      }
    });
  }

  logout() {
    this.apiService.post_api("khoj", "logout").subscribe((res) => {
      this.templateutilsService.navigateToRoute("/");
    });
  }
  response_data: any;
  load_data() {
    this.apiService.post_api("khoj", "getdataforwelcomescreen").subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["khoj_intro_question"].setValue(this.response_data["khoj_intro_question"]);
      this.localform.controls["khoj_intro_buyers"].setValue(this.response_data["khoj_intro_buyers"]);
      this.localform.controls["khoj_intro_makers"].setValue(this.response_data["khoj_intro_makers"]);
      this.localform.controls["khoj_intro_sellers"].setValue(this.response_data["khoj_intro_sellers"]);
      this.localform.controls["khoj_intro_influencers"].setValue(this.response_data["khoj_intro_influencers"]);
      this.localform.controls["about_video_id"].setValue(this.response_data["about_video_id"]);
    });
  }
}
