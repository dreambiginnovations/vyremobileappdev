import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { KhojKycPendingComponent } from "./khoj_kyc_pending.component";

export const routes: Routes = [
  {
    path: "",
    component: KhojKycPendingComponent,
  },
];

@NgModule({
  declarations: [KhojKycPendingComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, VideoRecordModule],
})
export class KhojKycPendingModule {
  static components = {
    KhojKycPendingComponent: KhojKycPendingComponent,
  };
}
