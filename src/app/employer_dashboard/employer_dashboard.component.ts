import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { AppstateService } from "../services/appstate.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_dashboard",
  templateUrl: "./employer_dashboard.template.html",
  styleUrls: ["./employer_dashboard.component.scss"],
})
export class EmployerDashboardComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  constructor(public appstateService: AppstateService, public templateutilsService: TemplateutilsService, private apiService: ApiService, private commonutilsService: CommonutilsService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("welcomemessage", new FormControl(""));
    this.localform.addControl("notifications", new FormArray([]));
    this.localform.addControl("select", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("jobsposted", new FormControl(""));
    this.localform.addControl("jobspostedactive", new FormControl(""));
    this.localform.addControl("jobspostedinactive", new FormControl(""));
    this.localform.addControl("applications", new FormControl(""));
    this.localform.addControl("unreadapplications", new FormControl(""));
    this.localform.addControl("selectedapplications", new FormControl(""));
    this.localform.addControl("onholdapplications", new FormControl(""));
    this.localform.addControl("rejectedapplications", new FormControl(""));
    this.localform.addControl("jobviewsinlist", new FormControl(""));
    this.localform.addControl("jobviewsindetails", new FormControl(""));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  markNotification(formgroup, notification_read_status, status_id) {
    if (status_id === 0 && !confirm("Are you sure?")) {
      return;
    }
    formgroup.controls["notification_read_status"].setValue(notification_read_status);
    formgroup.controls["status_id"].setValue(status_id);
    this.apiService.post_api("jobportal", "updatenotification", { id: formgroup.value["id"], notification_read_status: formgroup.value["notification_read_status"], status_id: formgroup.value["status_id"] }).subscribe((res) => {
      this.ngOnInit();
    });
  }
  evalCode(code) {
    console.log("eval", code);
    eval(code);
  }
  addNode_notifications(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("created_at", new FormControl(""));
    nodeControl.addControl("notification_title", new FormControl(""));
    nodeControl.addControl("notification_text", new FormControl(""));
    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("status_id", new FormControl(""));
    nodeControl.addControl("notification_read_status", new FormControl(""));
    nodeControl.addControl("notification_action_onclick", new FormControl(""));
    nodeControl.addControl("select", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("onhold", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("reject", new FormControl("", this.skipsubmitValidator));
    nodeControl.addControl("reject", new FormControl("", this.skipsubmitValidator));

    if (nodeData !== undefined) {
      nodeControl.controls["created_at"].setValue(nodeData["created_at"]);
      nodeControl.controls["notification_title"].setValue(nodeData["notification_title"]);
      nodeControl.controls["notification_text"].setValue(nodeData["notification_text"]);
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["status_id"].setValue(nodeData["status_id"]);
      nodeControl.controls["notification_read_status"].setValue(nodeData["notification_read_status"]);
      nodeControl.controls["notification_action_onclick"].setValue(nodeData["notification_action_onclick"]);
      nodeControl.controls["select"].setValue(nodeData["select"]);
      nodeControl.controls["onhold"].setValue(nodeData["onhold"]);
      nodeControl.controls["reject"].setValue(nodeData["reject"]);
      nodeControl.controls["reject"].setValue(nodeData["reject"]);
    }
    (<FormArray>this.localform.controls["notifications"]).push(nodeControl);
  }

  removeNode_notifications(index) {
    (<FormArray>this.localform.controls["notifications"]).removeAt(index);
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getemployerdashboarddata").subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["welcomemessage"].setValue(this.response_data["welcomemessage"]);
      this.localform.controls["notifications"] = new FormArray([]);
      if (this.response_data["notifications"] !== undefined) {
        this.response_data["notifications"].forEach((node) => {
          this.addNode_notifications(node);
        });
      }

      this.localform.controls["select"].setValue(this.response_data["select"]);
      this.localform.controls["jobsposted"].setValue(this.response_data["jobsposted"]);
      this.localform.controls["jobspostedactive"].setValue(this.response_data["jobspostedactive"]);
      this.localform.controls["jobspostedinactive"].setValue(this.response_data["jobspostedinactive"]);
      this.localform.controls["applications"].setValue(this.response_data["applications"]);
      this.localform.controls["unreadapplications"].setValue(this.response_data["unreadapplications"]);
      this.localform.controls["selectedapplications"].setValue(this.response_data["selectedapplications"]);
      this.localform.controls["onholdapplications"].setValue(this.response_data["onholdapplications"]);
      this.localform.controls["rejectedapplications"].setValue(this.response_data["rejectedapplications"]);
      this.localform.controls["jobviewsinlist"].setValue(this.response_data["jobviewsinlist"]);
      this.localform.controls["jobviewsindetails"].setValue(this.response_data["jobviewsindetails"]);
    });
  }
}
