import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerDashboardComponent } from "./employer_dashboard.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerDashboardComponent,
  },
];

@NgModule({
  declarations: [EmployerDashboardComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class EmployerDashboardModule {
  static components = {
    EmployerDashboardComponent: EmployerDashboardComponent,
  };
}
