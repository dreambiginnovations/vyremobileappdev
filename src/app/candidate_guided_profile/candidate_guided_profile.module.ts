import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { VjsPlayerModule } from "../common/videoplay/vjs-player.module";
import { CandidateGuidedProfileComponent } from "./candidate_guided_profile.component";
import { TejaVideoRecordModule } from "../common/tejavideorecord/tejavideorecord.module";

export const routes: Routes = [
  {
    path: "",
    component: CandidateGuidedProfileComponent,
  },
  {
    path: ":step",
    component: CandidateGuidedProfileComponent,
  },
];

@NgModule({
  declarations: [CandidateGuidedProfileComponent],
  imports: [CommonModule, RouterModule.forChild(routes), ReactiveFormsModule, FormsModule, VjsPlayerModule, TejaVideoRecordModule]
})
export class CandidateGuidedProfileModule {
  static components = {
    CandidateGuidedProfileComponent: CandidateGuidedProfileComponent,
  };
}
