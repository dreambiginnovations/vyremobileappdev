import { Component, OnInit, OnDestroy, HostBinding, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { ApiService } from "../services/api.service";
import { AppstateService } from "../services/appstate.service";
import { DialogService } from "../services/dialog.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";
import { CommunicationService } from "../services/communication.service";
import { CameraService } from '../services/camera.service';
import { EnvService } from "../services/env.service";

import videojs from "video.js";
import "videojs-playlist";
import * as adapter from "webrtc-adapter/out/adapter_no_global.js";
import * as RecordRTC from "recordrtc";
import * as Record from "videojs-record/dist/videojs.record.js";
import captureVideoFrame from "capture-video-frame";
import 'videojs-watermark';


@Component({
  selector: ".component__candidate_guided_profile",
  templateUrl: "./candidate_guided_profile.template.html",
  styleUrls: ["./candidate_guided_profile.component.scss"],
})
export class CandidateGuidedProfileComponent implements OnInit, OnDestroy {
  @HostBinding("class") hostclasses = "nonstandardiconcontainer container px-0 viewfor__DETAILS";
  step: any = "1.1";
  steptype: string;
  stepnumber: number;
  constructor(
    private apiService: ApiService,
    private appstateService: AppstateService,
    public dialogService: DialogService,
    public templateutilsService: TemplateutilsService,
    private toastService: ToastService,
    private route: ActivatedRoute,
    private communicationService: CommunicationService,
    private cameraService: CameraService,
    public envService: EnvService,
    private router: Router
  ) {
    console.log("Getting already uploaded videos");
    this.getAlreadyUploadedVideosOfUser();
  }
  checkIfReload() {
    if(this.continueRecordingObj===undefined && (this.step!=="recordingselection" && this.step!=="recordingdescription_0" && this.step!=="recording_0") && this.appstateService.guided_recording===undefined) {
      this.toastService.showSuccess("Reload detected", "Please restart recording");
      this.gotostep("recordingselection");
      return true;
    } else if(this.continueRecordingObj!==undefined) {
      return false;
    }
    return false;
  }
  doneCheckingAlreadyUploaded = false;
  alreadyUploadedVideos: any;
  ngOnInit() {
    console.log("got into init for", this.route.snapshot["_routerState"]["url"]);
    this.route.params.subscribe((params) => {
      this.steptype = undefined;
      this.showInitializeRecordingScreen = false;
      this.recordingStatus = this.RECORDING.UNINITIALIZED;
      //this.destroyAll();
      if(params.step) {this.step = params.step;}
      console.log("this.step", this.step);
      if(this.step=="1.9") {
        if(this.checkIfReload()) {return;}
        /*setTimeout(()=>{
          this.processFinalRecordingScreen();
        }, 500);*/
      } else if(this.step.includes("recordingselection")) {
        this.steptype = "recordingselection";
        this.processChooseCamerasScreen();
      } else if(this.step.includes("recordingdescription_")) {
        this.steptype = "recordingdescription";
        this.stepnumber = parseInt(this.step.replace("recordingdescription_", ""));
      } else if(this.step.includes("recording_")) {
        this.steptype = "recording";
        this.stepnumber = parseInt(this.step.replace("recording_", ""));
        if(this.checkIfReload()) {return;}
        this.recordingQuestion = this.partwiseQuestions[this.stepnumber].question;
        this.idx = this.step;
        this.scrollToTop();
        setTimeout(()=>{
          this.initializeRecorderOptions();
        }, 500);
        setTimeout(()=>{
          this.initializeVideoJS();
        }, 1500);
        var audioFileURL = this.envService.values.staticmediaurl + "guided_profile_questions/" + this.partwiseQuestions[this.stepnumber].question_audio_url;
        this.playAudioAfterTimer(audioFileURL);
      }
    });
  }
  ngOnDestroy(): void {
    console.log("got into destroy for", this.route.snapshot["_routerState"]["url"]);
    //this.destroyAll();
  }
  destroyAll() {
    this.showInitializeRecordingScreen = false;
    if (this.player) {
      this.player.dispose();
    }
    if (this.previewPlayer) {
      this.previewPlayer.dispose();
    }
    this.destroyAudio();
    if(this.initializeRecordingTimerInterval) {
      clearInterval(this.initializeRecordingTimerInterval);
    }
  }
  destroyAudio() {
    if(this.audioplay) {
      this.audioplay.pause();
    }
  }
  destroyForStep() {
    if(this.steptype == "recording") {
      console.log("Disposing this.player");
      if(this.player) {
        this.player.record().reset();
        setTimeout(()=>{
          this.player.dispose();
        }, 500);  
      }
    }
  }
  scrollToTop(): void {
    window.scroll(0, 0);
  }
  initializeRecordingTimerInterval: any;
  audioplay: any;
  processInitializeRecordingScreen() {
    this.showInitializeRecordingScreen = true;
    this.recordingStatus = this.RECORDING.UNINITIALIZED;
    this.initializeRecordingTimerInterval = setInterval(()=>{
      if(this.initializeRecordingTimerNum>1) {--this.initializeRecordingTimerNum;}
      else {
        this.audioplay.play();
      }
    }, 1000);
  }
  playAudioAfterTimer(audioFileURL) {
    this.audioplay = new Audio(audioFileURL);
    this.audioplay.addEventListener('canplaythrough', ()=>{
      console.log('Audio file is loaded and ready to play');
      this.initializeRecordingTimerNum = 5;
      this.processInitializeRecordingScreen();
      setTimeout(()=>{
        this.audioplay.play();
      }, 4000);
    });
    this.audioplay.addEventListener('ended', ()=>{
      console.log('Audio file ended playing');
      clearInterval(this.initializeRecordingTimerInterval);
      this.showInitializeRecordingScreen = false;
      this.startRecording();
    });
    this.audioplay.addEventListener('error', ()=>{
      console.error('Error loading audio file');
      this.initializeRecordingTimerNum = 5;
      this.processInitializeRecordingScreen();
    });
    this.audioplay.load();
  }
  gotostep(value) {
    this.destroyForStep();
    this.router.navigate(['/candidate/guided_profile', value]);
  }
  @HostListener('window:popstate', ['$event'])
  onPopState(event) {
    console.log('Back button pressed');
    if(this.player!==undefined) {videojs(this.player).dispose();}
    if(this.recordingStatus === this.RECORDING.UNINITIALIZED) {videojs(this.player).dispose();}
  }
  cameraList: any;
  chosenDeviceId: any = '';
  async processChooseCamerasScreen() {
    this.cameraList = null;
    if ('mediaDevices' in navigator && 'getUserMedia' in navigator.mediaDevices) {
      this.cameraService.getCameraList().then((value)=>{
        this.cameraList = value;
        console.log("this.cameraList inside then", this.cameraList);
        if(this.cameraList.length===0) {
          this.toastService.showError("No Cameras Found", "There are no cameras found on your device.");      
        } else if(this.cameraList.length===1) {
          this.chosenDeviceId = this.cameraList[0].deviceId;
          this.gotorecordingsteponprogress();
        }
      });
    } else {
      this.toastService.showError("No Cameras Found", "There are no cameras found on your device.");      
    }
  }
  gotorecordingsteponprogress() {
    if(this.continueRecordingObj===undefined) {
      this.gotostep('recordingdescription_0');
    } else {
      this.gotostep('recordingdescription_' + this.alreadyUploadedVideos.length);
    }
  }
  
  recordingQuestion: string;
  showInitializeRecordingScreen = false;
  initializeRecordingTimerNum: number;
  idx: string;
  gotorecordingstep(value) {
    this.step = "recording_" + value;
    this.showCanvas = false;
    this.router.navigate(['/candidate/guided_profile', this.step]);
    this.recordingQuestion = this.partwiseQuestions[value].question;
  }
  
  recordingStatus: number;
  initializeVideoJS() {
    this.player = videojs(document.getElementById(this.idx), this.config, () => {
      console.log("player ready! id", this.idx);
    });
    console.log("this.player", this.player);
    setTimeout(() => {
      console.log("Initializing device start...");
      this.player.record().getDevice();
      console.log("Initializing device finish...");
    }, 300);

    this.player.on("deviceReady", () => {
      console.log("device is ready!");
    });

    this.player.on("startRecord", () => {
      console.log("started recording!");
    });

    this.player.on("finishRecord", () => {
      console.log("finished recording: ", this.player.recordedData);
      this.recordingStatus = this.RECORDING.COMPLETED;
    });

    this.player.on("ended", () => {
      console.log("firing ended");
      this.recordingStatus = this.RECORDING.STOPPED;
    });

    this.player.on("error", (error) => {
      console.warn("error", error);
      this.processRecordingError(error, "");
    });

    this.player.on("deviceError", () => {
      console.error("device error:", this.player.deviceErrorCode, this.player);
      this.processRecordingError(undefined, "device");
    });
  }
  
  recordingInterval: any;
  recordingTimeInSeconds: number;
  maxRecordingTimeInSeconds = 60;
  startRecording() {
    console.log("Initializing actual recording...");
    if(this.steptype!=='recording') {return;}
    if(this.recordingInterval) {clearInterval(this.recordingInterval);}
    this.recordingTimeInSeconds = this.maxRecordingTimeInSeconds;
    this.recordingInterval = setInterval(() => {
      this.recordingTimeInSeconds--;
      if(this.recordingTimeInSeconds<=0) {
        clearInterval(this.recordingInterval);
        this.stopRecording();
      }
    }, 1000);
    this.player.record().start();
    this.recordingStatus = this.RECORDING.INPROGRESS;
  }
  useRecording() {
    const videoPlayer:any = document.getElementById(this.idx).getElementsByTagName('video')[0];
    const stream = videoPlayer.captureStream();
    this.cameraService.stopStream(stream);
    
    //this.showCanvas = true;
    clearInterval(this.recordingInterval);
    setTimeout(() => {
      //this.captureVideoFrame();
      if(this.appstateService.guided_recording===undefined) {
        this.appstateService.guided_recording = {};
      }
      this.appstateService.guided_recording[this.step] = { video: this.player.recordedData, seconds: (this.maxRecordingTimeInSeconds-this.recordingTimeInSeconds), question: this.recordingQuestion };
      console.log("this.stepnumber, this.partwiseQuestions.length", this.stepnumber, this.partwiseQuestions.length);
      var communication_obj = {};
      if(this.continueRecordingObj!==undefined) {
        communication_obj = this.continueRecordingObj;
      }
      this.communicationService.broadcastCommunication({
        context: { parentid: "candidate_profile_guided_video_upload", communicationid: this.stepnumber },
        obj: communication_obj,
      });
      if(this.stepnumber < (this.partwiseQuestions.length-1)) {
        this.gotostep('recordingdescription_' + (this.stepnumber + 1));
      } else {this.gotostep('1.9');}
      this.recordingStatus = this.RECORDING.UNINITIALIZED;
    }, 200);
  }
  capturedFrame: any;
  showCanvas = false;
  captureVideoFrame(idString?) {
    if (idString === undefined) {
      idString = this.idx;
    }
    var capturedFrameObj = captureVideoFrame(idString + "_html5_api", "png");
    this.capturedFrame = capturedFrameObj.dataUri;
  }
  stopRecording() {
    console.log("inside stopRecording()");
    this.player.record().stop();
    this.recordingStatus = this.RECORDING.COMPLETED;
    clearInterval(this.recordingInterval);
    setTimeout(()=>{
      this.playRecordedVideo();
      setTimeout(()=>{
        this.pauseRecordedVideo();
      }, 500);
    }, 500);
  }
  
  parentId: string | number;
  finalRecordedData = {};
  recordingErrorMessage: string;
  private config: any;
  private player: any;
  private plugin: any;
  
  switchCamera() {}
  
  restartRecording() {
    clearInterval(this.recordingInterval);
    this.stopRecording();
    this.recordingStatus = this.RECORDING.UNINITIALIZED;
    this.player.record().reset();
    setTimeout(() => {
      this.startRecording();
    }, 1000);
  }
  pauseRecording() {
    this.player.record().pause();
    this.recordingStatus = this.RECORDING.PAUSED;
  }
  unpauseRecording() {
    this.player.record().resume();
    this.recordingStatus = this.RECORDING.INPROGRESS;
  }
  

  playRecordedVideo() {
    this.player.play();
    this.recordingStatus = this.RECORDING.PLAYING;
  }
  pauseRecordedVideo() {
    this.player.pause();
    this.recordingStatus = this.RECORDING.PLAYINGPAUSED;
  }
  
  
  
  
  
  RECORDING = {
    UNINITIALIZED: 0,
    INITIALIZING: 1,
    INPROGRESS: 2,
    PAUSED: 3,
    STOPPED: 4,
    PLAYING: 5,
    PLAYINGPAUSED: 6,
    COMPLETED: 7
  };
  initializeRecorderOptions() {
    var video_parent = document.getElementById("parent_" + this.idx);
    var video = document.createElement('video');
    video.id = this.idx;
    video.className="video-js vjs-default-skin shadow";
    video.preload="auto";
    video.setAttribute("playsinline","true");
    video_parent.appendChild(video);
    
    this.plugin = Record;
    this.config = {
      controls: false,
      autoplay: false,
      fluid: false,
      loop: false,
      width: window.innerWidth,
      height: window.innerHeight*0.75,
      bigPlayButton: false,
      controlBar: {
        volumePanel: false,
        fullscreenToggle: false,
        pictureInPictureToggle: false,
        progressControl: false,
      },
      plugins: {
        record: {
          audio: true,
          screen: true,
          videoMimeType: 'video/webm;codecs=vp8',
          maxLength: this.maxRecordingTimeInSeconds,
          video: {
            width: 320,
            height: 240,
            deviceId: this.chosenDeviceId
          }
        }
      }
    };
  }
  processRecordingError(errorObj, errorType) {
    if (errorType == "device") {
      if (this.player.deviceErrorCode != undefined && this.player.deviceErrorCode.message.toLowerCase().includes("permission")) {
        this.recordingErrorMessage = "ERROR! Your permissions to access camera and microphone are turned off. Please turn on the permission. If having trouble, please reload/reinstall the app to get the permissions screen.";
      } else {
        this.recordingErrorMessage = this.player.deviceErrorCode;
      }
    } else {
      this.recordingErrorMessage = "ERROR! " + errorObj;
    }
  }
  previewPlayer: any;
  createPreviewVideo() {
    console.log("preview video version", 1);
    var currentRunningVideo:any = document.getElementById("previewvideo");
    if (this.previewPlayer && currentRunningVideo) {
      const videoPlayer:any = document.getElementById('previewvideo').getElementsByTagName('video')[0];
      const stream2 = videoPlayer.captureStream();
      this.cameraService.stopStream(stream2);
      this.previewPlayer.dispose();
    }
    if(this.chosenDeviceId==='') {return;}
    var finalParent = document.getElementById("previewvideocontainer");
    var video = document.createElement('video');
    video.id = "previewvideo";
    video.className = "video-js vjs-default-skin shadow";
    video.preload = "auto";
    video.setAttribute("playsinline", "true");
    finalParent.appendChild(video);
    setTimeout(()=>{
      const options = {
        width: 320,
        height: 240,
        controls: false,
        autoplay: true,
        fluid: true
      };
      this.previewPlayer = videojs(document.getElementById(video.id), options);
      /*navigator.mediaDevices.getUserMedia({
        video: {
          deviceId: { exact: this.chosenDeviceId }
        }
      })*/
      navigator.mediaDevices.getUserMedia({video: {deviceId: { exact: this.chosenDeviceId }}})
      .then(stream => {
        console.log('Camera stream obtained:', stream);
        video.srcObject = stream;
        video.onloadedmetadata = (e) => {
            video.play();
        };
      })
      .catch(error => {
        console.error('Error accessing camera:', error);
      });
    }, 500);
  }


  processFinalRecordingScreen() {
    var config = {
      controls: true,
      autoplay: false,
      fluid: false,
      loop: false,
      width: 320,
      height: 240,
      bigPlayButton: true,
      controlBar: {
        volumePanel: false,
        fullscreenToggle: false,
        pictureInPictureToggle: false,
        progressControl: false,
      }
    };
    var finalParentID = "finalrecording_parent_" + this.idx;
    var finalParent = document.getElementById(finalParentID);
    for(var count=0;count<this.partwiseQuestions.length;count++) {
      var finalParentIDCount = "finalrecording_container_" + count + "_" + this.idx;
      var finalParentIDCountDiv = document.createElement('div');
      finalParentIDCountDiv.id = finalParentIDCount;
      finalParent.appendChild(finalParentIDCountDiv);
      var video_parent = document.getElementById(finalParentID);
      var titleElement = document.createElement('div');
      titleElement.className="videotitle";
      titleElement.innerText = this.appstateService.guided_recording["recording_" + count]["question"];
      finalParentIDCountDiv.appendChild(titleElement);
      var video = document.createElement('video');
      video.id = "finalrecording_" + count + "_" + this.idx;
      video.className="video-js vjs-default-skin shadow";
      video.preload="auto";
      video.setAttribute("playsinline", "true");
      finalParentIDCountDiv.appendChild(video);
      this.initializeFinalPlayers(video.id, config, count);
    }
  }
  initializeFinalPlayers(videoID, config, count) {
    setTimeout(()=>{
      var finalplayer = videojs(document.getElementById(videoID), config, () => {
        console.log("final video player ready! id", videoID);
      });
      setTimeout(()=>{
        finalplayer.src({ type: this.appstateService.guided_recording["recording_" + count]["video"].type, src: URL.createObjectURL(this.appstateService.guided_recording["recording_" + count]["video"])});
        finalplayer.load();
      }, 500);
    }, 500);
  }
  getAlreadyUploadedVideosOfUser() {
    this.apiService.post_api("jobportal", "getcandidatecandidatevideos", {}).subscribe((res) => {
      this.doneCheckingAlreadyUploaded = true;
      if(res.videos) {
        this.alreadyUploadedVideos = res.videos;
      } else {
        this.alreadyUploadedVideos = undefined;
      }
      if(this.alreadyUploadedVideos===undefined || this.alreadyUploadedVideos.length===0 || this.alreadyUploadedVideos.length===4) {
        
      } else {
        this.gotostep("continueorrestart");
      }
    });
  }
  continueRecordingObj;
  continueRecording() {
    this.continueRecordingObj = {playlist_id: this.alreadyUploadedVideos[0].playlist_id};
    this.gotostep("recordingselection");
  }
  partwiseQuestions = [
    {
      "question": "Tell us about yourself - Your name, location and qualifications",
      "question_description_html": `
        <p>If you're telling someone about yourself, here’s how you can make it fun and interesting:</p>
        <ul class="guided-list">
          <li>Say Your Name with a Smile: "Hi, I'm [Your Name], and your interests"</li>
          <li>Tell Them About Where You Live: "I live in [Your City or Town]"</li>
          <li>Share Your qualifications: "I have passed my[ name of the class/degree] from [School or college name]"</li>
        </ul>
      `,
      "question_next_execute_function": "recording",
      "question_audio_url": "1.mp3"
    },
    {
      "question": "Do you have any experience?  If so in what field and how many years of experience do you have?",
      "question_description_html": `
        <p>When you're asked about your experience, here’s how you can shine:</p>
        <ul class="guided-list">
          <li>Experience Holders: "Yes, I bring [X years] of experience in [Field], where I’ve contributed to [Brief Highlight]. Excited to leverage that here!”</li>
          <li>Fresh Grads: "While I’m newly graduated from [Your School] with a degree in [Your Field],</li>
          <li>I’ve engaged in projects and internships like [Example], ready to apply and expand those skills!”</li>
        </ul>
        <p>This way, whether you’ve been in the game or you’re just stepping in, you’re showing you’re ready and equipped! 🚀</p>
      `,
      "question_next_execute_function": "recording",
      "question_audio_url": "2.mp3"
    },
    {
      "question": "Please share your achievements, Interests and hobbies",
      "question_description_html": `
        <p>Here’s a quick guide to answer this question</p>
        <ul class="guided-list">
          <li>Wins First: Mention your top win. “Won a national science fair with a cool eco-friendly project!”</li>
          <li>Passions Next: Talk about what excites you. “I love wildlife photography to save nature.”</li>
          <li>Hobbies Last: Add your fun activities. “For kicks, I go trekking and play with baking recipes.”</li>
        </ul>
        <p>It's your spotlight to share what makes you uniquely awesome! 🌈🚀</p>
      `,
      "question_next_execute_function": "recording",
      "question_audio_url": "3.mp3"
    },
    {
      "question": "What kind of a Job are you looking for and in which location",
      "question_description_html": `
        <p>Here are some Tips - A quick way to reply:</p>
        <ul class="guided-list">
          <li>Be Specific: Mention the job role you're aiming for. "I’m excited about roles in Sales  where my strengths in networking and relationships with people can be used  !"</li>
          <li>Share Your Why: Briefly say why you're interested in that role. "I love creating visuals that tell a story and connect with people."</li>
          <li>Location Preferences: Mention your preferred location, but be open. "I’m looking in New Delhi, but I’m also open to remote opportunities that allow me to work from anywhere!"</li>
        </ul>
        <p>This way, you show clarity about your job preferences and flexibility in location! 🌍✨</p>
      `,
      "question_next_execute_function": "recording",
      "question_audio_url": "4.mp3"
    }
  ];
  
}
