import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__candidate_candidate_edit_details_tempsession",
  templateUrl: "./candidate_candidate_edit_details_tempsession.template.html",
  styleUrls: ["./candidate_candidate_edit_details_tempsession.component.scss"],
})
export class CandidateCandidateEditDetailsTempsessionComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__EDIT";
  session_id: any;
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, public templateutilsService: TemplateutilsService, private toastService: ToastService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.session_id = this.route.snapshot.params["session_id"];

    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("id", new FormControl(""));
    this.localform.addControl("photo_id", new FormControl(""));
    this.localform.addControl("whatsthis", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("video_id", new FormControl(""));
    this.localform.addControl("name", new FormControl("", Validators.required));
    this.localform.addControl("changemobile", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("mobile", new FormControl("", Validators.compose([Validators.minLength(10), Validators.maxLength(10)])));
    this.localform.addControl("email", new FormControl(""));
    this.localform.addControl("job_category_id", new FormControl("", Validators.required));
    this.load_category_id_options().subscribe((res) => {
      this.response_category_id_options = res;
    });
    this.localform.addControl("job_role_id", new FormControl("", Validators.required));
    this.load_role_id_options().subscribe((res) => {
      this.response_role_id_options = res;
    });
    this.localform.addControl("preferred_job_location", new FormControl("", Validators.required));
    this.localform.addControl("preferred_job_latitude_location", new FormControl("", Validators.required));
    this.localform.addControl("preferred_job_longitude_location", new FormControl("", Validators.required));
    this.localform.addControl("preferred_job_location_city", new FormControl("", Validators.required));
    this.localform.addControl("preferred_job_location_state", new FormControl("", Validators.required));
    this.localform.addControl("preferred_job_location_country", new FormControl("", Validators.required));
    this.localform.addControl("current_location", new FormControl("", Validators.required));
    this.localform.addControl("current_latitude_location", new FormControl("", Validators.required));
    this.localform.addControl("current_longitude_location", new FormControl("", Validators.required));
    this.localform.addControl("current_location_city", new FormControl("", Validators.required));
    this.localform.addControl("current_location_state", new FormControl("", Validators.required));
    this.localform.addControl("current_location_country", new FormControl("", Validators.required));
    this.localform.addControl("about_yourself", new FormControl("", Validators.required));
    this.localform.addControl("current_salary", new FormControl("", Validators.compose([Validators.required, Validators.min(0)])));
    this.localform.addControl("dob", new FormControl("", Validators.required));
    this.localform.addControl("pan_card", new FormControl(""));
    this.localform.addControl("languages_known", new FormControl(""));
    this.localform.addControl("resume_id", new FormControl(""));
    this.localform.addControl("attachments", new FormArray([]));
    this.localform.addControl("id", new FormControl(""));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
      this.submitFormAPI();
    }
  }

  submitformres: any;
  submitFormAPI() {
    this.localform.value["session_id"] = this.session_id;
    this.apiService.post_api("jobportal", "updatecandidatecandidate", this.localform.value).subscribe((res) => {
      this.submitformres = res;
      if (this.submitformres.message) {
        if (this.submitformres.status == "TRUE") {
          this.toastService.showSuccess("Success", this.submitformres.message);
        } else if (this.submitformres.status == "FALSE") {
          this.toastService.showError("Error", this.submitformres.message);
        } else {
          this.toastService.showSuccess("Success", this.submitformres.message);
        }
      }
      if (res.status == "TRUE" || res.status === true) {
        this.commonutilsService.processResponse(res);
        this.ngOnInit();
      } else {
      }
    });
  }

  addNode_attachments(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("attachment_media_id", new FormControl("", Validators.required));

    if (nodeData !== undefined) {
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["attachment_media_id"].setValue(nodeData["attachment_media_id"]);
      nodeControl.controls["attachment_media_id"].valueChanges.subscribe((val) => {
        if (nodeControl.value["attachment_media_id"] !== val) {
          setTimeout(() => {
            this["submitFormAPI"]();
          }, 200);
        }
      });
    }
    (<FormArray>this.localform.controls["attachments"]).push(nodeControl);
  }

  removeNode_attachments(index) {
    (<FormArray>this.localform.controls["attachments"]).removeAt(index);
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getcandidatecandidate", { session_id: this.session_id }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["id"].setValue(this.response_data["id"]);
      this.localform.controls["photo_id"].setValue(this.response_data["photo_id"]);
      this.localform.controls["photo_id"].valueChanges.subscribe((val) => {
        if (this.localform.value["photo_id"] !== val) {
          setTimeout(() => {
            this["submitFormAPI"]();
          }, 200);
        }
      });
      this.localform.controls["whatsthis"].setValue(this.response_data["whatsthis"]);
      this.localform.controls["video_id"].setValue(this.response_data["video_id"]);
      this.localform.controls["video_id"].valueChanges.subscribe((val) => {
        if (this.localform.value["video_id"] !== val) {
          setTimeout(() => {
            this["submitFormAPI"]();
          }, 200);
        }
      });
      this.localform.controls["name"].setValue(this.response_data["name"]);
      this.localform.controls["changemobile"].setValue(this.response_data["changemobile"]);
      this.localform.controls["mobile"].setValue(this.response_data["mobile"]);
      this.localform.controls["email"].setValue(this.response_data["email"]);
      this.localform.controls["job_category_id"].setValue(this.response_data["job_category_id"]);
      this.localform.controls["job_role_id"].setValue(this.response_data["job_role_id"]);
      this.localform.controls["preferred_job_location"].setValue(this.response_data["preferred_job_location"]);
      this.localform.controls["preferred_job_latitude_location"].setValue(this.response_data["preferred_job_latitude_location"]);
      this.localform.controls["preferred_job_longitude_location"].setValue(this.response_data["preferred_job_longitude_location"]);
      this.localform.controls["preferred_job_location_city"].setValue(this.response_data["preferred_job_location_city"]);
      this.localform.controls["preferred_job_location_state"].setValue(this.response_data["preferred_job_location_state"]);
      this.localform.controls["preferred_job_location_country"].setValue(this.response_data["preferred_job_location_country"]);
      this.localform.controls["current_location"].setValue(this.response_data["current_location"]);
      this.localform.controls["current_latitude_location"].setValue(this.response_data["current_latitude_location"]);
      this.localform.controls["current_longitude_location"].setValue(this.response_data["current_longitude_location"]);
      this.localform.controls["current_location_city"].setValue(this.response_data["current_location_city"]);
      this.localform.controls["current_location_state"].setValue(this.response_data["current_location_state"]);
      this.localform.controls["current_location_country"].setValue(this.response_data["current_location_country"]);
      this.localform.controls["about_yourself"].setValue(this.response_data["about_yourself"]);
      this.localform.controls["current_salary"].setValue(this.response_data["current_salary"]);
      this.localform.controls["dob"].setValue(this.response_data["dob"]);
      this.localform.controls["pan_card"].setValue(this.response_data["pan_card"]);
      this.localform.controls["languages_known"].setValue(this.response_data["languages_known"]);
      this.localform.controls["resume_id"].setValue(this.response_data["resume_id"]);
      this.localform.controls["resume_id"].valueChanges.subscribe((val) => {
        if (this.localform.value["resume_id"] !== val) {
          setTimeout(() => {
            this["submitFormAPI"]();
          }, 200);
        }
      });
      this.localform.controls["attachments"] = new FormArray([]);
      if (this.response_data["attachments"] !== undefined) {
        this.response_data["attachments"].forEach((node) => {
          this.addNode_attachments(node);
        });
      }

      this.localform.controls["id"].setValue(this.response_data["id"]);
    });
  }

  response_category_id_options: any;
  load_category_id_options() {
    return this.apiService.post_api("jobportal", "getjobcategoriesoptions");
  }

  response_role_id_options: any;
  load_role_id_options() {
    return this.apiService.post_api("jobportal", "getjobrolesoptions");
  }
}
