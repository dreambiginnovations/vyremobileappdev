import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { FileUploaderModule } from "../common/fileuploader/fileuploader.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { MapLocationModule } from "../common/maplocation/maplocation.module";
import { CustomTextAreaModule } from "../common/customtextarea/customtextarea.module";
import { CustomDropDownModule } from "../common/dropdown/dropdown.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerCompanyJobAddComponent } from "./employer_company_job_add.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerCompanyJobAddComponent,
  },
];

@NgModule({
  declarations: [EmployerCompanyJobAddComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, FileUploaderModule, VideoRecordModule, MapLocationModule, CustomTextAreaModule, CustomDropDownModule],
})
export class EmployerCompanyJobAddModule {
  static components = {
    EmployerCompanyJobAddComponent: EmployerCompanyJobAddComponent,
  };
}
