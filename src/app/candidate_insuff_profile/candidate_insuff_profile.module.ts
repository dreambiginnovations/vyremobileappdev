import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateInsuffProfileComponent } from "./candidate_insuff_profile.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateInsuffProfileComponent,
  },
];

@NgModule({
  declarations: [CandidateInsuffProfileComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, VideoRecordModule],
})
export class CandidateInsuffProfileModule {
  static components = {
    CandidateInsuffProfileComponent: CandidateInsuffProfileComponent,
  };
}
