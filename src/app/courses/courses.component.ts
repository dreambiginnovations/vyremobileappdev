import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { TemplateutilsService } from "../services/templateutils.service";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__courses",
  templateUrl: "./courses.template.html",
  styleUrls: ["./courses.component.scss"],
})
export class CoursesComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  id: any;
  constructor(public templateutilsService: TemplateutilsService, private apiService: ApiService, private commonutilsService: CommonutilsService, private toastService: ToastService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];

    this.initializeFormGroup();

    this.route.queryParams.subscribe((queryParams) => {
      if (queryParams.page_number) {
        this.page_number = parseInt(queryParams.page_number);
      } else {
        this.page_number = 1;
      }
      this.load_data();
    });
  }

  localform: FormGroup;
  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("content_category_name", new FormControl(""));
    this.localform.addControl("courses", new FormArray([]));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  addNode_courses(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("content_category_name", new FormControl("", Validators.required));
    nodeControl.addControl("course_image_dummy_id", new FormControl("", Validators.required));
    nodeControl.addControl("id", new FormControl(""));

    if (nodeData !== undefined) {
      nodeControl.controls["content_category_name"].setValue(nodeData["content_category_name"]);
      nodeControl.controls["course_image_dummy_id"].setValue(nodeData["course_image_dummy_id"]);
      nodeControl.controls["id"].setValue(nodeData["id"]);
    }
    (<FormArray>this.localform.controls["courses"]).push(nodeControl);
  }

  removeNode_courses(index) {
    (<FormArray>this.localform.controls["courses"]).removeAt(index);
  }

  records_per_page = 6;
  page_number = 1;
  total_pages;
  load_data_page(page_number) {
    var newQueryParams = Object.assign({}, this.route.snapshot.queryParams);
    newQueryParams["page_number"] = page_number;
    this.router.navigate(["."], { relativeTo: this.route, queryParams: newQueryParams });
  }
  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getcourseswithparent", { id: this.id, records_per_page: this.records_per_page, page_number: this.page_number }).subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res.response;
      if (!isNaN(res.total)) {
        this.total_pages = Math.ceil(res.total / this.records_per_page);
      } else {
        this.total_pages = Math.ceil(res.total[Object.keys(res.total)[0]] / this.records_per_page);
      }

      this.localform.controls["content_category_name"].setValue(this.response_data["content_category_name"]);
      this.localform.controls["courses"] = new FormArray([]);
      if (this.response_data["courses"] !== undefined) {
        this.response_data["courses"].forEach((node) => {
          this.addNode_courses(node);
        });
      }
    });
  }
}
