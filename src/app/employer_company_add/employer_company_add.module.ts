import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { FileUploaderModule } from "../common/fileuploader/fileuploader.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { MapLocationModule } from "../common/maplocation/maplocation.module";
import { CustomTextAreaModule } from "../common/customtextarea/customtextarea.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { EmployerCompanyAddComponent } from "./employer_company_add.component";

export const routes: Routes = [
  {
    path: "",
    component: EmployerCompanyAddComponent,
  },
];

@NgModule({
  declarations: [EmployerCompanyAddComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, FileUploaderModule, VideoRecordModule, MapLocationModule, CustomTextAreaModule],
})
export class EmployerCompanyAddModule {
  static components = {
    EmployerCompanyAddComponent: EmployerCompanyAddComponent,
  };
}
