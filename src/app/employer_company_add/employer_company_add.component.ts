import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_company_add",
  templateUrl: "./employer_company_add.template.html",
  styleUrls: ["./employer_company_add.component.scss"],
})
export class EmployerCompanyAddComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__EDIT";
  id: any;
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, private templateutilsService: TemplateutilsService, private toastService: ToastService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.id = this.route.snapshot.params["id"];

    this.initializeFormGroup();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("company_id", new FormControl(""));
    this.localform.addControl("num_of_jobs", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("company_logo_id", new FormControl("", Validators.required));
    this.localform.controls["company_logo_id"].valueChanges.subscribe((val) => {
      if (this.localform.value["company_logo_id"] !== val) {
        setTimeout(() => {
          this["submitFormAPI"]();
        }, 200);
      }
    });
    this.localform.addControl("video_id", new FormControl(""));
    this.localform.controls["video_id"].valueChanges.subscribe((val) => {
      if (this.localform.value["video_id"] !== val) {
        setTimeout(() => {
          this.toastService.showWarning("Alert!", "Your video is NOT SAVED yet. Please fill in remaining details and SUBMIT this form to save.", 5000);
        }, 200);
      }
    });
    this.localform.addControl("company_name", new FormControl("", Validators.required));
    this.localform.addControl("company_location", new FormControl("", Validators.required));
    this.localform.controls["company_location"].valueChanges.subscribe((val) => {
      if (this.localform.value["company_location"] !== val) {
        setTimeout(() => {
          this["validateChosenLocation"]();
        }, 200);
      }
    });
    this.localform.addControl("company_latitude_location", new FormControl("", Validators.required));
    this.localform.addControl("company_longitude_location", new FormControl("", Validators.required));
    this.localform.addControl("company_location_city", new FormControl("", Validators.required));
    this.localform.addControl("company_location_state", new FormControl("", Validators.required));
    this.localform.addControl("company_location_country", new FormControl("", Validators.required));
    this.localform.addControl("no_of_employees", new FormControl("", Validators.required));
    this.localform.addControl("gstin", new FormControl(""));
    this.localform.addControl("pan", new FormControl(""));
    this.localform.addControl("company_description", new FormControl("", Validators.required));
    this.localform.addControl("attachments", new FormArray([], Validators.min(1)));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    this.localform = this.processformgroupforskip(this.localform);
    console.log("form before submit", this.localform);
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
      this.submitFormAPI();
    }
  }

  submitformres: any;
  submitFormAPI() {
    this.localform.value["id"] = this.id;
    this.apiService.post_api("jobportal", "addemployercompany", this.localform.value).subscribe((res) => {
      this.submitformres = res;
      if (this.submitformres.message) {
        if (this.submitformres.status == "TRUE") {
          this.toastService.showSuccess("Success", this.submitformres.message);
        } else if (this.submitformres.status == "FALSE") {
          this.toastService.showError("Error", this.submitformres.message);
        } else {
          this.toastService.showSuccess("Success", this.submitformres.message);
        }
      }
      if (res.status == "TRUE" || res.status === true) {
        this.commonutilsService.processResponse(res);
        this.ngOnInit();
      } else {
      }
    });
  }

  addNode_attachments(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("attachment_media_id", new FormControl("", Validators.required));
    nodeControl.controls["attachment_media_id"].valueChanges.subscribe((val) => {
      if (nodeControl.value["attachment_media_id"] !== val) {
        setTimeout(() => {
          this["submitFormAPI"]();
        }, 200);
      }
    });

    if (nodeData !== undefined) {
      nodeControl.controls["attachment_media_id"].valueChanges.subscribe((val) => {
        if (nodeControl.value["attachment_media_id"] !== val) {
          setTimeout(() => {
            this["submitFormAPI"]();
          }, 200);
        }
      });
    }
    (<FormArray>this.localform.controls["attachments"]).push(nodeControl);
  }

  removeNode_attachments(index) {
    (<FormArray>this.localform.controls["attachments"]).removeAt(index);
  }
}
