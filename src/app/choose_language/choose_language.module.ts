import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { ChooseLanguageComponent } from "./choose_language.component";

export const routes: Routes = [
  {
    path: "",
    component: ChooseLanguageComponent,
  },
];

@NgModule({
  declarations: [ChooseLanguageComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class ChooseLanguageModule {
  static components = {
    ChooseLanguageComponent: ChooseLanguageComponent,
  };
}
