import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { NavigationService } from "../services/navigation.service";
import { EnvService } from "../services/env.service";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__choose_language",
  templateUrl: "./choose_language.template.html",
  styleUrls: ["./choose_language.component.scss"],
})
export class ChooseLanguageComponent implements OnInit {
  @HostBinding("class") hostclasses = "viewascardondesktop bg-white px-0 flex-column nonstandardiconcontainer text-center container viewfor__DETAILS";
  constructor(private navigationService: NavigationService, public envService: EnvService, private apiService: ApiService, private commonutilsService: CommonutilsService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
  }

  localform: FormGroup;
  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("english", new FormControl(""));
    this.localform.controls["english"].setValue("English");
    this.localform.addControl("hindi", new FormControl(""));
    this.localform.controls["hindi"].setValue("Hindi");
    this.localform.addControl("telugu", new FormControl(""));
    this.localform.controls["telugu"].setValue("Telugu");
    this.localform.addControl("bengali", new FormControl(""));
    this.localform.controls["bengali"].setValue("Bengali");
    this.localform.addControl("kannada", new FormControl(""));
    this.localform.controls["kannada"].setValue("Kannada");
    this.localform.addControl("tamil", new FormControl(""));
    this.localform.controls["tamil"].setValue("Tamil");
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  chooseLanguage(language) {
    console.log("chooseLanguage");
    this.navigationService.back();
  }
}
