import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { CustomTextAreaModule } from "../common/customtextarea/customtextarea.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateJobApplicationAddComponent } from "./candidate_job_application_add.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateJobApplicationAddComponent,
  },
];

@NgModule({
  declarations: [CandidateJobApplicationAddComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, VideoRecordModule, CustomTextAreaModule],
})
export class CandidateJobApplicationAddModule {
  static components = {
    CandidateJobApplicationAddComponent: CandidateJobApplicationAddComponent,
  };
}
