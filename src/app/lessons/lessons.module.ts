import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { PipesModule } from "../common/pipes/pipes.module";
import { LessonsComponent } from "./lessons.component";

export const routes: Routes = [
  {
    path: "",
    component: LessonsComponent,
  },
];

@NgModule({
  declarations: [LessonsComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class LessonsModule {
  static components = {
    LessonsComponent: LessonsComponent,
  };
}
