import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { TemplateutilsService } from "../services/templateutils.service";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__lms",
  templateUrl: "./lms.template.html",
  styleUrls: ["./lms.component.scss"],
})
export class LmsComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__DETAILS";
  constructor(public templateutilsService: TemplateutilsService, private apiService: ApiService, private commonutilsService: CommonutilsService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;
  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("hotcourses", new FormArray([]));
    this.localform.addControl("coursecategories", new FormArray([]));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }

  addNode_hotcourses(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("content_category_name", new FormControl("", Validators.required));
    nodeControl.addControl("course_image_dummy_id", new FormControl("", Validators.required));
    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("content_category_short_description", new FormControl("", Validators.required));

    if (nodeData !== undefined) {
      nodeControl.controls["content_category_name"].setValue(nodeData["content_category_name"]);
      nodeControl.controls["course_image_dummy_id"].setValue(nodeData["course_image_dummy_id"]);
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["content_category_short_description"].setValue(nodeData["content_category_short_description"]);
    }
    (<FormArray>this.localform.controls["hotcourses"]).push(nodeControl);
  }

  removeNode_hotcourses(index) {
    (<FormArray>this.localform.controls["hotcourses"]).removeAt(index);
  }

  addNode_coursecategories(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("content_category_name", new FormControl("", Validators.required));
    nodeControl.addControl("course_image_dummy_id", new FormControl("", Validators.required));
    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("content_category_short_description", new FormControl("", Validators.required));

    if (nodeData !== undefined) {
      nodeControl.controls["content_category_name"].setValue(nodeData["content_category_name"]);
      nodeControl.controls["course_image_dummy_id"].setValue(nodeData["course_image_dummy_id"]);
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["content_category_short_description"].setValue(nodeData["content_category_short_description"]);
    }
    (<FormArray>this.localform.controls["coursecategories"]).push(nodeControl);
  }

  removeNode_coursecategories(index) {
    (<FormArray>this.localform.controls["coursecategories"]).removeAt(index);
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getlmshomecontent").subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["hotcourses"] = new FormArray([]);
      if (this.response_data["hotcourses"] !== undefined) {
        this.response_data["hotcourses"].forEach((node) => {
          this.addNode_hotcourses(node);
        });
      }

      this.localform.controls["coursecategories"] = new FormArray([]);
      if (this.response_data["coursecategories"] !== undefined) {
        this.response_data["coursecategories"].forEach((node) => {
          this.addNode_coursecategories(node);
        });
      }
    });
  }
}
