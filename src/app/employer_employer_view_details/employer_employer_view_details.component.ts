import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, FormArray, AbstractControl, Validators } from "@angular/forms";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { TemplateutilsService } from "../services/templateutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_employer_view_details",
  templateUrl: "./employer_employer_view_details.template.html",
  styleUrls: ["./employer_employer_view_details.component.scss"],
})
export class EmployerEmployerViewDetailsComponent implements OnInit {
  @HostBinding("class") hostclasses = "container px-0 viewfor__EDIT";
  constructor(private apiService: ApiService, private commonutilsService: CommonutilsService, public templateutilsService: TemplateutilsService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
    this.load_data();
  }

  localform: FormGroup;

  skipsubmitValidator(control: AbstractControl) {
    return { skip: true };
  }

  processcontrolforskip(control) {
    var newControl;
    if (control instanceof FormArray) {
      newControl = this.processformgroupforskip(control, true);
    } else if (control instanceof FormGroup) {
      newControl = this.processformgroupforskip(control, false);
    } else {
      newControl = control;
    }
    return newControl;
  }

  processformgroupforskip(formgroup, controlsArray?) {
    var returnFormGroup;
    if (controlsArray) {
      returnFormGroup = new FormArray([]);
      for (var count = 0; count < formgroup.controls.length; count++) {
        var control = formgroup.controls[count];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.push(newControl);
        }
      }
    } else {
      returnFormGroup = new FormGroup({});
      for (var controlName in formgroup.controls) {
        var control = formgroup.controls[controlName];
        if (control.errors?.skip !== true) {
          var newControl = this.processcontrolforskip(control);
          returnFormGroup.addControl(controlName, newControl);
        }
      }
    }
    return returnFormGroup;
  }

  initializeFormGroup() {
    this.localform = new FormGroup({});

    this.localform.addControl("edit_details_top", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("photo_id", new FormControl(""));
    this.localform.addControl("name", new FormControl(""));
    this.localform.addControl("mobile", new FormControl("", Validators.compose([Validators.minLength(10), Validators.maxLength(10)])));
    this.localform.addControl("email", new FormControl(""));
    this.localform.addControl("current_location", new FormControl(""));
    this.localform.addControl("current_latitude_location", new FormControl(""));
    this.localform.addControl("current_longitude_location", new FormControl(""));
    this.localform.addControl("current_location_city", new FormControl(""));
    this.localform.addControl("current_location_state", new FormControl(""));
    this.localform.addControl("current_location_country", new FormControl(""));
    this.localform.addControl("company_id", new FormControl(""));
    this.localform.addControl("num_of_jobs", new FormControl("", this.skipsubmitValidator));
    this.localform.addControl("company_logo_id", new FormControl(""));
    this.localform.addControl("video_id", new FormControl(""));
    this.localform.addControl("company_name", new FormControl(""));
    this.localform.addControl("company_location", new FormControl(""));
    this.localform.addControl("company_latitude_location", new FormControl(""));
    this.localform.addControl("company_longitude_location", new FormControl(""));
    this.localform.addControl("company_location_city", new FormControl(""));
    this.localform.addControl("company_location_state", new FormControl(""));
    this.localform.addControl("company_location_country", new FormControl(""));
    this.localform.addControl("no_of_employees", new FormControl(""));
    this.localform.addControl("gstin", new FormControl(""));
    this.localform.addControl("pan", new FormControl(""));
    this.localform.addControl("company_description", new FormControl(""));
    this.localform.addControl("attachments", new FormArray([], Validators.min(1)));
  }

  validateChosenLocation() {
    if (this.localform.value["company_location"] !== null && this.localform.value["company_location_city"] === null) {
      this.localform.controls["company_location"].patchValue(null);
      alert("ERROR! You cannot select State or District as Company Location, please select again.");
    }
  }
  addNode_attachments(nodeData?) {
    let nodeControl = new FormGroup({});

    nodeControl.addControl("id", new FormControl(""));
    nodeControl.addControl("attachment_media_id", new FormControl(""));

    if (nodeData !== undefined) {
      nodeControl.controls["id"].setValue(nodeData["id"]);
      nodeControl.controls["attachment_media_id"].setValue(nodeData["attachment_media_id"]);
      nodeControl.controls["attachment_media_id"].valueChanges.subscribe((val) => {
        if (nodeControl.value["attachment_media_id"] !== val) {
          setTimeout(() => {
            this["submitFormAPI"]();
          }, 200);
        }
      });
    }
    (<FormArray>this.localform.controls["attachments"]).push(nodeControl);
  }

  removeNode_attachments(index) {
    (<FormArray>this.localform.controls["attachments"]).removeAt(index);
  }

  response_data: any;
  load_data() {
    this.apiService.post_api("jobportal", "getemployeremployer").subscribe((res) => {
      if (res.status === "FALSE" || res.status === false) {
        this.toastService.showError("Error", res.message);
        this.commonutilsService.processResponse(res);
        return;
      }
      this.response_data = res;

      this.localform.controls["edit_details_top"].setValue(this.response_data["edit_details_top"]);
      this.localform.controls["photo_id"].setValue(this.response_data["photo_id"]);
      this.localform.controls["name"].setValue(this.response_data["name"]);
      this.localform.controls["mobile"].setValue(this.response_data["mobile"]);
      this.localform.controls["email"].setValue(this.response_data["email"]);
      this.localform.controls["current_location"].setValue(this.response_data["current_location"]);
      this.localform.controls["current_latitude_location"].setValue(this.response_data["current_latitude_location"]);
      this.localform.controls["current_longitude_location"].setValue(this.response_data["current_longitude_location"]);
      this.localform.controls["current_location_city"].setValue(this.response_data["current_location_city"]);
      this.localform.controls["current_location_state"].setValue(this.response_data["current_location_state"]);
      this.localform.controls["current_location_country"].setValue(this.response_data["current_location_country"]);
      this.localform.controls["company_id"].setValue(this.response_data["company_id"]);
      this.localform.controls["num_of_jobs"].setValue(this.response_data["num_of_jobs"]);
      this.localform.controls["company_logo_id"].setValue(this.response_data["company_logo_id"]);
      this.localform.controls["company_logo_id"].valueChanges.subscribe((val) => {
        if (this.localform.value["company_logo_id"] !== val) {
          setTimeout(() => {
            this["submitFormAPI"]();
          }, 200);
        }
      });
      this.localform.controls["video_id"].setValue(this.response_data["video_id"]);
      this.localform.controls["video_id"].valueChanges.subscribe((val) => {
        if (this.localform.value["video_id"] !== val) {
          setTimeout(() => {
            this["submitForm"]();
          }, 200);
        }
      });
      this.localform.controls["company_name"].setValue(this.response_data["company_name"]);
      this.localform.controls["company_location"].setValue(this.response_data["company_location"]);
      this.localform.controls["company_location"].valueChanges.subscribe((val) => {
        if (this.localform.value["company_location"] !== val) {
          setTimeout(() => {
            this["validateChosenLocation"]();
          }, 200);
        }
      });
      this.localform.controls["company_latitude_location"].setValue(this.response_data["company_latitude_location"]);
      this.localform.controls["company_longitude_location"].setValue(this.response_data["company_longitude_location"]);
      this.localform.controls["company_location_city"].setValue(this.response_data["company_location_city"]);
      this.localform.controls["company_location_state"].setValue(this.response_data["company_location_state"]);
      this.localform.controls["company_location_country"].setValue(this.response_data["company_location_country"]);
      this.localform.controls["no_of_employees"].setValue(this.response_data["no_of_employees"]);
      this.localform.controls["gstin"].setValue(this.response_data["gstin"]);
      this.localform.controls["pan"].setValue(this.response_data["pan"]);
      this.localform.controls["company_description"].setValue(this.response_data["company_description"]);
      this.localform.controls["attachments"] = new FormArray([]);
      if (this.response_data["attachments"] !== undefined) {
        this.response_data["attachments"].forEach((node) => {
          this.addNode_attachments(node);
        });
      }
    });
  }
}
