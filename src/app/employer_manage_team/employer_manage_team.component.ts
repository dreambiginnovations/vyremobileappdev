import { Component, OnInit, HostBinding } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NavigationService } from "../services/navigation.service";
import { EnvService } from "../services/env.service";
import { ApiService } from "../services/api.service";
import { CommonutilsService } from "../services/commonutils.service";
import { ToastService } from "../services/toast.service";

@Component({
  selector: ".component__employer_manage_team",
  templateUrl: "./employer_manage_team.component.html",
  styleUrls: ["./employer_manage_team.component.scss"],
})
export class EmployerManageTeamComponent implements OnInit {
  @HostBinding("class") hostclasses = "viewascardondesktop bg-white px-0 flex-column nonstandardiconcontainer text-center container viewfor__DETAILS";
  constructor(public envService: EnvService, private apiService: ApiService, private toastService: ToastService) {}

  ngOnInit() {
    this.initializeFormGroup();
    this.getEmployees();
  }

  localform: FormGroup;
  initializeFormGroup() {
    this.localform = new FormGroup({});

   this.localform.addControl("name", new FormControl("", [Validators.required]));
   this.localform.addControl("mobile", new FormControl("", [Validators.required]));
  }

  submitForm() {
    this.localform.updateValueAndValidity();
    if (this.localform.status == "INVALID") {
      this.hostclasses += " showvalidationerrors";
      alert("Errors in the form");
      return;
    } else {
    }
  }
  employees = [];
  getEmployees() {
    this.apiService.post_api("jobportal", "manageemployeesunderemployer").subscribe((res) => {
      this.employees = res.employees;
    });
  }
  addEmployee() {
    if(this.localform.controls["name"].status=="INVALID") {
      this.toastService.showError("Name is Required", "Please fill in a vaild Name.");
      return;
    }
    if(this.localform.controls["mobile"].status=="INVALID") {
      this.toastService.showError("Invalid Mobile", "Please fill in a proper 10 digit Mobile Number.");
      return;
    }
    if(this.localform.value["mobile"].toString().length!=10) {
      this.toastService.showError("Invalid Mobile", "Please fill in a proper 10 digit Mobile Number.");
      return;
    }
    this.apiService.post_api("jobportal", "manageemployeesunderemployer", {"OPERATION": "ADD", "name": this.localform.value["name"], "mobile": this.localform.value["mobile"]}).subscribe((res) => {
      if(res.message) {
        this.toastService.showError("ERROR!", res.message);
      } else {
        this.employees = res.employees;
      }
    });
  }
  deleteEmployee(employeeID) {
    if(confirm("Are you sure?")) {
      this.apiService.post_api("jobportal", "manageemployeesunderemployer", {"OPERATION": "DELETE", "id": employeeID}).subscribe((res) => {
        this.employees = res.employees;
      });
    }
  }

}
