import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { EmployerManageTeamComponent } from "./employer_manage_team.component";

import { PipesModule } from "../common/pipes/pipes.module";

export const routes: Routes = [
  {
    path: "",
    component: EmployerManageTeamComponent,
  },
];

@NgModule({
  declarations: [EmployerManageTeamComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule],
})

export class EmployerManageTeamModule {
  static components = {
    EmployerManageTeamComponent: EmployerManageTeamComponent,
  };
}
