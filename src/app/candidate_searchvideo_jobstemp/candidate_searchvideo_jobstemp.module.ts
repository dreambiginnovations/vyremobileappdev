import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { ImgCropperModule } from "../common/imagecropper/imagecropper.module";
import { VideoRecordModule } from "../common/videorecord/videorecord.module";
import { PipesModule } from "../common/pipes/pipes.module";
import { CandidateSearchvideoJobstempComponent } from "./candidate_searchvideo_jobstemp.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateSearchvideoJobstempComponent,
  },
];

@NgModule({
  declarations: [CandidateSearchvideoJobstempComponent],
  imports: [CommonModule, PipesModule, RouterModule.forChild(routes), ReactiveFormsModule, ImgCropperModule, VideoRecordModule],
})
export class CandidateSearchvideoJobstempModule {
  static components = {
    CandidateSearchvideoJobstempComponent: CandidateSearchvideoJobstempComponent,
  };
}
