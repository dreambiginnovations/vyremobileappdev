import { BrowserModule } from "@angular/platform-browser";
import { NgModule, APP_INITIALIZER } from "@angular/core";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { MatDialogModule } from "@angular/material/dialog";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule } from "@angular/forms";

import { NgxGoogleAnalyticsModule } from "ngx-google-analytics";

import { HttpRequestInterceptor } from "./services/httprequestinterceptor.service";
import { AppinitializeService } from "./services/appinitialize.service";
import { DummyAPIService } from "./services/dummyapi.service";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { HeaderComponent } from "./header/header.component";

import { DialogComponent } from "./common/dialog/dialog.component";
import { AlertComponent } from "./common/dialog/alert.component";

export function initializeApp1(appInitService: AppinitializeService) {
  return (): Promise<any> => {
    return appInitService.initializeApp();
  };
}

@NgModule({
  declarations: [AppComponent, HeaderComponent, DialogComponent, AlertComponent],
  imports: [BrowserModule, AppRoutingModule, BrowserAnimationsModule, HttpClientModule, MatDialogModule, ReactiveFormsModule, NgxGoogleAnalyticsModule.forRoot("UA-206519367-1")],
  providers: [
    DummyAPIService,
    AppinitializeService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp1,
      deps: [AppinitializeService],
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpRequestInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
