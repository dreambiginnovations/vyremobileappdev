import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";

import { CandidateRequestForInterviewComponent } from "./candidate_request_for_interview.component";

export const routes: Routes = [
  {
    path: "",
    component: CandidateRequestForInterviewComponent,
  },
];

@NgModule({
  declarations: [CandidateRequestForInterviewComponent],
  imports: [CommonModule, RouterModule.forChild(routes), ReactiveFormsModule],
})
export class CandidateRequestForInterviewModule {
  static components = {
    CandidateRequestForInterviewComponent: CandidateRequestForInterviewComponent,
  };
}
