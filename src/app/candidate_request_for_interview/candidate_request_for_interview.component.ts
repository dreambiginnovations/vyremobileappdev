import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppstateService } from "../services/appstate.service";
import { ApiService } from "../services/api.service";

@Component({
  selector: ".component__candidate_request_for_interview",
  templateUrl: "./candidate_request_for_interview.template.html"
})
export class CandidateRequestForInterviewComponent implements OnInit {
    constructor(private appstateService: AppstateService, private apiService: ApiService, public router: Router) {
        
    }
    username;
    ngOnInit() {
        this.username = this.appstateService.appState.user.name;
        this.getinterviewrequestsofcandidate();
    }
    
    interviewRequests: any;
    getinterviewrequestsofcandidate() {
        this.apiService.post_api("jobportal", "getinterviewrequestsofcandidate", {user_id: this.appstateService.appState.user.id}).subscribe((res) => {
          this.interviewRequests = res;
        });
    }
}