<?php

  $page = $_GET["page"];
  //print_r($page);
  $apiURL = "https://".$_SERVER["SERVER_NAME"]."/backend/index.php/jobportal/getsocialshareinfo?page=$page";
  $arrContextOptions=array(
      "ssl"=>array(
          "verify_peer"=>false,
          "verify_peer_name"=>false,
      ),
  );
  $content = file_get_contents($apiURL, false, stream_context_create($arrContextOptions));
  $content = json_decode($content, true);
  $host = $_SERVER["HTTP_HOST"];
  $host = str_replace("social_media.php?", "", $host);
  $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$host."/".$page;
  $url = "https://app.swaayam.co/".$page;
  if(!isset($content["response"]["og:url"])) {
    $content["response"]["og:url"] = $url;
  }
  if(!isset($content["response"]["og:type"])) {
    $content["response"]["og:type"] = "website";
  }
  if(!isset($content["response"]["twitter:card"])) {
    $content["response"]["twitter:card"] = "summary_large_image";
  }
  if(!isset($content["response"]["twitter:domain"])) {
    $content["response"]["twitter:domain"] = "";
  }
  if(!isset($content["response"]["twitter:url"])) {
    $content["response"]["twitter:url"] = $url;
  }

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <link rel="icon" type="image/x-icon" href="favicon.ico">

  <title><?=$content["response"]["title"] ?></title>
  <meta name="description" content="<?=substr(strip_tags($content["response"]["description"]), 0, 100) ?>">
  
  <!-- Facebook Meta Tags -->
  <meta property="og:url" content="<?=$content["response"]["og:url"] ?>">
  <meta property="og:type" content="<?=$content["response"]["og:type"] ?>">
  <meta property="og:title" content="<?=$content["response"]["og:title"] ?>">
  <meta property="og:description" content="<?=substr(strip_tags($content["response"]["og:description"]), 0, 100) ?>">
  <meta property="og:image" content="<?=$content["response"]["og:image"] ?>">
  
  <!-- Twitter Meta Tags -->
  <meta name="twitter:card" content="<?=$content["response"]["twitter:card"] ?>">
  <meta property="twitter:domain" content="<?=$content["response"]["twitter:domain"] ?>">
  <meta property="twitter:url" content="<?=$content["response"]["twitter:url"] ?>">
  <meta name="twitter:title" content="<?=$content["response"]["twitter:title"] ?>">
  <meta name="twitter:description" content="<?=substr(strip_tags($content["response"]["twitter:description"]), 0, 100) ?>">
  <meta name="twitter:image" content="<?=$content["response"]["twitter:image"] ?>">
  
<?php if(isset($content["response"]["job"])) { 
        $jobObj = $content["response"]["job"];
        $createdDate = new DateTime($jobObj["created_at"]);
        $jobObj["created_at"] = $createdDate->format('Y-m-d');
        $createdDate->add(new DateInterval("P30D"));
        $jobObj["valid_through"] = $createdDate->format('Y-m-d');
?>  
  <script type="application/ld+json">
    {
      "@context" : "https://schema.org/",
      "@type" : "JobPosting",
      "title" : "<?=$jobObj["job_title"] ?>",
      "description" : "<?=$jobObj["job_description"] ?>",
      "identifier": {
        "@type": "PropertyValue",
        "name": "SWAAYAM",
        "value": "<?=$jobObj["id"] ?>"
      },
      "datePosted" : "<?=$jobObj["created_at"] ?>",
      "validThrough" : "<?=$jobObj["valid_through"] ?>",
      "employmentType" : "FULL_TIME",
      "hiringOrganization" : {
        "@type" : "Organization",
        "name" : "<?=$jobObj["company_name"] ?>",
        "logo" : "<?=$jobObj["company_logo_url"] ?>"
      },
      
<?php if($jobObj["is_remote_job"]==="Yes") { ?>      
      "applicantLocationRequirements": {
        "@type": "Country",
        "name": "INDIA"
      },
      "jobLocationType": "TELECOMMUTE",
<?php } else { ?>      
      "jobLocation": {
        "@type": "Place",
        "address": {
          "@type": "PostalAddress",
          "streetAddress": "<?=$jobObj["job_location"] ?>",
          "addressLocality": "<?=$jobObj["job_location_city"] ?>",
          "addressRegion": "<?=$jobObj["job_location_state"] ?>",
          "addressCountry": "<?=$jobObj["job_location_country"] ?>"
        }
      },
<?php } ?>      
      "baseSalary": {
        "@type": "MonetaryAmount",
        "currency": "INR",
        "value": {
          "@type": "QuantitativeValue",
          "value": <?=$jobObj["min_job_salary"] ?>,
          "unitText": "MONTH"
        }
      }
    }
    </script>
<?php } ?>

</head>
<body class="mat-typography">

</body>
</html>