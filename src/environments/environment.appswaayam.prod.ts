export const environment = {
  production: true,
  staticmediaurl: 'https://swaayam.s3.ap-south-1.amazonaws.com/static/',
  ugcmediaurl: "https://swaayam.s3.ap-south-1.amazonaws.com/live/ugc/",
  api: 'backend/index.php',
  updateusertoke_controller: 'jobportal',
  updateusertoke_method: 'updateusertoken'
};