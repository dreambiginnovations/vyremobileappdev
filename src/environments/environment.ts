export const environment = {
  production: false,
  staticmediaurl: 'https://swaayam.s3.ap-south-1.amazonaws.com/static/',
  ugcmediaurl: 'https://swaayam.s3.ap-south-1.amazonaws.com/ugc/',
  lmsmediaurl: 'https://swaayam.s3.ap-south-1.amazonaws.com/testing/lms/',
  api: 'http://localhost/devbackend-bhuvan/index.php',
  updateusertoke_controller: 'jobportal',
  updateusertoke_method: 'updateusertoken',
  dummyapi: true
};
