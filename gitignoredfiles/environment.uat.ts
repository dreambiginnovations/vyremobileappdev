export const environment = {
  production: true,
  staticmediaurl: "https://s3dam.s3.ap-south-1.amazonaws.com/static/",
  ugcmediaurl: "https://s3dam.s3.ap-south-1.amazonaws.com/ugc/",
  api: 'https://ravitejadev2.motherpod.org:442/index.php'
};
