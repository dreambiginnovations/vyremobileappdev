export const environment = {
  production: false,
  staticmediaurl: 'https://s3dam.s3.ap-south-1.amazonaws.com/static/',
  ugcmediaurl: 'https://s3dam.s3.ap-south-1.amazonaws.com/ugc/',
  api: 'https://ashishdev.motherpod.org:442/backendcodegeneration/generatedproject/index.php'
};
